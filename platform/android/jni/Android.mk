LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := draft
LOCAL_MODULE_FILENAME := libdraft

LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/../../../engine/include/

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../../engine/include/

LOCAL_SRC_FILES := \
	../../../engine/src/collision.c \
	../../../engine/src/config.c \
	../../../engine/src/debug.c \
	../../../engine/src/dslib.c \
	../../../engine/src/filesystem.c \
	../../../engine/src/lz4.c \
	../../../engine/src/maths.c \
	../../../engine/src/octree.c \
	../../../engine/src/platform.c \
	../../../engine/src/render.c \
	../../../engine/src/resource.c \
	../../../engine/src/scene.c \

LOCAL_LDLIBS := -llog -landroid -lEGL -lGLESv1_CM

LOCAL_EXPORT_LDLIBS := -llog -landroid -lEGL -lGLESv1_CM

include $(BUILD_STATIC_LIBRARY)
