/******************************************************************************
 *	File:			collision.h
 *
 *	Created by:		Ivan Shishkin <codingdude@gmail.com>
 *
 *	History:		Created 30/01/13
 *
 *	Copyright (c) 2013 All rights reserved
 *
 *****************************************************************************/

#ifndef __DRAFT_COLLISION_H__
#define __DRAFT_COLLISION_H__

/******************************************************************************
 * Bounding box
 *****************************************************************************/

/**
 *	Returns the identity bounding box.
 *	@param out Pointer to the bbox_t structure that is the result of the operation.
 *	@return Pointer to the bbox_t structure that is the identity bounding box.
 */
bbox_t *bbox_identity(bbox_t *out);

/**
 *	Sets both minimum and maximum extents.
 *	@param out Pointer to the bbox_t structure that is the result of the operation.
 *	@param minimum Pointer to a vector3_t structure which x, y, z coordinates are minimum extents.
 *	@param minimum Pointer to a vector3_t structure which x, y, z coordinates are maximum extents.
 *	@return Pointer to the bbox_t structure.
 */
bbox_t *bbox_set_extents(bbox_t *out, const vector3_t *minimum, const vector3_t *maximum);

/**
 *	Sets box to infinite.
 *	@param out Pointer to the bbox_t structure that is the result of the operation.
 *	@return Pointer to the bbox_t structure.
 */
bbox_t *bbox_set_infinite(bbox_t *out);

/**
 *	Merges given boxes into one.
 *	@param out Pointer to the bbox_t structure that is the result of the operation.
 *	@param bb1 Pointer to a source bbox_t structure.
 *	@param bb1 Pointer to a source bbox_t structure.
 *	@return A pointer to the bbox_t structure which encompasses both boxes.
 */
bbox_t *bbox_merge(bbox_t *out, const bbox_t *bb1, const bbox_t *bb2);

/**
 *	Extends the box to encompass the specified point, if needed.
 *	@param out Pointer to the bbox_t structure that is the result of the operation.
 *	@param bb Pointer to a source bbox_t structure.
 *	@param Pointer to a source vector3_t structure.
 *	@return A pointer to the bbox_t structure which encompasses point.
 */
bbox_t *bbox_merge_point(bbox_t *out, const bbox_t *bb, const vector3_t *p);

/**
 *	Transforms the box according to the matrix supplied.
 *	@param out Pointer to the bbox_t structure that is the result of the operation.
 *	@param bb Pointer to a source bbox_t structure.
 *	@param tm Pointer to a source matrix_t structure.
 *	@return Pointer to the bbox_t structure which is transformed box.
 */
bbox_t *bbox_transform(bbox_t *out, const bbox_t *bb, const matrix_t *tm);

/**
 *	Returns whether or not one box intersects another.
 *	@param bb1 Pointer to the source bbox_t structure.
 *	@param bb2 Pointer to the source bbox_t structure.
 *	@return Nonzero value if boxes has intersection, otherwise returns zero.
 */
int bbox_intersects(const bbox_t *bb1, const bbox_t *bb2);

/**
 *	Tests whether the vector point is within a box.
 *	@param bb Pointer to the source bbox_t structure.
 *	@param p Pointer to the source vector_t structure.
 *	@return Nonzero value if point is inside the box, otherwise returns zero.
 */
int bbox_intersects_point(const bbox_t *bb, const vector3_t *p);

/**
 *	Gets the minimum corner of the box.
 *	@param out Pointer to the vector_t structure what is the result of operation.
 *	@param bb Pointer to the source bbox_t structure.
 *	@return Pointer to a vector_t structure which is the minimum corner of the box.
 */
vector3_t *bbox_get_minimum(vector3_t *out, const bbox_t *bb);

/**
 *	Gets the maximum corner of the box.
 *	@param out Pointer to the vector_t structure what is the result of operation.
 *	@param bb Pointer to the source bbox_t structure.
 *	@return Pointer to a vector_t structure which is the maximum corner of the box.
 */
vector3_t *bbox_get_maximum(vector3_t *out, const bbox_t *bb);

/*
 *	Gets the center of the box
 *	@param out The pointer to the vector3_t structure what is a result of operation
 *	@param bbox The given bounding box
 *	@return The pointer to the vector3_t which is a center of a given box
 */
vector3_t *bbox_get_center(vector3_t *out, const bbox_t *bbox);

/*
 *	Gets the size of the box
 *	@param out The pointer to the vector3_t structure what is a result of operation
 *	@param bbox The given bounding box
 *	@return The pointer to the vector3_t which is a size of a given box
 */
vector3_t *bbox_get_size(vector3_t *out, const bbox_t *bbox);

/*
 *	Gets the half size of the box
 *	@param out The pointer to the vector3_t structure what is a result of operation
 *	@param bbox The given bounding box
 *	@return The pointer to the vector3_t which is a half size of a given box
 */
vector3_t *bbox_get_half_size(vector3_t *out, const bbox_t *bbox);

/**
 *	Returns a pointer to an array of 8 vector3_t structures which is corner points
 *	@param out Pointer to the array of vector_t structures what is the result of operation.
 *	@param bb Pointer to the source bbox_t structure.
 *	@return Pointer to an array of 8 vector3_t structures which is corner points.
 */
vector3_t *bbox_get_corners(vector3_t *out, const bbox_t *bb);

/**
 *	Returns whether or not the box is infinite.
 *	@param bb Pointer to the source bbox_t structure.
 *	@return Nonsero value if the box is infinite.
 */
int bbox_is_infinite(const bbox_t *bb);

/******************************************************************************
 * Frustum
 *****************************************************************************/

/**
 *	Calculates a view matrix for the frustum.
 *	@param f Pointer to the frustum_t structure which is a result of operation.
 *	@param translation Pointer to a vector3_t structure, what describes the translation.
 *	@param scale Pointer to a vector3_t structure, what contains scaling factors applied along the x, y, and z-axes.
 *	@param rotation Pointer to a quaternion_t structure, what describes the rotation.
 *	@return Pointer to the frustum_t structure which is a result of operation.
 */
frustum_t *frustum_set_view(frustum_t *f, const vector3_t *translation, const vector3_t *scale, const quaternion_t *rotation);

/**
 *	Calculates an orthographic projection matrix for the frustum.
 *	@param f Pointer to the frustum_t structure which is a result of operation.
 *	@param w Width of the view volume.
 *	@param h Height of the view volume.
 *	@param zn Minimum z-value of the view volume which is referred to as z-near.
 *	@param zf Maximum z-value of the view volume which is referred to as z-far.
 *	@return Pointer to the frustum_t structure which is a result of operation.
 */
frustum_t * frustum_set_ortho(frustum_t *f, float w, float h, float zn, float zf);

/**
 *	Calculates a perspective projection matrix for the frustum.
 *	@param f Pointer to the frustum_t structure which is a result of operation.
 *	@param w Width of the view volume at the near view-plane.
 *	@param h Height of the view volume at the near view-plane.
 *	@return Pointer to the frustum_t structure which is a result of operation.
 */
frustum_t *frustum_set_perspective(frustum_t *f, float w, float h, float zn, float zf);

/**
 *	Calculates a perspective projection matrix for the frustum based on a field of view.
 *	@param f Pointer to the frustum_t structure which is a result of operation.
 *	@param fov Field of view in the y direction, in radians.
 *	@param aspect Aspect ratio, defined as view space width divided by height.
 *	@param zn Z-value of the near view-plane.
 *	@param zf Z-value of the far view-plane.
 *	@return Pointer to the frustum_t structure which is a result of operation.
 */
frustum_t *frustum_set_perspective_fov(frustum_t *f, float fov, float aspect, float zn, float zf);

/**
 *	Gets a view matrix for the frustum.
 *	@param f Pointer to the source frustum_t structure.
 *	@return A pointer to the matrix_t structure which is the view matrix.
 */
const matrix_t *frustum_get_view_matrix(const frustum_t *f);

/**
 *	Gets the projection matrix for the frustum.
 *	@param f Pointer to the source frustum_t structure.
 *	@return A pointer to the matrix_t structure which is the projection matrix.
 */	
const matrix_t *frustum_get_proj_matrix(const frustum_t *f);

/**
 *	Tests whether the given bounding box is visible in the Frustum.
 *	@param f Pointer to the source frustum_t structure.
 *	@param bb Pointer to the source bbox_t structure.
 *	@return A nonzero value if the given bounding box is visible in the frustum.
 */
int frustum_is_visible_box(frustum_t *f, const bbox_t *bb);

#endif // __DRAFT_COLLISION_H__