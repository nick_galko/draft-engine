/******************************************************************************
 *	File:			debug.h
 *
 *	Created by:		Ivan Shishkin <codingdude@gmail.com>
 *
 *	History:		Created 19/01/13
 *
 *	Copyright (c) 2013 All rights reserved
 *
 *****************************************************************************/

#ifndef __DRAFT_DEBUG_H__
#define __DRAFT_DEBUG_H__

/**
 *	Redirects debug output.
 *	@param out - The pointer to the function used to output debug message.
 *	@return Returns the pointer to the debug output function used before.
 */
debug_output_func debug_redirect_output(debug_output_func out);

/**
 *	Gets the pointer to the currently used debug output function.
 *	@return The pointer to the current debug output function.
 */
debug_output_func debug_output();

/**
 *	Gets the pointer to the default debug output function.
 *	@return The pointer to the default debug output function.
 */
debug_output_func debug_default_output();

/**
 *	Outputs a debug message.
 *	@param format - Format control.
 *	@param argument - optional arguments.
 */
void debug_message(const char *format, ...);

/**
 *	Outputs a debug warning.
 *	@param format - Format control.
 *	@param argument - optional arguments.
 */
void debug_warning(const char *format, ...);

/**
 *	Outputs a debug error and terminates the calling process.
 *	@param format - Format control.
 *	@param argument - optional arguments.
 */
void debug_error(const char *format, ...);

/* Used to display messages, should never be called directly. */
void debug_info(const char *msg, const char *file, int line);

/* Used to define macros, never use these directly. */
#define _assert(condition) \
	do { \
		if (!(condition)) { \
			debug_info("Assertion failed: " #condition, __FILE__, __LINE__); \
			debug_break(); \
		} \
	} while(0)

/* debug macros */
#if defined (DEBUG)

#define debug_assert(condition) _assert(condition)

#else

#define debug_assert(condition) ((void)0)

#endif // DEBUG

#endif // __DRAFT_DEBUG_H__