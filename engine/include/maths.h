/******************************************************************************
 *	File:			maths.h
 *
 *	Created by:		Ivan Shishkin <codingdude@gmail.com>
 *
 *	History:		Created 23/09/12
 *
 *	Copyright (c) 2012 All rights reserved
 *
 *****************************************************************************/

#ifndef __DRAFT_MATHS_H__
#define __DRAFT_MATHS_H__

/******************************************************************************
 * 2D Vector
 *****************************************************************************/

/**
 *	Dot product of two 2D vectors.
 *	@param v1 Pointer to a source vector2_t structure.
 *	@param v2 Pointer to a source vector2_t structure.
 *	@return A float representing the dot product value.
 */
float vector2_dot(const vector2_t *v1, const vector2_t *v2);

/**
 *	Taking the cross product of two 2D vectors.
 *	@param v1 Pointer to a source vector2_t structure.
 *	@param v2 Pointer to a source vector2_t structure.
 *	@return The z-component.
 */
float vector2_cross(const vector2_t *v1, const vector2_t *v2);

/**
 *	Adds two 2D vectors.
 *	@param out Pointer to the vector2_t structure that is the result of the operation.
 *	@param v1 Pointer to a source vector2_t structure.
 *	@param v2 Pointer to a source vector2_t structure
 *	#return Pointer to a vector2_t structure that is the sum of the two vectors.
 */
vector2_t *vector2_add(vector2_t *out, const vector2_t *v1, const vector2_t *v2);

/**
 *	Substract two 2D vectors.
 *	@param out Pointer to the vector2_t structure that is the result of the operation.
 *	@param v1 Pointer to a source vector2_t structure.
 *	@param v2 Pointer to a source vector2_t structure
 *	#return Pointer to a vector2_t structure that is the sum of the two vectors.
 */
vector2_t *vector2_substract(vector2_t *out, const vector2_t *v1, const vector2_t *v2);

/**
 *	Returns the square of the length of a 2D vector.
 *	@param v Pointer to the source vector2_t structure.
 *	@return The vector's squared length.
 */
float vector2_length_square(const vector2_t *v);

/**
 *	Returns the length of a 2D vector.
 *	@param v Pointer to the source vector2_t structure.
 *	@return The vector's length.
 */
float vector2_length(const vector2_t *v);

/**
 *	Scales a 2D vector.
 *	@param out Pointer to the vector2_t structure that is the result of the operation.
 *	@param v Pointer to the source vector2_t structure.
 *	@param s Scaling value.
 *	@return Pointer to a vector2_t structure that is the scaled vector.
 */
vector2_t *vector2_scale(vector2_t *out, const vector2_t *v, float s);

/**
 *	Returns the normalized version of a 2D vector.
 *	@param out Pointer to the vector2_t structure that is the result of the operation.
 *	@param v Pointer to the source vector2_t structure.
 *	@return Pointer to a vector2_t structure that is the normalized version of the vector.
 */
vector2_t *vector2_normalize(vector2_t *out, const vector2_t *v);

/**
 *	Returns a 2D vector that is made up of the smallest components of two 2D vectors.
 *	@param out Pointer to the vector2_t structure that is the result of the operation.
 *	@param v1 Pointer to the source vector2_t structure.
 *	@param v2 Pointer to the source vector2_t structure.
 *	@return Pointer to a vector2_t structure that is made up of the smallest components of the two vectors.
 */
vector2_t *vector2_minimize(vector2_t *out, const vector2_t *v1, const vector2_t *v2);

/**
 *	Returns a 2D vector that is made up of the largest components of two 2D vectors.
 *	@param out Pointer to the vector2_t structure that is the result of the operation.
 *	@param v1 Pointer to the source vector2_t structure.
 *	@param v2 Pointer to the source vector2_t structure.
 *	@return Pointer to a vector2_t structure that is made up of the largest components of the two vectors.
 */
vector2_t *vector2_maximize(vector2_t *out, const vector2_t *v1, const vector2_t *v2);

/**
 *	Transforms a 2D vector by a given matrix, projecting the result back into w = 1
 *	@param out Pointer to the vector2_t structure that is the result of the operation.
 *	@param v Pointer to the source vector2_t structure.
 *	@param m Pointer to the source matrix_t structure.
 *	@return Pointer to a vector2_t structure that is the transformed vector.
 */
vector2_t *vector2_transform(vector2_t *out, const vector2_t *v, const matrix_t *m);

/******************************************************************************
 * 3D Vector
 *****************************************************************************/

/**
 *	Dot product of two 3D vectors.
 *	@param v1 Pointer to a source vector3_t structure.
 *	@param v2 Pointer to a source vector3_t structure.
 *	@return A float representing the dot product value.
 */
float vector3_dot(const vector3_t *v1, const vector3_t *v2);

/**
 *	Taking the cross product of two 3D vectors.
 *	@param out Pointer to the vector3_t structure that is the result of the operation.
 *	@param v1 Pointer to a source vector3_t structure.
 *	@param v2 Pointer to a source vector3_t structure.
 *	@return Pointer to a vector3_t structure that is the cross product of two 3D vectors
 */
vector3_t *vector3_cross(vector3_t *out, const vector3_t *v1, const vector3_t *v2);

/**
 *	Adds two 3D vectors.
 *	@param out Pointer to the vector3_t structure that is the result of the operation.
 *	@param v1 Pointer to a source vector3_t structure.
 *	@param v2 Pointer to a source vector3_t structure
 *	#return Pointer to a vector3_t structure that is the sum of the two vectors.
 */
vector3_t *vector3_add(vector3_t *out, const vector3_t *v1, const vector3_t *v2);

/**
 *	Substract two 3D vectors.
 *	@param out Pointer to the vector3_t structure that is the result of the operation.
 *	@param v1 Pointer to a source vector3_t structure.
 *	@param v2 Pointer to a source vector3_t structure
 *	#return Pointer to a vector3_t structure that is the sum of the two vectors.
 */
vector3_t *vector3_substract(vector3_t *out, const vector3_t *v1, const vector3_t *v2);

/**
 *	Returns the square of the length of a 3D vector.
 *	@param v Pointer to the source vector3_t structure.
 *	@return The vector's squared length.
 */
float vector3_length_square(const vector3_t *v);

/**
 *	Returns the length of a 3D vector.
 *	@param v Pointer to the source vector3_t structure.
 *	@return The vector's length.
 */
float vector3_length(const vector3_t *v);

/**
 *	Scales a 3D vector.
 *	@param out Pointer to the vector3_t structure that is the result of the operation.
 *	@param v Pointer to the source vector3_t structure.
 *	@param s Scaling value.
 *	@return Pointer to a vector3_t structure that is the scaled vector.
 */
vector3_t *vector3_scale(vector3_t *out, const vector3_t *v, float s);

/**
 *	Returns the normalized version of a 3D vector.
 *	@param out Pointer to the vector2_t structure that is the result of the operation.
 *	@param v Pointer to the source vector3_t structure.
 *	@return Pointer to a vector3_t structure that is the normalized version of the vector.
 */
vector3_t *vector3_normalize(vector3_t *out, const vector3_t *v);

/**
 *	Returns a 3D vector that is made up of the smallest components of two 3D vectors.
 *	@param out Pointer to the vector3_t structure that is the result of the operation.
 *	@param v1 Pointer to the source vector3_t structure.
 *	@param v2 Pointer to the source vector3_t structure.
 *	@return Pointer to a vector3_t structure that is made up of the smallest components of the two vectors.
 */
vector3_t *vector3_minimize(vector3_t *out, const vector3_t *v1, const vector3_t *v2);

/**
 *	Returns a 3D vector that is made up of the largest components of two 3D vectors.
 *	@param out Pointer to the vector3_t structure that is the result of the operation.
 *	@param v1 Pointer to the source vector3_t structure.
 *	@param v2 Pointer to the source vector3_t structure.
 *	@return Pointer to a vector3_t structure that is made up of the largest components of the two vectors.
 */
vector3_t *vector3_maximize(vector3_t *out, const vector3_t *v1, const vector3_t *v2);

/**
 *	Transforms a 3D vector by a given matrix, projecting the result back into w = 1
 *	@param out Pointer to the vector3_t structure that is the result of the operation.
 *	@param v Pointer to the source vector3_t structure.
 *	@param m Pointer to the source matrix_t structure.
 *	@return Pointer to a vector3_t structure that is the transformed vector.
 */
vector3_t *vector3_transform(vector3_t *out, const vector3_t *v, const matrix_t *m);

/******************************************************************************
 * 4D Vector
 *****************************************************************************/

/**
 *	Dot product of two 4D vectors.
 *	@param v1 Pointer to a source vector4_t structure.
 *	@param v2 Pointer to a source vector4_t structure.
 *	@return A float representing the dot product value.
 */
float vector4_dot(const vector4_t *v1, const vector4_t *v2);

/**
 *	Taking the cross product of two 4D vectors.
 *	@param out Pointer to the vector4_t structure that is the result of the operation.
 *	@param v1 Pointer to a source vector4_t structure.
 *	@param v2 Pointer to a source vector4_t structure.
 *	@param v3 Pointer to a source vector4_t structure.
 *	@return Pointer to a vector4_t structure that is the cross product of two 3D vectors
 */
vector4_t *vector4_cross(vector4_t *out, const vector4_t *v1, const vector4_t *v2, const vector4_t *v3);

/**
 *	Adds two 4D vectors.
 *	@param out Pointer to the vector4_t structure that is the result of the operation.
 *	@param v1 Pointer to a source vector4_t structure.
 *	@param v2 Pointer to a source vector4_t structure
 *	#return Pointer to a vector4_t structure that is the sum of the two vectors.
 */
vector4_t *vector4_add(vector4_t *out, const vector4_t *v1, const vector4_t *v2);

/**
 *	Substract two 4D vectors.
 *	@param out Pointer to the vector4_t structure that is the result of the operation.
 *	@param v1 Pointer to a source vector4_t structure.
 *	@param v2 Pointer to a source vector4_t structure
 *	#return Pointer to a vector4_t structure that is the sum of the two vectors.
 */
vector4_t *vector4_substract(vector4_t *out, const vector4_t *v1, const vector4_t *v2);

/**
 *	Returns the square of the length of a 4D vector.
 *	@param v Pointer to the source vector4_t structure.
 *	@return The vector's squared length.
 */
float vector4_length_square(const vector4_t *v);

/**
 *	Returns the length of a 4D vector.
 *	@param v Pointer to the source vector4_t structure.
 *	@return The vector's length.
 */
float vector4_length(const vector4_t *v);

/**
 *	Scales a 4D vector.
 *	@param out Pointer to the vector4_t structure that is the result of the operation.
 *	@param v Pointer to the source vector4_t structure.
 *	@param s Scaling value.
 *	@return Pointer to a vector4_t structure that is the scaled vector.
 */
vector4_t *vector4_scale(vector4_t *out, const vector4_t *v, float s);

/**
 *	Returns the normalized version of a 4D vector.
 *	@param out Pointer to the vector2_t structure that is the result of the operation.
 *	@param v Pointer to the source vector4_t structure.
 *	@return Pointer to a vector4_t structure that is the normalized version of the vector.
 */
vector4_t *vector4_normalize(vector4_t *out, const vector4_t *v);

/**
 *	Returns a 4D vector that is made up of the smallest components of two 4D vectors.
 *	@param out Pointer to the vector4_t structure that is the result of the operation.
 *	@param v1 Pointer to the source vector4_t structure.
 *	@param v2 Pointer to the source vector4_t structure.
 *	@return Pointer to a vector4_t structure that is made up of the smallest components of the two vectors.
 */
vector4_t *vector4_minimize(vector4_t *out, const vector4_t *v1, const vector4_t *v2);

/**
 *	Returns a 4D vector that is made up of the largest components of two 4D vectors.
 *	@param out Pointer to the vector4_t structure that is the result of the operation.
 *	@param v1 Pointer to the source vector4_t structure.
 *	@param v2 Pointer to the source vector4_t structure.
 *	@return Pointer to a vector4_t structure that is made up of the largest components of the two vectors.
 */
vector4_t *vector4_maximize(vector4_t *out, const vector4_t *v1, const vector4_t *v2);

/**
 *	Transforms a 4D vector by a given matrix
 *	@param out Pointer to the vector4_t structure that is the result of the operation.
 *	@param v Pointer to the source vector4_t structure.
 *	@param m Pointer to the source matrix_t structure.
 *	@return Pointer to a vector4_t structure that is the transformed vector.
 */
vector4_t *vector4_transform(vector4_t *out, const vector4_t *v, const matrix_t *m);

/******************************************************************************
 * 4D Matrix
 *****************************************************************************/

/**
 *	Creates an identity matrix.
 *	@param out Pointer to the matrix_t structure that is the result of the operation.
 *	@result Pointer to a matrix_t structure that is the identity matrix.
 */
matrix_t *matrix_identity(matrix_t *out);

/**
 *	Determines if a matrix is an identity matrix.
 *	@param m Pointer to the matrix_t structure that will be tested for identity.
 *	@return If the matrix is an identity matrix, this function returns nonzero value.
 *			Otherwise, this function returns zero.
 */
int matrix_is_identity(const matrix_t *m);

/*
 *	Returns the determinant of a matrix.
 *	@param m Pointer to the source matrix_t structure.
 *	@return Returns the determinant of the matrix.
 */
float matrix_determinant(const matrix_t *m);

/*
 *	Returns the matrix transpose of a matrix.
 *	@param out Pointer to the matrix_t structure that is the result of the operation.
 *	@param m Pointer to the source matrix_t structure.
 *	@return Pointer to the matrix_t structure that is the matrix transpose of the matrix.
 */
matrix_t *matrix_transpose(matrix_t *out, const matrix_t *m);

/*
 *	Breaks down a general 3D transformation matrix into its translational, rotational, and scalar components.
 *	@param translation Pointer to the vector3_t structure that describes the translation.
 *	@param rotation Pointer to the quaternion_t structure that describes the rotation.
 *	@param scale Pointer to the output vector3_t that contains scaling factors applied along the x, y, and z-axes.
 *	@param m Pointer to an input martix_t matrix to decompose.
 */
void matrix_decompose(vector3_t *translation, quaternion_t *rotation, vector3_t *scale, const matrix_t *m);

/*
 *	Adds two 4D matrices.
 *	@param out Pointer to the matrix_t structure that is the result of the operation.
 *	@param m1 Pointer to the source matrix_t structure.
 *	@param m2 Pointer to the source matrix_t structure.
 *	@return Pointer to the resulting matrix_t.
 */
matrix_t *matrix_add(matrix_t *out, const matrix_t *m1, const matrix_t *m2);

/*
 *	Substracts two 4D matrices.
 *	@param out Pointer to the matrix_t structure that is the result of the operation.
 *	@param m1 Pointer to the source matrix_t structure.
 *	@param m2 Pointer to the source matrix_t structure.
 *	@return Pointer to the resulting matrix_t.
 */
matrix_t *matrix_substract(matrix_t *out, const matrix_t *m1, const matrix_t *m2);

/*
 *	Determines the product of two matrices.
 *	@param out Pointer to the matrix_t structure that is the result of the operation.
 *	@param m1 Pointer to a source matrix_t structure.
 *	@param m2 Pointer to a source matrix_t structure.
 *	@return Pointer to a matrix_t structure that is the product of two matrices.
 */
matrix_t *matrix_multiply(matrix_t *out, const matrix_t *m1, const matrix_t *m2);

/*
 *	Calculates the inverse of a matrix.
 *	@param  out Pointer to the matrix_t structure that is the result of the operation.
 *	@param m Pointer to a source matrix_t structure.
 *	@return Pointer to a matrix_t structure that is the inverse of the matrix. If matrix inversion fails, 0 is returned.
 */
matrix_t *matrix_inverse(matrix_t *out, const matrix_t *m);

/*
 *	Builds a matrix that scales along the x-axis, the y-axis, and the z-axis.
 *	@param out Pointer to the matrix_t structure that is the result of the operation.
 *	@param sx Scaling factor that is applied along the x-axis.
 *	@param sy Scaling factor that is applied along the y-axis.
 *	@param sz Scaling factor that is applied along the z-axis.
 *	@return Pointer to the scaling transformation matrix_t.
 */
matrix_t *matrix_scalling(matrix_t *out, float sx, float sy, float sz);

/*
 *	Builds a matrix using the specified offsets.
 *	@param out Pointer to the matrix_t structure that is the result of the operation.
 *	@param x X-coordinate offset.
 *	@param y Y-coordinate offset.
 *	@param z Z-coordinate offset.
 *	@return Pointer to the matrix_t structure that contains a translated transformation matrix.
 */
matrix_t *matrix_translation(matrix_t *out, float x, float y, float z);

/*
 *	Builds a matrix that rotates around an arbitrary axis.
 *	@param out Pointer to the matrix_t structure that is the result of the operation.
 *	@param v Pointer to the arbitrary axis. See matrix_t.
 *	@param angle Angle of rotation in radians. Angles are measured clockwise when looking along the rotation axis toward the origin.
 *	@return Pointer to a matrix_t structure rotated around the specified axis.
 */
matrix_t *matrix_rotation_axis(matrix_t *out, const vector3_t *v, float angle);

/*
 *	Builds a rotation matrix from a quaternion.
 *	@param out Pointer to the matrix_t structure that is the result of the operation.
 *	@param q Pointer to the source quaternion_t structure.
 *	@return Pointer to a matrix_t structure built from the source quaternion.
 */
matrix_t *matrix_rotation_quaternion(matrix_t *out, const quaternion_t *q);

/*
 *	Builds a matrix with a specified yaw, pitch, and roll.
 *	@param out Pointer to the matrix_t structure that is the result of the operation.
 *	@param yaw Yaw around the y-axis, in radians.
 *	@param pitch Pitch around the x-axis, in radians.
 *	@param roll Roll around the z-axis, in radians.
 *	@return Pointer to a matrix_t structure with the specified yaw, pitch, and roll.
 */
matrix_t *matrix_rotation_yaw_pitch_roll(matrix_t *out, float yaw, float pitch, float roll);

/*
 *	Builds a transformation matrix. NULL arguments are treated as identity transformations.
 *	@param out Pointer to the matrix_t structure that is the result of the operation.
 *	@param translation Pointer to a vector3_t structure, representing the translation.
 *	@param rotation Pointer to a quaternion_t structure that specifies the rotation.
 *	@param scale Pointer to a vector3_t structure, the scaling vector.
 *	@return Pointer to a matrix_t structure that is the transformation matrix.
 */
matrix_t *matrix_transformation(matrix_t *out, const vector3_t *translation,
	const quaternion_t *rotation, const vector3_t *scale);

/*
 *	Builds a right-handed, look-at matrix.
 *	@param out Pointer to the matrix_t structure that is the result of the operation.
 *	@param eye Pointer to the vector3_t structure that defines the eye point. This value is used in translation.
 *	@param at Pointer to the vector3_t structure that defines the camera look-at target.
 *	@param up Pointer to the vector3_t structure that defines the current world's up, usually [0, 1, 0].
 *	@return Pointer to a matrix_t structure that is a left-handed, look-at matrix.
 */
matrix_t *matrix_look_at(matrix_t *out, const vector3_t *eye,
	const vector3_t *at, const vector3_t *up);

/*
 *	Builds a right-handed perspective projection matrix.
 *	@param out Pointer to the matrix_t structure that is the result of the operation.
 *	@param w Width of the view volume at the near view-plane.
 *	@param h Height of the view volume at the near view-plane.
 *	@param zn Z-value of the near view-plane.
 *	@param zf Z-value of the far view-plane.
 *	@return Pointer to a matrix_t structure that is a left-handed perspective projection matrix.
 */
matrix_t *matrix_perspective(matrix_t *out, float w, float h, float zn, float zf);

/*
 *	Builds a right-handed perspective projection matrix based on a field of view.
 *	@param out Pointer to the matrix_t structure that is the result of the operation.
 *	@param fov Field of view in the y direction, in radians.
 *	@param aspect Aspect ratio, defined as view space width divided by height.
 *	@param zn Z-value of the near view-plane.
 *	@param zf Z-value of the far view-plane.
 *	@return Pointer to a matrix_t structure that is a left-handed perspective projection matrix.
 */
matrix_t *matrix_perspective_fov(matrix_t *out, float fov, float aspect, float zn, float zf);

/*
 *	Builds a right-handed orthographic projection matrix.
 *	@param out Pointer to the matrix_t structure that is the result of the operation.
 *	@param w Width of the view volume.
 *	@param h Height of the view volume.
 *	@param zn Minimum z-value of the view volume which is referred to as z-near.
 *	@param zf Maximum z-value of the view volume which is referred to as z-far.
 *	@return Pointer to the resulting matrix_t.
 */
matrix_t *matrix_ortho(matrix_t *out, float w, float h, float zn, float zf);

/******************************************************************************
 * Quaternion
 *****************************************************************************/

/**
 *	Returns the identity quaternion.
 *	@param out Pointer to the quaternion_t structure that is the result of the operation.
 *	@return Pointer to the quaternion_t structure that is the identity quaternion.
 */
quaternion_t *quaternion_identity(quaternion_t *out);

/**
 *	Determines if a quaternion is an identity quaternion.
 *	@param q Pointer to the quaternion_t structure that will be tested for identity.
 *	@return If the quaternion is an identity quaternion, this function returns non zero value.
 */
int quaternion_is_identity(const quaternion_t *q);

/**
 *	Returns the length of a quaternion.
 *	@param q Pointer to the source quaternion_t structure.
 *	@return The quaternion's length.
 */
float quaternion_length(const quaternion_t *q);

/**
 *	Returns the square of the length of a quaternion.
 *	@param q Pointer to the source quaternion_t structure.
 *	@return The quaternion's squared length.
 */
float quaternion_length_square(const quaternion_t *q);

/**
 *	Computes a quaternion's axis and angle of rotation.
 *	@param q Pointer to the source quaternion_t structure.
 *	@param axis A pointer to a vector3_t structure that identifies the quaternion's axis of rotation.
 *	@param angle A pointer to a float value that identifies the quaternion's angle of rotation in radians.
 */
void quaternion_to_axis_angle(const quaternion_t *q, vector3_t *axis, float *angle);

/**
 *	Gets yaw, pitch and roll rotations from the given quaternion.
 *	@param q The pointer to the source quaternion_t structure.
 *	@param yaw Pointer to a float value that identifies the quaternion's angle of yaw rotation in radians.
 *	@param pitch Pointer to a float value that identifies the quaternion's angle of pitch rotation in radians.
 *	@param roll Pointer to a float value that identifies the quaternion's angle of roll rotation in radians.
 */
void quaternion_to_yaw_pitch_roll(const quaternion_t *q, float *yaw, float *pitch, float *roll);

/**
 *	Builds a quaternion from a rotation matrix.
 *	@param out Pointer to the quaternion_t structure that is the result of the operation.
 *	@param m Pointer to the source matrix_t structure.
 *	@return Pointer to the quaternion_t structure built from a rotation matrix.
 */
quaternion_t *quaternion_rotation_matrix(quaternion_t *out, const matrix_t *m);

/**
 *	Rotates a quaternion about an arbitrary axis.
 *	@param out Pointer to the quaternion_t structure that is the result of the operation.
 *	@param axis Pointer to the vector3_t structure that identifies the axis about which to rotate the quaternion.
 *	@param angle Angle of rotation, in radians. Angles are measured clockwise when looking along the rotation axis toward the origin.
 *	@return Pointer to a quaternion_t structure rotated around the specified axis.
 */
quaternion_t *quaternion_rotation_axis(quaternion_t *out, const vector3_t *axis, float angle);

/**
 *	Multiplies two quaternions.
 *	@param out Pointer to the quaternion_t structure that is the result of the operation.
 *	@param q1 Pointer to a source quaternion_t structure.
 *	@param q2 Pointer to a source quaternion_t structure.
 *	@return Pointer to a quaternion_t structure that is the product of two quaternions.
 */
quaternion_t *quaternion_multiply(quaternion_t *out, const quaternion_t *q1, const quaternion_t *q2);

/**
 *	Builds a quaternion with a specified yaw, pitch, and roll.
 *	@param out Pointer to the quaternion_t structure that is the result of the operation.
 *	@param yaw Yaw around the y-axis, in radians.
 *	@param pitch Pitch around the x-axis, in radians.
 *	@param roll Roll around the z-axis, in radians.
 *	@return Pointer to a quaternion_t structure with the specified yaw, pitch, and roll.
 */
quaternion_t *quaternion_rotation_yaw_pitch_roll(quaternion_t *out, float yaw, float pitch, float roll);

/**
 *	Computes a unit length quaternion.
 *	@param out Pointer to the quaternion_t structure that is the result of the operation.
 *	@param q Pointer to a source quaternion_t structure.
 *	@return Pointer to a quaternion_t structure that is the normal of the quaternion.
 */
quaternion_t *quaternion_normalize(quaternion_t *out, const quaternion_t *q);

/**
 *	Conjugates and renormalizes a quaternion.
 *	@param out Pointer to the quaternion_t structure that is the result of the operation.
 *	@param q Pointer to a source quaternion_t structure.
 *	@return Pointer to a quaternion_t structure that is the inverse quaternion of the quaternion.
 */
quaternion_t *quaternion_inverse(quaternion_t *out, const quaternion_t *q);

/******************************************************************************
 * Plane
 *****************************************************************************/

/**
 *	Computes the dot product of a plane and a 4D vector.
 *	@param p Pointer to a source plane_t structure.
 *	@param v Pointer to a source vector4_t structure.
 *	@return The dot product of the plane and 4D vector.
 */
float plane_dot(const plane_t *p, const vector4_t *v);

/**
 *	Computes the dot product of a plane and a 3D vector.
 *	The w parameter of the vector is assumed to be 1.
 *	@param p Pointer to a source plane_t structure.
 *	@param v Pointer to a source vector3_t structure.
 *	@return The dot product of the plane and 3D vector.
 */
float plane_dot_coord(const plane_t *p, vector3_t *v);

/**
 *	Computes the dot product of a plane and a 3D vector.
 *	The w parameter of the vector is assumed to be 0.
 *	@param p Pointer to a source plane_t structure.
 *	@param v Pointer to a source vector3_t structure.
 *	@return The dot product of the plane and 3D vector.
 */
float plane_dot_normal(const plane_t *p, vector3_t *v);

/**
 *	Normalizes the plane coefficients so that the plane normal has unit length.
 *	@param out Pointer to the plane_t structure that is the result of the operation.
 *	@param p Pointer to a source plane_t structure.
 *	@return Pointer to a plane_t structure that represents the normal of the plane.
 */
plane_t *plane_normalize(plane_t *out, const plane_t *p);

/**
 *	Finds the intersection between a plane and a line.
 *	@param out Pointer to a vector3_t structure, identifying the intersection between the specified plane and line.
 *	@param p Pointer to a source plane_t structure.
 *	@param p1 Pointer to a source vector3_t structure, defining a line starting point.
 *	@param p2 Pointer to a source vector3_t structure, defining a line ending point.
 *	@return Pointer to a vector3_t structure that is the intersection between the specified plane and line.
 */
vector3_t *plane_intersect_line(vector3_t *out, const plane_t *p, const vector3_t *p1, const vector3_t *p2);

/**
 *	Constructs a plane from a point and a normal.
 *	@param out Pointer to the plane_t structure that is the result of the operation.
 *	@param p Pointer to a vector3_t, defining the point used to construct the plane.
 *	@param n Pointer to a vector3_t structure, defining the normal used to construct the plane.
 *	@return
 *		Pointer to the plane_t structure constructed from the point and the normal.
 *		If the line is parallel to the plane, NULL is returned.
 */
plane_t *plane_from_point_normal(plane_t *out, const vector3_t *p, const vector3_t *n);

/**
 *	Constructs a plane from three points.
 *	@param out Pointer to the plane_t structure that is the result of the operation.
 *	@param p1 Pointer to a vector3_t structure, defining one of the points used to construct the plane.
 *	@param p2 Pointer to a vector3_t structure, defining one of the points used to construct the plane.
 *	@param p3 Pointer to a vector3_t structure, defining one of the points used to construct the plane.
 *	@return Pointer to the plane_t structure constructed from the given points.
 */
plane_t *plane_from_points(plane_t *out, const vector3_t *p1, const vector3_t *p2, const vector3_t *p3);

/**
 *	Transforms a plane by a matrix. The input matrix is the inverse transpose of the actual transformation.
 *	@param out Pointer to the plane_t structure that is the result of the operation.
 *	@param p
 *		Pointer to a source plane_t structure, which contains the plane that will be transformed.
 *		The vector (a,b,c) that describes the plane must be normalized before this function is called.
 *	@param m
 *		Pointer to the source matrix_t structure, which contains the transformation values.
 *		This matrix must contain the inverse transpose of the transformation values.
 */
plane_t *plane_transform(plane_t *out, const plane_t *p, const matrix_t *m);

#endif // __DRAFT_MATHS_H__