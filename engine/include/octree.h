/******************************************************************************
 *	File:			octree.h
 *
 *	Created by:		Ivan Shishkin <codingdude@gmail.com>
 *
 *	History:		Created 25/03/13
 *
 *	Copyright (c) 2013 All rights reserved
 *
 *****************************************************************************/

#ifndef __DRAFT_OCTREE_H__
#define __DRAFT_OCTREE_H__

/**
 *	Initializes the octree to the given box and depth.
 *	@param oct Pointer to the given octree_t structure.
 *	@param bbox The initial bounding box.
 *	@param depth The initial depth.
 *	@return The pointer to the octree_t structure which is a result of operation.
 */
octree_t *octree_init(octree_t *oct, const bbox_t *bbox, int depth);

/**
 *	Destroys the given octree.
 *	@param oct The pointer to the given octree_t structure.
 */
void octree_destroy(octree_t *oct);

/**
 *	Adds the Scene Node, starting at the given octree, and recursing at max to the specified depth.
 *	@param oct The pointer to the given octree_t structure.
 *	@param node The pointer to the given scene_node_t structure.
 *	@param depth The specified depth.
 */
void octree_add_node(octree_t *oct, scene_node_t *node, int depth);

/**
 *	Removes the given Scene Node.
 *	@param node The pointer to the given scene_node_t structure.
 */
void octree_remove_node(scene_node_t *node);

/**
 *	Checks the given Scene Node, and determines if it needs to be moved to a different octant.
 *	@param oct The pointer to the given octree_t structure.
 *	@param node The pointer to the given scene_node_t structure.
 */
void octree_update_node(octree_t *oct, scene_node_t *node);

/**
 *	Walks through the octree, adding any visible objects to the render queue.
 *	@param oct The pointer to the given octree_t structure.
 *	@param cam The pointer to the camera_t structure, which represents the currently active camera.
 *	@param queue The render queue, which is a result of operation.
 */
void octree_walk_tree(const octree_t *oct, camera_t *cam, array_t *queue);

#endif // __DRAFT_OCTREE_H__