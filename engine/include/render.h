/******************************************************************************
 *	File:			render.h
 *
 *	Created by:		Ivan Shishkin <codingdude@gmail.com>
 *
 *	History:		Created 25/11/12
 *
 *	Copyright (c) 2012 All rights reserved
 *
 *****************************************************************************/

#ifndef __DRAFT_RENDER_H__
#define __DRAFT_RENDER_H__

#include <types.h>

/**
 *	Initializes the render system and the drawing context.
 *	@param hwnd Handle of a platform specified viewport.
 *	This value can be NULL, if a target platform initializes rendering context by itself.
 */
void render_init(void *hwnd);

/**
 *	shutting down the render system.
 */
void render_shutdown();

/**
 *	Resizes current viewport.
 *	@param w The new width.
 *	@param h The new height.
 */
void render_resize(int w, int h);

/**
 *	Sets the current clear color.
 *	@param color The pointer to the vector4_t which represents a color value.
 */
void render_set_clear_color(const vector4_t *color);

/**
 *	Swaps the back buffer.
 */
void render_swap_buffers();

/**
 *	Stets the transformation matrix.
 *	@param type The type of transformations, which is one of a matrix_type_t enumeration
 *	@param m Pointer to the given matrix_t structure.
 */
void render_bind_matrix(int type, const matrix_t *m);

/**
 *	Creates a hardware vertex buffer.
 *	@param vsize The size of a buffer in bytes.
 *	@param vcount The total count of a vertices.
 *	@param vdata The vertex data to be written in.
 *	@return If success the return value is the buffer identifier, otherwise it returns 0.
 */
int render_create_vertex_buffer(int vsize, int vcount, const void *vdata);

/**
 *	Creates a hardware index buffer.
 *	@param count The total count of an indices.
 *	@param data The index data to be written in.
 *	@return If success the return value is the buffer identifier, otherwise it returns 0.
 */
int render_create_index_buffer(int count, const void *data);

/**
 *	Destroys the given hardware vertex buffer.
 *	@param vb Identifier of the given buhher.
 */
void render_destroy_vertex_buffer(int vb);

/**
 *	Destroys the given hardware index buffer.
 *	@param vb Identifier of the given buhher.
 */
void render_destroy_index_buffer(int ib);

/**
 *	Creates a 2D hardware texture array.
 *	@param w Source image width.
 *	@param h Source image height.
 *	@param array_size The size of an texture array.
 *	@param mipmap The number of a mip-levels.
 *	@param int channel The given pixel channel.
 *	@param int format The given pixel format.
 *	@param flags Creation flags, which is set of a texmap_flags_t enumerations
 *	@param data The pointer to the source image data.
 *	@return If success the return value is the hardware texture identifier,
 *	otherwise it returns 0.
 */
int render_create_texmap_2d(int w, int h, int array_size,
	int mipmaps, int channel, int format, int flags, const void *data);

/**
 *	Creates a 3D hardware texture.
 *	@param w Source image width.
 *	@param h Source image height.
 *	@param d Source image depth.
 *	@param mipmap The number of a mip-levels.
 *	@param int channel The given pixel channel.
 *	@param int format The given pixel format.
 *	@param flags Creation flags, which is set of a texmap_flags_t enumerations
 *	@param data The pointer to the source image data.
 *	@return If success the return value is the hardware texture identifier,
 *	otherwise it returns 0
 */
int render_create_texmap_3d(int w, int h, int d,
	int mipmaps, int channel, int format, int flags, const void *data);

/**
 *	Destroys the given hardware texture.
 *	@param tmap Identifier of a given hardware texture.
 */
void render_destroy_texmap(int tmap);

/**
 *	Binds the given hardware texture.
 *	@param id Identifier of the given hardware texture.
 *	@param flags Creation flags of the given texture.
 */
void render_bind_texmap(int id, int flags);

/**
 *	Draws indexed primitive.
 *	@param vb Hardware vertex buffer identifier.
 *	@parram vdecl The vertex declaration.
 *	@param ib Hardware index buffer identifier.
 *	@param count Primitive count.
 */
void render_draw(int vb, int vdecl, int ib, int count);



#endif // __DRAFT_RENDER_H__