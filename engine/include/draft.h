/******************************************************************************
 *	File:			draft.h
 *
 *	Created by:		Ivan Shishkin <codingdude@gmail.com>
 *
 *	History:		Created 02/05/13
 *
 *	Copyright (c) 2013 All rights reserved
 *
 *****************************************************************************/

#ifndef __DRAFT_H__
#define __DRAFT_H__

#if defined(__cplusplus)
extern "C" {
#endif // __cplusplus

#include "platform.h"
#include "types.h"
#include "debug.h"
#include "dslib.h"
#include "maths.h"
#include "collision.h"
#include "filesystem.h"
#include "resource.h"
#include "render.h"
#include "scene.h"

#if defined(__cplusplus)
}
#endif // __cplusplus

#endif // __DRAFT_H__