/******************************************************************************
 *	File:			filesystem.h
 *
 *	Created by:		Ivan Shishkin <codingdude@gmail.com>
 *
 *	History:		Created 28/12/12
 *
 *	Copyright (c) 2012 All rights reserved
 *
 *****************************************************************************/

#ifndef __DRAFT_FILESYSTEM_H__
#define __DRAFT_FILESYSTEM_H__

/******************************************************************************
*	File System
******************************************************************************/

/**
 *	Initializes the file sytem
 *	@param fs_config Path to configuration file, This value might be NULL
 */
void fs_init(const char *fs_config);

/**
 *	Shutting down the file system
 */
void fs_shutdown();

/**
 *	Opens a reader which represent an abstract binary stream of data
 *	@return Pointer to the reader_t structure, which is a result of operation
 */
reader_t *fs_reader_open();

/**
 *	Closes the reader which represent an abstract binary stream of data
 *	@param r Pointer to the given reader_t structure
 */
void fs_reader_close(reader_t *r);

/**
 *	Gets total number of path aliases
 *	@return Total number of path aliases
 */
int fs_get_alias_count();

/**
 *	Gets the path alias by the given index
 *	@return The null-terminated string, which is the path alias.
 */
const char *fs_get_alias(int index);

/**
 *	Gets the archive file name.
 *	@param buffer Pointer to the data storage location
 *	@param length The length of the data storage in bytes.
 *	@return The null-terminated string, which is archieve name.
 */
const char *fs_get_archive(char *buffer, int length);

/**
 *	Gets the full path by alias.
 *	@param alias The path alias.
 *	@param buffer Pointer to the data storage location.
 *	@param length The length of the data storage in bytes. 
 *	@return The null-terminated string, which is the path alias.
 */
const char *fs_get_path(const char *alias, char *buffer, int length);

/**
 *	Loads content of a file using a file name and a path alias
 *	@param r Pointer to the reader_t structure
 *	@param alias A short path alias
 *	@param filename The source file name
 *	@return Nonzero value on success
 */
int fs_reader_load(reader_t *r, const char *alias, const char *filename);

/**
 *	Loads content of a file using a complete file name
 *	@param r Pointer to the reader_t structure
 *	@param filename Full path to the source file. 
 *	@return Nonzero value on success
 */
int fs_reader_load_file(reader_t *r, const char *filename);

/**
 *	Opens a writer which represent an abstract binary stream of data
 *	@return Pointer to the writer_t structure which is a result of operation.
 */
writer_t *fs_writer_open();

/**
 *	Closes the writer which represent an abstract binary stream of data
 *	@param w Pointer to the given writer_t structure.
 */
void fs_writer_close(writer_t *w);

/**
 *	Stores the data onto afile using a path alias and a file name.
 *	@param w Pointer to the given writer_t structure
 *	@param alias A short path alias
 *	@aram filename The target file name.
 *	@return Nonzero value on success.
 */
int fs_writer_save(writer_t *w, const char *alias, const char *filename);

/**
 *	Stores the data onto a file using a full file name.
 *	@param w Pointer to the given writer_t structure
 *	@aram filename The full path to the target file.
 *	@return Nonzero value on success.
 */
int fs_writer_save_file(writer_t *w, const char *filename);

/******************************************************************************
*	Reader
******************************************************************************/

/**
 *	Clears the contents of the reader.
 *	@param r Pointer to the given reader_t structure.
 */
void reader_clear(reader_t *r);

/**
 *	Searches for a child chunk with the given identifier,
 *	which is only refers to the data of the parent chunk.
 *	@param r Pointer to a given reader_t structure
 *	@param id The given chunk identifier.
 *	@param fromwhere If nonsero the searching starts from the current position
 *	@return The pointer to the reader_t structure which is a result of operation.
 *		If nothing founded, then returns NULL.
 */
reader_t *reader_find(reader_t *r, int id, int fromhere);

/**
 *	Copies data from the chunk with the specified identifier onto another.
 *	@param source Pointer to the source reader_t structure,
 *		which is inspected for a child with the given id.
 *	@param target Pointer to the target reader_t structure.
 *	@param id The given identifier
 *	@return Zero value on success.
 */
int reader_copy(reader_t *source, reader_t *target, int id);

/**
 *	Decompress compressed data from the chunk onto another.
 *	@param source Pointer to the source reader_t structure.
 *	@return Pointer to the reader_t structure, which is a result of operation.
 */
reader_t *reader_decompress(reader_t *source);

/**
 *	Determines whether or not the chunk data is compressed.
 *	@param Pointer to the given reader_t structure.
 *	@return Nonzero value if the chunk data is compressed.
 */
int reader_compressed(reader_t *r);

/**
 *	Returns the chunk identifier.
 *	@param r Pointer to the given reader_t structure.
 *	$return The chunk identifier.
 */
int reader_id(reader_t *r);

/**
 *	Returns the size of a chunk data.
 *	@param r Pointer to the given reader_t structure.
 *	$return The chunk data size.
 */
int reader_size(reader_t *r);

/**
 *	Returns a pointer to the begining of the data block.
 *	@param r The pointer to the given reader_t structure.
 *	@return A pointer to the begining of the data block.
 */
void *reader_data(reader_t *r);

/**
 *	Returns a current data pointer.
 *	@param r The pointer to the given reader_t structure.
 *	@return The current data pointer.
 */
void *reader_ptr(reader_t *r);

/**
 *	Moves data pointer to the specified location.
 *	@param r Pointer to the given reader_t structure.
 *	@param offset Number of bytes from the initial position.
 *	@return If success, returns 0, otherwise it returns a nonzero value.
 */
int reader_seek(reader_t *r, int offset);

/**
 *	Moves data pointer to the specified location.
 *	@param r Pointer to the given reader_t structure.
 *	@param offset Number of bytes from the icurrent position.
 *	@return If success, returns 0, otherwise it returns a nonzero value.
 */
int reader_advance(reader_t *r, int offset);

/**
 *	Gets the current position of a data pointer.
 *	@param r Pointer to the given reader_t structure.
 *	@return The current position of a data pointer.
 */
int reader_tell(reader_t *r);

/**
 *	Gets the number of bytes to the end of a data block.
 *	@param r Pointer to the given reader_t structure.
 *	@return The number of bytes to the end of a data block
 */
int reader_elapsed(reader_t *r);

/**
 *	Determines whether or not the end of a data block is reached.
 *	@param r Pointer to the given reader_t structure.
 *	@return Nonsero value if the end of a data block is reached.
 */
int reader_eof(reader_t *r);

/**
 *	Reads data from the curent position of a data pointer.
 *	@param r Pointer to the given reader_t structure.
 *	@param data Storage location for data.
 *	@param size The number of bytes to be read.
 *	@return The number of bytes actually read.
 */
int reader_read(reader_t *r, void *data, int size);

/**
 *	Reads unsigned 8-bit integer value.
 *	@param r Pointer to the given reader_t structure.
 *	@return An uint8_t value.
 */
uint8_t reader_r_u8(reader_t *r);

/**
 *	Reads signed 8-bit integer value.
 *	@param r Pointer to the given reader_t structure.
 *	@return An int8_t value.
 */
int8_t reader_r_s8(reader_t *r);

/**
 *	Reads unsigned 16-bit integer value.
 *	@param r Pointer to the given reader_t structure.
 *	@return An uint16_t value.
 */
uint16_t reader_r_u16(reader_t *r);

/**
 *	Reads signed 16-bit integer value.
 *	@param r Pointer to the given reader_t structure.
 *	@return An int16_t value.
 */
int16_t reader_r_s16(reader_t *r);

/**
 *	Reads unsigned 32-bit integer value.
 *	@param r Pointer to the given reader_t structure.
 *	@return An uint32_t value.
 */
uint32_t reader_r_u32(reader_t *r);

/**
 *	Reads signed 32-bit integer value.
 *	@param r Pointer to the given reader_t structure.
 *	@return An int32_t value.
 */
int32_t reader_r_s32(reader_t *r);

/**
 *	Reads unsigned 64-bit integer value.
 *	@param r Pointer to the given reader_t structure.
 *	@return An uint64_t value.
 */
uint64_t reader_r_u64(reader_t *r);

/**
 *	Reads signed 64-bit integer value.
 *	@param r Pointer to the given reader_t structure.
 *	@return An int64_t value.
 */
int64_t reader_r_s64(reader_t *r);

/**
 *	Reads 32-bit floating point value.
 *	@param r Pointer to the given reader_t structure.
 *	@return A float value.
 */
float reader_r_f32(reader_t *r);

/**
 *	Reads a null terminated string.
 *	@param r Pointer to the given reader_t structure.
 *	@return Pointer to a null-terminated string.
 */
const char *reader_r_sz(reader_t *r);

/******************************************************************************
*	Writer
******************************************************************************/

/**
 *	Clears the contents of the writer.
 *	@param r Pointer to the given writer_t structure.
 */
void writer_clear(writer_t *w);

/**
 *	Compress chunk data into another chunk.
 *	@param source Pointer to the source writer_t structure.
 *	@return Pointer to the writer_t structure, which is a result of operation.
 */
writer_t *writer_compress(writer_t *source);

/**
 *	Appends chunk data at the end of another chunk.
 *	@param source Pointer to the source writer_t structure.
 *	@param target Pointer to the target writer_t structure.
 *	@return If success, returns 0, otherwise returns nonzero value. 
 */
int writer_append(writer_t *source, writer_t *target);

/**
 *	Compress and append compressed chunk data at the end of another chunk.
 *	@param source Pointer to the source writer_t structure.
 *	@param target Pointer to the target writer_t structure.
 *	@return If success, returns 0, otherwise returns nonzero value. 
 */
int writer_append_compressed(writer_t *soure, writer_t *target);

/**
 *	Sets the chunk identifier.
 *	@param w Pointer to the given writer_t structure.
 *	@param id The chunk identifier.
 *	@return If success, returns 0, otherwise returns nonzero value.  
 */
int writer_set_id(writer_t *w, int id);

/**
 *	Moves data pointer to the specified location.
 *	@param r Pointer to the given writer_t structure.
 *	@param offset Number of bytes from the initial position.
 *	@return If success, returns 0, otherwise it returns a nonzero value.
 */
int writer_seek(writer_t *w, int offset);

/**
 *	Moves data pointer to the specified location.
 *	@param r Pointer to the given writer_t structure.
 *	@param offset Number of bytes from the current position.
 *	@return If success, returns 0, otherwise it returns a nonzero value.
 */
int writer_advance(writer_t *w, int offset);

/**
 *	Gets the current position of a data pointer.
 *	@param r Pointer to the given writer_t structure.
 *	@return The current position of a data pointer.
 */
int writer_tell(writer_t *w);

/**
 *	Writes data to a chunk.
 *	@param w Pointer to the given writer_t structure.
 *	@param data Pointer to the data to be written.
 *	@param size Number of bytes to be written.
 *	@return The number of bytes actually written.
 */
int writer_write(writer_t *w, void *data, int size);

/**
 *	Writes unsigned 8-bit integer value.
 *	@param w Pointer to the given writer_t structure.
 *	@param val A given uint8_t value.
 *	@return If success, returns 0, otherwise it returns a nonzero value.
 */
int writer_w_u8(writer_t *w, uint8_t val);

/**
 *	Writes signed 8-bit integer value.
 *	@param w Pointer to the given writer_t structure.
 *	@param val A given int8_t value.
 *	@return If success, returns 0, otherwise it returns a nonzero value.
 */
int writer_w_s8(writer_t *w, int8_t val);

/**
 *	Writes unsigned 16-bit integer value.
 *	@param w Pointer to the given writer_t structure.
 *	@param val A given uint16_t value.
 *	@return If success, returns 0, otherwise it returns a nonzero value.
 */
int writer_w_u16(writer_t *w, uint16_t val);

/**
 *	Writes signed 16-bit integer value.
 *	@param w Pointer to the given writer_t structure.
 *	@param val A given int16_t value.
 *	@return If success, returns 0, otherwise it returns a nonzero value.
 */
int writer_w_s16(writer_t *w, int16_t val);

/**
 *	Writes unsigned 32-bit integer value.
 *	@param w Pointer to the given writer_t structure.
 *	@param val A given uint32_t value.
 *	@return If success, returns 0, otherwise it returns a nonzero value.
 */
int writer_w_u32(writer_t *w, uint32_t val);

/**
 *	Writes signed 32-bit integer value.
 *	@param w Pointer to the given writer_t structure.
 *	@param val A given int32_t value.
 *	@return If success, returns 0, otherwise it returns a nonzero value.
 */
int writer_w_s32(writer_t *w, int32_t val);

/*
 *
 */
int writer_w_u64(writer_t *w, uint64_t val);

/**
 *	Writes unsigned 64-bit integer value.
 *	@param w Pointer to the given writer_t structure.
 *	@param val A given uint64_t value.
 *	@return If success, returns 0, otherwise it returns a nonzero value.
 */
int writer_w_s64(writer_t *w, int64_t val);

/**
 *	Writes signed 32-bit floating point value.
 *	@param w Pointer to the given writer_t structure.
 *	@param val A given float value.
 *	@return If success, returns 0, otherwise it returns a nonzero value.
 */
int writer_w_f32(writer_t *w, float val);

/**
 *	Writes a null-terminated string.
 *	@param w Pointer to the given writer_t structure.
 *	@param sz A pointer to the given null-terminated string.
 *	@return If success, returns 0, otherwise it returns a nonzero value.
 */
int writer_w_sz(writer_t *w, const char *sz);

#endif // __DRAFT_FILESYSTEM_H__