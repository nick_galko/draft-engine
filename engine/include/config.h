/******************************************************************************
 *	File:			config.h
 *
 *	Created by:		Ivan Shishkin <codingdude@gmail.com>
 *
 *	History:		Created 07/02/13
 *
 *	Copyright (c) 2013 All rights reserved
 *
 *****************************************************************************/

#ifndef __DRAFT_CONFIG_H__
#define __DRAFT_CONFIG_H__

/**
 *	Opens a new configuration.
 *	@return Pointer to the config_t structure.
 */
config_t config_open();

/**
 *	Closes the configuration.
 *	@param cfg Pointer to the given config_t structure.
 *	
 */
void config_close(config_t cfg);

/**
 *	Loads configuration from file.
 *	@param cfg The pointer to the config_t structure what is the given configuration.
 *	@param file The source file name.
 *	@return If the function succeeds, the return value is nonzero.
 */
int config_load(config_t cfg, const char *file);

/**
 *	Save configuration to a file.
 *	@param cfg The pointer to the given config_t structure.
 *	@param file The target file name.
 *	@return If the function succeeds, the return value is nonzero.
 */
int config_save(config_t cfg, const char *file);

/**
 *	Parses the given file.
 *	@param cfg Pointer to the config_t structure what is the result of operation.
 *	@param f A pointer to the FILE structure, what is the opened file descriptor.
 *	@return If the function succeeds, the return value is nonzero.
 */
int config_parse_file(config_t cfg, FILE *f);

/**
 *	Saves configuration to the given file.
 *	@param cfg The pointer to the given config_t structure.
 *	@param f A pointer to the FILE structure, what is the opened file descriptor.
 */
void config_save_file(config_t cfg, FILE *f);

/**
 *	Clears the given configuration.
 *	@param cfg Pointer to the given config_t structure.
 */
void config_clear(config_t cfg);

/**
 *	Sets the value to the given configuration.
 *	@param cfg The pointer to the given config_t structure.
 *	@param section The section name.
 *	@param key The key name.
 *	@param value The value.
 */
void config_set_value(config_t cfg, const char *section, const char *key, const char *value);

/**
 *	Sets the string value to the given configuration.
 *	@param cfg The pointer to the given config_t structure.
 *	@param section The section name.
 *	@param key The key name.
 *	@param value The string value.
 */
void config_set_string(config_t cfg, const char *section, const char *key, const char *str);

/**
 *	Sets the integer value to the given configuration.
 *	@param cfg The pointer to the given config_t structure.
 *	@param section The section name.
 *	@param key The key name.
 *	@param value The integer value.
 */
void config_set_int(config_t cfg, const char *section, const char *key, int value);

/**
 *	Sets the floating point value to the given configuration.
 *	@param cfg The pointer to the given config_t structure.
 *	@param section The section name.
 *	@param key The key name.
 *	@param value The floating point value.
 */
void config_set_float(config_t cfg, const char *section, const char *key, float value);


/**
 *	Gets the number of sections in the current configuration.
 *	@param cfg The pointer to the given config_t structure.
 *	@return The number of sections.
 */
int config_get_section_count(config_t cfg);

/**
 *	Gets the name of the given section.
 *	@param cfg The pointer to the given config_t structure.
 *	@param i The index of the given section.
 *	@return
 *		If the function succeeds, the return value is null terminated string,
 *		which is the section name. Otherwise the function returns NULL.
 */
const char *config_get_section(config_t cfg, int i);

/**
 *	Gets the number of keys in the specified section.
 *	@param cfg The pointer to the given config_t structure.
 *	@param section The section name.
 *	@return The number of keys, or null if nothing founds.
 */
int config_get_key_count(config_t cfg, const char *section);

/**
 *	Gets the name of the given key.
 *	@param cfg The pointer to the given config_t structure.
 *	@param section The name of the given section.
 *	@param i The index of the given key.
 *	@return
 *		If the function succeeds, the return value is null terminated string,
 *		which is the key name. Otherwise the function returns NULL.
 */
const char *config_get_key(config_t cfg, const char *section, int i);

/**
 *	Gets the key value.
 *	@param cfg The pointer to the given config_t structure.
 *	@param section The section name.
 *	@param key The key.
 *	@param def The default value.
 *	@return
 *		If the function succeeds, it returns the null terminated string which is the key value,
 *		otherwise it returns the pointer to the default string.
 */
const char *config_get_value(config_t cfg, const char *section, const char *key, const char *def);

/*
 *	Gets the string value by the specified key.
 *	@param cfg The pointer to the given config_t structure.
 *	@param section The section name.
 *	@param key The key.
 *	@param buffer Storage location for output.
 *	@param length Maximum number of characters to store.
 *	@return The pointer to the storage location.
 */
const char *config_get_string(config_t cfg, const char *section, const char *key, char *buffer, int length);

/**
 *	Gets the integer value by the specified key.
 *	@param cfg The pointer to the given config_t structure.
 *	@param section The section name.
 *	@param key The key.
 *	@param def The default value
 *	@return The integer value, which is a result of operation, or default value if the function fails.
 */
int config_get_int(config_t cfg, const char *section, const char *key, int def);

/**
 *	Gets the floating point value by the specified key.
 *	@param cfg The pointer to the given config_t structure.
 *	@param section The section name.
 *	@param key The key.
 *	@param def The default value
 *	@return The floating point value, which is a result of operation, or default value if the function fails.
 */
float config_get_float(config_t cfg, const char *section, const char *key, float def);


#endif // __DRAFT_CONFIG_H__