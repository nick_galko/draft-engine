/******************************************************************************
 *	File:			resource.h
 *
 *	Created by:		Ivan Shishkin <codingdude@gmail.com>
 *
 *	History:		Created 02/02/13
 *
 *	Copyright (c) 2013 All rights reserved
 *
 *****************************************************************************/

#ifndef __DRAFT_RESOURCE_H__
#define __DRAFT_RESOURCE_H__

/******************************************************************************
*	Resource
******************************************************************************/

/**
 *	Initializes the resource system.
 */
void resource_system_init();

/**
 *	Shhutting down the resource system
 */
void resource_system_shutdown();

/**
 *	Gets the resource by name, increments the resource reference counter.
 *	@param name The given name of a resource.
 *	@return If success returns the pointer to the resource_t structure,
 *	which is a result of operation, otherwise return NULL.
 */
resource_t *resource_get(const char *name);

/**
 *	Determines if the resource exists.
 *	@param name The given name of a resource.
 *	@return Nonzero value if resource exists.
 */
int resource_exists(const char *name);

/**
 *	Releases resource by decrementing its reference counter,
 *	frees the data, if needed.
 *	@param r Pointer to the given resource_t structure.
 *	@return The count of the references
 */
int resource_release(resource_t *r);

/**
 *	Frees resource data and sets resource reference counter to 0.
 *	@param r Pointer to the given resource_t structure.
 */
void resource_destroy(resource_t *r);

/**
 *	Gets the resource name.
 *	@param Pointer to the given resource_t structure.
 *	@return The given resource name.
 */
const char *resource_name(const resource_t *r);

/**
 *	Gets the resource type, which is one of the resource_type_t enumeration
 *	@param Pointer to the given resource_t structure.
 *	@return Resource type identifier.
 */
int resource_type(const resource_t *r);

/**
 *	Determines if the resource is loaded or not.
 *	@param Pointer to the given resource_t structure.
 *	@return Nonzero value if the resource is currently loaded.
 */
int resource_loaded(const resource_t *r);

/**
 *	Determines if the resource is prepared for loading or not.
 *	@param Pointer to the given resource_t structure.
 *	@return Nonzero value if the resource is currently prepared to be loaded.
 */
int resource_prepared(const resource_t *r);

/**
 *	Gets resource reference count.
 *	@param Pointer to the given resource_t structure.
 *	@return The given resource reference count.
 */
int resource_reference_count(const resource_t *r);

/******************************************************************************
*	Mesh
******************************************************************************/

/**
 *	Creates the mesh.
 *	@param name Mesh name.
 *	@return The pointer to the mesh_t structure, which is a result of operation.
 */
mesh_t *mesh_create(const char *name);

/**
 *	Releases the mesh by decrementing its reference counter,
 *	destroys mesh data if needed.
 *	@param m Pointer to the given mesh_t structure.
 *	@return The total number of references.
 */
int mesh_release(mesh_t *m);

/**
 *	Destroys the mesh and sets its reference counter to 0.
 *	@param m Pointer to the given mesh_t structure.
 */
void mesh_destroy(mesh_t *m);

/**
 *	Loads the mesh data.
 *	@param m Pointer to the given mesh_t structure.
 *	@return Nonzero value on success.
 */
int mesh_load(mesh_t *m);

/**
 *	Unloads the mesh data.
 *	@param m Pointer to the given mesh_t structure.
 */
void mesh_unload(mesh_t *m);

/**
 *	Reads the mesh data.
 *	@param m Pointer to the given mesh_t structure.
 *	@param r Pointer to the given reader_t structure.
 *	@return Nonzero value on success.
 */
int mesh_read(mesh_t *m, reader_t *r);

/**
 *	Stores the mesh data.
 *	@param m Pointer to the given mesh_t structure.
 *	@param r Pointer to the given writer_t structure.
 *	@return Nonzero value on success.
 */
int mesh_write(mesh_t *m, writer_t *w);

/**
 *	Reads mesh data from a file.
 *	@param m Pointer to the given mesh_t structure.
 *	@param file Source file name.
 */
int mesh_read_file(mesh_t *m, const char *file);

/**
 *	Writes mesh data into a file.
 *	@param m Pointer to the given mesh_t structure.
 *	@param file Target file name.
 */
int mesh_write_file(mesh_t *m, const char *file);

/**
 *	Builds mesh data.
 *	@param m Pointer to the given mesh_t structure.
 *	@param md Pointer to the source mesh_data_t structure.
 */
void mesh_build(mesh_t *m, const mesh_data_t *md);

/**
 *	Gets the hardware vertex buffer identifier.
 *	@param m Pointer to the given mesh_t structure.
 *	@return The hardware vertex buffer identifier.
 */
int mesh_get_vb(const mesh_t *m);

/**
 *	Gets the hardware index buffer identifier.
 *	@param m Pointer to the given mesh_t structure.
 *	@return The hardware index buffer identifier.
 */
int mesh_get_ib(const mesh_t *m);

/**
 *	Gets the bounding box of the given mesh.
 *	@param m Pointer to the given mesh_t structure.
 *	@return pointer to the bbox_t structure, which is a result of operation.
 */
bbox_t *mesh_get_bbox(mesh_t *m);

/**
 *	Gets the vertex declaration of the given mesh.
 *	@param m Pointer to the given mesh_t structure.
 *	@return The vertex declaration, which is set of a vertex_element_t enumeration.
 */
int mesh_get_vertex_decl(const mesh_t *m);

/** 
 *	Gets the vertex count of the given mesh.
 *	@param m Pointer to the given mesh_t structure.
 *	@return The vertex count of the given mesh.
 */
int mesh_get_vertex_count(const mesh_t *m);

/******************************************************************************
*	Texture
******************************************************************************/

/**
 *	Creates the texture.
 *	@param name The given name of the texture.
 *	@return Pointer to the texture_t structure, which is a result of operation.
 */
texture_t *texture_create(const char *name);

/**
 *	Releases the texture by decrementing its reference counter,
 *	destroys texture data, if needed.
 *	@param t Pointer to the given texture_t structure.
 *	@return The total number of references.
 */
int texture_release(texture_t *t);

/**
 *	Destroys the texture and sets its reference counter to 0.
 *	@param t Pointer to the given texture_t structure.
 */
void texture_destroy(texture_t *t);

/**
 *	Loads the texture data.
 *	@param m Pointer to the given texture_t structure.
 *	@return Nonzero value on success.
 */
int texture_load(texture_t *t);

/**
 *	Unloads the texture data.
 *	@param m Pointer to the given texture_t structure.
 */
void texture_unload(texture_t *t);

/**
 *	Reads the texture data.
 *	@param m Pointer to the given texture_t structure.
 *	@param r Pointer to the given reader_t structure.
 *	@return Nonzero value on success.
 */
int texture_read(texture_t *t, reader_t *r);

/**
 *	Stores the texture data.
 *	@param m Pointer to the given texture_t structure.
 *	@param r Pointer to the given writer_t structure.
 *	@return Nonzero value on success.
 */
int texture_write(texture_t *t, writer_t *w);

/**
 *	Reads texture data from a file.
 *	@param m Pointer to the given texture_t structure.
 *	@param file Source file name.
 */
int texture_read_file(texture_t *t, const char *file);

/**
 *	Writes texture data into a file.
 *	@param m Pointer to the given texture_t structure.
 *	@param file Target file name.
 */
int texture_write_file(texture_t *t, const char *file);

/**
 *	Gets texture width.
 *	@param m Pointer to the given texture_t structure.
 *	@return Texture width.
 */
int texture_width(texture_t *t);

/**
 *	Gets texture height.
 *	@param m Pointer to the given texture_t structure.
 *	@return Texture height.
 */
int texture_height(texture_t *t);

/**
 *	Gets the given texture mip-map count.
 *	@param m Pointer to the given texture_t structure.
 *	@return Texture mip-map count.
 */
int texture_mipmaps(texture_t *t);

/**
 *	Gets texture pixel format.
 *	@param m Pointer to the given texture_t structure.
 *	@return Texture pixel format, which is one of the pixel_format_t enumeration.
 */
int texture_format(texture_t *t);

/**
 *	Gets texture image data.
 *	@param m Pointer to the given texture_t structure.
 *	@return Pointer to the image data.
 */
void *texture_data(texture_t *t);

/**
 *	Gets the texture image size.
 *	@param m Pointer to the given texture_t structure.
 *	@return Image size in bytes.
 */
int texture_size(texture_t *t);

#endif // __DRAFT_RESOURCE_H__