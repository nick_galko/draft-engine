/******************************************************************************
 *
 *	File:			scene.h
 *
 *	Created by:		Ivan Shishkin <codingdude@gmail.com>
 *
 *	History:		Created 27/02/13
 *
 *	Copyright (c) 2013 All rights reserved
 *
 *****************************************************************************/

#ifndef __SCENE_H__
#define __SCENE_H__

/******************************************************************************
*	Scene
******************************************************************************/

/**
 *	Initializes the scene.
 *	@return Nonzero value on success.
 */
int scene_init();

/**
 *	Shutting down the scene.
 */
void scene_shutdown();

/**
 *	Clears the scene.
 */
void scene_clear();

/**
 *	Render the scene.
 */
void scene_render();

/**
 *	Gets the scene node by name.
 *	@param name The name of the scene node.
 *	@return If success returns the pointer to the scene_node_t structure,
 *	which is a result of operation, otherwise return 0.
 */
scene_node_t *scene_get_node(const char *name);

/**
 *	Gets the root scene node.
 *	@return The pointer to the scene_node_t, which is a result of operation.
 */
scene_node_t *scene_get_root(void);

/******************************************************************************
*	Entity
******************************************************************************/

/**
 *	Gets the scene entity and incrementing its reference counter.
 *	@param name The given entity name.
 *	@return Pointer to the entity_t structure, which is a result of operation.
 */
entity_t *scene_get_entity(const char *name);

/**
 *	Destroys the entity.
 *	@param ent The pointer to the given entity_t structure.
 */
void scene_destroy_entity(entity_t *ent);

/**
 *	Gets the entity type, which is one of the entity_types_t enumeration.
 *	@param ent Pointer to the given entity_t structure.
 *	@return The entity type identifier.
 */
int scene_entity_type(const entity_t *ent);

/**
 *	Gets the surface from an entity, if it's has one. Increments the surface reference counter.
 *	@param ent Pointer to the source entity_t structure.
 *	@return The pointer to the surface_t structure which is a result of operation,
 *	or NULL, if no surfaces in the given entity.
 */
surface_t *scene_entity_get_surface(const entity_t *ent);

/**
 *	Creates the new surface.
 *	@param name The given surface name.
 *	@param mesh The given mesh name.
 *	@param texture The given texture name.
 *	@return Pointer to the surface_t structure, which is a result of operation.
 */
surface_t *scene_create_surface(const char *name, const char *mesh, const char *texture);

/**
 *	Destroys the surface.
 *	@param ent The pointer to the given surface_t structure.
 */
void scene_destroy_surface(surface_t *surface);


/******************************************************************************
*	Scene Node
******************************************************************************/

/**
 *	Creates the scene node.
 *	@param name The given name.
 *	@return Pointer to the scene_node_t structure, which is a result of operation.
 */
scene_node_t *scene_node_create(const char *name);

/**
 *	Destroys the scene node.
 *	@param node Pointer to a given scene node.
 */
void scene_node_destroy(scene_node_t *node);

/**
 *	Updates the scene node and its childs.
 *	@param node Pointer to a given scene node.
 */
void scene_node_update(scene_node_t *node);

/**
 *	Adds the child scene node.
 *	@param node Pointer to dhe given scene_node_t structure.
 *	@param child Pointer to the child scene_node_t structure.
 */
void scene_node_add_child(scene_node_t *node, scene_node_t *child);

/**
 *	Removes child scene node.
 *	@param node Pointer to dhe given scene_node_t structure.
 *	@param child Pointer to the child scene_node_t structure.
 */
void scene_node_remove_child(scene_node_t *node, scene_node_t *child);

/**
 *	Gets the count of the children.
 *	@param node Pointer to dhe given scene_node_t structure.
 *	@return The childs count.
 */
int scene_node_child_count(const scene_node_t *node);

/**
 *	Gets the child by it index.
 *	@param node Pointer to the given scene_node_t structure.
 *	@param i Child index.
 *	@return Pointer to the scene_node_t, which is a result of operation.
 */
scene_node_t *scene_node_get_child(const scene_node_t *node, int i);

/**
 *	Adds an instance of the scene object to the node.
 *	@param node Pointer to the given scene_node_t structure.
 *	@param ent Pointer to the given entity_t structure.
 */
void scene_node_add_entity(scene_node_t *node, entity_t *ent);

/**
 *	Removes an instance of the scene object from the node.
 *	@param node Pointer to the given scene_node_t structure.
 *	@param ent Pointer to the given entity_t structure.
 */
void scene_node_remove_entity(scene_node_t *node, entity_t *ent);

/**
 *	Gets the count of the scene objects, attached to the node.
 *	@param node Pointer to the given scene_node_t structure.
 *	@return The number of a scene objects, attached to the node. 
 */
int scene_node_entity_count(const scene_node_t *node);

/**
 *	Gets the attached scene object from the node.
 *	@param node Pointer to the given scene_node_t structure.
 *	@param i The index of an attached scene object.
 */
entity_t *scene_node_get_entity(const scene_node_t *node, int i);

/**
 *	Rotates the given scene node by the specified quaternion.
 *	@param node Pointer to the given scene_node_t structure.
 *	@param rotation Pointer to the given quaternion_t structure.
 */
void scene_node_rotate(scene_node_t *node, const quaternion_t *rotation);

/**
 *	Translates the given scene node by the specified vector.
 *	@param node Pointer to the given scene_node_t structure.
 *	@param translation Pointer to the given vector3_t structure.
 */
void scene_node_translate(scene_node_t *node, const vector3_t *translation);

/**
 *	Scales the given scene node by the specified vector.
 *	@param node Pointer to the given scene_node_t structure.
 *	@param scale Pointer to the given vector3_t structure.
 */
void scene_node_scale(scene_node_t *node, const vector3_t *scale);

/**
 *	Sets the rotation of the given scene node.
 *	@param node Pointer to the given scene_node_t structure.
 *	@param rotation Pointer to the given quaternion_t structure, representing rotation.
 */
void scene_node_set_rotation(scene_node_t *node, const quaternion_t *rotation);

/**
 *	Sets the translation of the given scene node.
 *	@param node Pointer to the given scene_node_t structure.
 *	@param translation Pointer to the vector3_t structure, representing translation.
 */
void scene_node_set_translation(scene_node_t *node, const vector3_t *translation);

/**
 *	Sets the scale of the given scene node.
 *	@param node Pointer to the given scene_node_t structure.
 *	@param translation Pointer to the vector3_t structure, representing thescale.
 */
void scene_node_set_scale(scene_node_t *node, const vector3_t *scale);

/**
 *	Gets the scene node rotation.
 *	@param node Pointer to the given scene_node_t structure.
 *	@return Pointer to the quaternion_t structure, which is a result of operation.
 */
const quaternion_t *scene_node_get_rotation(const scene_node_t *node);

/**
 *	Gets the scene node translation.
 *	@param node Pointer to the given scene_node_t structure.
 *	@return Pointer to the vector3_t structure, which is a result of operation.
 */
const vector3_t *scene_node_get_translation(const scene_node_t *node);

/**
 *	Gets the scene node scale.
 *	@param node Pointer to the given scene_node_t structure.
 *	@return Pointer to the vector3_t structure, which is a result of operation.
 */
const vector3_t *scene_node_get_scale(const scene_node_t *node);

/**
 *	Gets the scene node world rotation.
 *	@param node Pointer to the given scene_node_t structure.
 *	@return Pointer to the quaternion_t structure, which is a result of operation.
 */
quaternion_t *scene_node_world_rotation(quaternion_t *out, const scene_node_t *node);

/**
 *	Gets the scene node world translation.
 *	@param node Pointer to the given scene_node_t structure.
 *	@return Pointer to the vector3_t structure, which is a result of operation.
 */
vector3_t *scene_node_world_translation(vector3_t *out, const scene_node_t *node);

/**
 *	Gets the scene node world scale.
 *	@param node Pointer to the given scene_node_t structure.
 *	@return Pointer to the vector3_t structure, which is a result of operation.
 */
vector3_t *scene_node_world_scale(vector3_t *out, const scene_node_t *node);

/**
 *	Determines if the given scene node is in the bounding box or not.
 *	@param node Pointer to the given scene_node_t structure.
 *	@param bbox Pointer to the given bbox_t structure.
 *	@return A nonzero value if the scene node is in the box.
 */
int scene_node_is_in(const scene_node_t *node, const bbox_t *bbox);

/**
 *	Sets the LOD mode of the given node.
 *	@param node Pointer to the given scene_node_t structure.
 *	@mode The mode, which is one of the lod_mode_t enumeration.
 */
void scene_node_set_lodmode(scene_node_t *node, int mode);

/**
 *	Gets the given scene node LOD mode.
 *	@param node Pointer to the given scene_node_t structure.
 *	@return The mode identifier, which is one of the lod_mode_t enumeration.
 */
int scene_node_lod_mode(const scene_node_t *node);

/**
 *	Sets the LOD range.
 *	@param _min The minimum.
 *	@param _max The maximum.
 */
void scene_node_set_range(scene_node_t *node, float _min, float _max);

/**
 *	Determines if the LOD is visible or not.
 *	@param node Pointer to the given scene_node_t structure.
 *	@return A nonsero value if the LOD is visible.
 */
int scene_node_lod_visible(scene_node_t *node);

/******************************************************************************
*	Camera
******************************************************************************/

/**
 *	Creates the scene camera.
 *	@param name The given camera name.
 *	@return The pointer to the camera_t structure, which is a result of operation.
 */
camera_t *scene_create_camera(const char *name);

/**
 *	Destroys the given camera.
 *	@param cam The pointer to the given camerat_t structure.
 */
void scene_destroy_camera(camera_t *cam);

/**
 *	Gets the scene camera.
 *	@param name The given camera name.
 *	@return If succeded, returns the pointer to the camera_t structure,
 *	which is a result of operation, otherwise returns NULL.
 */
camera_t *scene_get_camera(const char *name);

/**
 *	Gets the currently active camera.
 *	@return Pointer to the camera_t structure, which is a result of operation.
 */
camera_t *scene_get_active_camera();

/**
 *	Sets the currently active camera.
 *	@param cam Pointer to the given camera_t structure.
 */
void scene_set_active_camera(camera_t *cam);

/**
 *	Sets the camera translation.
 *	@param cam Poiner to the given camera_t structure.
 *	@param translations Pointer to the vector3_t structure,
 *	which represents the camera translation.
 */
void camera_set_translation(camera_t *cam, const vector3_t *translations);

/**
 *	Sets the camera rotation.
 *	@param cam The pointer to the given camera_t structure.
 *	@param rotations Pointer to the given quaternion_t structure.
 */
void camera_set_rotation(camera_t *cam, const quaternion_t *rotations);

/**
 *	Sets the camera scale.
 *	@param cam Poiner to the given camera_t structure.
 *	@param scale Pointer to the vector3_t structure,
 *	which represents the camera scalling.
 */
void camera_set_scale(camera_t *cam, const vector3_t *scale);

/**
 *	Translates the given camera.
 *	@param cam Poiner to the given camera_t structure.
 *	@param translations Pointer to the vector3_t structure,
 *	which represents the camera translation.
 */
void camera_translate(camera_t *cam, const vector3_t *translations);

/**
 *	Rotates the given camera.
 *	@param cam Poiner to the given camera_t structure.
 *	@param rotations Pointer to the quaternion_t structure,
 *	which represents the camera rotations.
 */
void camera_rotate(camera_t *cam, const quaternion_t *rotations);

/**
 *	Scales the given camera.
 *	@param cam Poiner to the given camera_t structure.
 *	@param scale Pointer to the vector3_t structure,
 *	which represents the camera scalling.
 */
void camera_scale(camera_t *cam, const vector3_t *scale);

/**
 *	Sets the ortographics projection matrix for the current camera view.
 *	@param cam Pointer to the given camera_t structure.
 *	@param w The width of the view volume.
 *	@param h The height of the view volume.
 *	@param zn Minimum z-value of the view volume
 *	@param zf Maximum z-value of the view volume
 */
void camera_set_ortho(camera_t *cam, float w, float h, float znear, float zfar);

/**
 *	Sets the perspective projection matrix for the current camera view.
 *	@param cam Pointer to the given camera_t structure.
 *	@param w The width of the view volume.
 *	@param h The height of the view volume.
 *	@param zn Minimum z-value of the view volume
 *	@param zf Maximum z-value of the view volume
 */
void camera_set_perspective(camera_t *cam, float w, float h, float znear, float zfar);

/**
 *	Sets the perspective projection matrix for the current camera view, based on the field of view.
 *	@param cam Pointer to the given camera_t structure.
 *	@param fov Field of view in the y direction, in radians.
 *	@param aspect Aspect ratio, defined as view space width divided by height.
 *	@param zn Minimum z-value of the view volume
 *	@param zf Maximum z-value of the view volume
 */
void camera_set_perspective_fov(camera_t *cam, float fovy, float aspect, float znear, float zfar);

/**
 *	Updates the camera.
 *	@param cam Pointer to the camera_t structure.
 */
void camera_update(camera_t *cam);

/**
 *	Determines if the camera view has changed.
 *	@param cam Pointer to the given camera_t structure.
 *	@return A nonzero value if the camera view has changed.
 */
int camera_view_changed(const camera_t *cam);

/**
 *	Determines if the camera projection has changed.
 *	@param cam Pointer to the given camera_t structure.
 *	@return A nonzero value if the camera projection has changed.
 */ 
int camera_proj_changed(const camera_t *cam);

/**
 *	Gets the camera view.
 *	@param cam Pointer to the given camera_t structure.
 *	@return Pointer to the matrix_t structure, which is a result of operation.
 */
const matrix_t *camera_get_view(const camera_t *cam);

/**
 *	Gets the camera projection.
 *	@param cam Pointer to the given camera_t structure.
 *	@return Pointer to the matrix_t structure, which is a result of operation.
 */
const matrix_t *camera_get_proj(const camera_t *cam);

#endif // __SCENE_H__