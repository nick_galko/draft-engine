/******************************************************************************
 *	File:			types.h
 *
 *	Created by:		Ivan Shishkin <codingdude@gmail.com>
 *
 *	History:		Created 23/09/12
 *
 *	Copyright (c) 2012 All rights reserved
 *
 *****************************************************************************/

#ifndef __DRAFT_TYPES_H__
#define __DRAFT_TYPES_H__

#ifndef min
#define min(a, b) (((a) < (b)) ? (a) : (b))
#endif

#ifndef max
#define max(a, b) (((a) > (b)) ? (a) : (b))
#endif

typedef void *(*duplicate_func)(void *);
typedef void (*release_func)(void *);
typedef int (*compare_func)(const void *left, const void *right);
typedef void (*debug_output_func)(const char *msg);

typedef struct _vector2 {
	union {
		struct {
			float x;
			float y;
		};
		float v[2];
	};
} vector2_t;

typedef struct _vector3 {
	union {
		struct {
			float x;
			float y;
			float z;
		};
		float v[3];
	};
} vector3_t;

typedef struct _vector4 {
	union {
		struct {
			float x;
			float y;
			float z;
			float w;
		};
		float v[4];
	};
} vector4_t, quaternion_t;

typedef struct _matrix {
	union {
		struct {
			float m11, m12, m13, m14;
			float m21, m22, m23, m24;
			float m31, m32, m33, m34;
			float m41, m42, m43, m44;
		};
		float m[4][4];
	};
} matrix_t;

typedef struct _plane {
	float		a, b, c, d;
} plane_t;

typedef struct _bbox {
	int			infinite;

	vector3_t	minimum;
	vector3_t	maximum;
} bbox_t;

typedef struct _frustum {
	matrix_t	view;
	matrix_t	proj;
	matrix_t	viewproj;

	char		need_update;

	plane_t		planes[6];
} frustum_t;

typedef struct _array {
	int		size;
	int		stride;
	int		reserved;
	void	*data;
} array_t;

typedef struct _list {
	struct _list	*prev;
	struct _list	*next;
	void			*data;
} list_t;

typedef struct _node {
	struct _node	*right;
	struct _node	*left;
	struct _node	*parent;

	int				color;

	void			*data;
} node_t;

typedef struct _tree {
	node_t			root;
	node_t			nil;

	int				count;

	compare_func	cmp;
} tree_t;

typedef tree_t *config_t;

#define PA_MESHES		"meshes"
#define PA_TEXTURES		"textures"
#define PA_MAPS			"maps"

typedef struct _reader {
	uint32_t		id;
	uint32_t		size;
	uint8_t			*data;

	uint32_t		refer;

	union {
		uint8_t		*u8p;
		int8_t		*s8p;
		uint16_t	*u16p;
		int16_t		*s16p;
		uint32_t	*u32p;
		int32_t		*s32p;
		float		*f32p;
	};
} reader_t;

typedef struct _writer {
	uint32_t		id;
	uint32_t		ptr;
	array_t			*data;
} writer_t;

typedef enum _resource_type {
	RT_MESH,
	RT_TEXTURE,

	RT_MAX_TYPES
} resource_type_t;

typedef void (*resource_unload_func)(void*);

typedef struct _resource {
	resource_unload_func
		unload_fn;

	char		*name;
	int			type;

	int			refs;

	int			loaded;
	int			prepared;
} resource_t;

typedef enum _matrix_type {
	MATRIX_MODEL,
	MATRIX_VIEW,
	MATRIX_PROJECTION,

	MATRIX_MAX_TYPES
} matrix_type_t;

typedef enum _vertex_element {
	VE_POINT		= (0x01<<0),
	VE_NORMAL		= (0x01<<1),
	VE_TANGENT		= (0x01<<2),
	VE_BINORMAL		= (0x01<<3),
	VE_BONES		= (0x01<<4),
	VE_WEIGHTS		= (0x01<<5),
	VE_TEXCOORDS1	= (0x01<<6),
	VE_TEXCOORDS2	= (0x01<<7),
	VE_TEXCOORDS3	= (0x01<<8),
	VE_TEXCOORDS4	= (0x01<<9),
	VE_TEXCOORDS5	= (0x01<<10),
	VE_TEXCOORDS6	= (0x01<<11),
	VE_TEXCOORDS7	= (0x01<<12),
	VE_TEXCOORDS8	= (0x01<<13)
} vertex_element_t;

typedef struct _mesh_data {
	array_t		*points;
	array_t		*normals;
	array_t		*tangents;
	array_t		*binormals;
	array_t		*bones;
	array_t		*weights;
	array_t		*uv_maps;
	array_t		*indices;
} mesh_data_t;

typedef struct _mesh {
	resource_t		resource;
	bbox_t			bbox;

	int				vdecl;
	int				vsize;
	int				vcount;
	int				icount;

	union {
		struct {
			array_t	*vdata;
			array_t	*idata;
		};
		struct {
			int		vbuffer;
			int		ibuffer;
		};
	};
} mesh_t;

typedef enum _texmap_flags {
	TEXMAP_CUBEMAP			= (0x01<<0),
	TEXMAP_2D				= (0x01<<1),
	TEXMAP_2D_ARRAY			= (0x01<<2),
	TEXMAP_3D				= (0x01<<3)
} texmap_flags_t;

typedef enum _pixel_format {
	PVRTC_2BPP_RGB = 0,
	PVRTC_2BPP_RGBA = 1,
	PVRTC_4BPP_RGB = 2,
	PVRTC_4BPP_RGBA = 3,
	PVRTCII_2BPP = 4,
	PVRTCII_4BPP = 5,
	ETC1 = 6,
	ETC2_RGB = 7,
	ETC2_RGBA = 8,
	ETC2_RGBA1 = 9,
	EACR11U = 10,
	EACR11S = 11,
	EACRG11U = 12,
	EACRG11S = 13,
	DXT1 = 14,
	DXT3 = 15,
	DXT5 = 16,
	RGBA8888 = 17,
	RGBA4444 = 18,
	RGBA5551 = 19,
	BGRA8888 = 20,
	RGB888 = 21,
	RGB565 = 22,
	ALPHA = 23
} pixel_format_t;

typedef enum _color_channel_type {
	CHANNEL_UBYTE	= 2,
	CHANNEL_SBYTE	= 3,
	CHANNEL_USHORT	= 6,
	CHANNEL_SSHORT	= 7,
	CHANNEL_UINT	= 10,
	CHANNEL_SINT	= 11,
	CHANNEL_FLOAT	= 12
} color_channel_type_t;

typedef struct _texture {
	resource_t		resource;

	int				format;
	int				channel;
	int				flags;

	int				width;
	int				height;
	int				depth;

	int				mipmaps;
	int				layers;

	union {
		array_t		*raw;
		int			texmap;
	};
} texture_t;

typedef enum _entity_types {
	ENTITY_UNKNOWN,
	ENTITY_SURFACE
} entity_types_t;

typedef void (*entity_destroy_func)(void *);

typedef struct _entity {
	const char		*name;

	entity_destroy_func
		destroy_fn;

	int				type;
} entity_t;

typedef struct _surface {
	entity_t		entity;

	mesh_t			*mesh;
	texture_t		*texture;
} surface_t;

typedef struct _octree {
	struct _octree	*parent;

	struct _octree	*octants[2][2][2];

	int				depth;

	bbox_t			bbox;

	list_t			*nodes;
} octree_t;

typedef enum _lod_mode {
	LOD_DISABLED,
	LOD_DISTANCE,
	LOD_PERCENTAGE
} lod_mode_t;

typedef struct _scene_node {
	const char			*name;

	array_t				*entities;

	vector3_t			translation;
	quaternion_t		rotation;
	vector3_t			scale;

	octree_t			*octant;

	bbox_t				bbox;

	int					moved;
	int					modified;
	int					lod;

	vector2_t			range;

	matrix_t			tm;
	matrix_t			world_tm;

	struct _scene_node	*parent;

	array_t				*children;
} scene_node_t;

typedef enum _projection_type {
	PROJECTION_PERSPECTIVE,
	PROJECTION_PERSPECTIVE_FOV,
	PROJECTION_ORTHO
} projection_type_t;

typedef struct _camera {
	frustum_t		frustum;

	const char		*name;

	union {
		struct {
			float	width;
			float	height;
		};
		struct {
			float	fov;
			float	aspect;
		};
	};

	float			znear;
	float			zfar;

	int				projection_type;

	int				view_changed;
	int				projection_changed;

	scene_node_t	*owner;

	vector3_t		translation;
	quaternion_t	rotation;
	vector3_t		scale;

} camera_t;

typedef struct _animation {
    struct key {
        float       time;
        float       value;
    };
    
    struct track {
        const char  *name;
        
        array_t     *rkeys[3];
        array_t     *tkeys[3];
    };
    
    struct mark {
        float       t0;
        float       t1;
    };
    
    resource_t      resource;
    
    float           length;
    
    array_t         tracks;
    array_t         marks;
    
    int             repeat;
} animation_t;

#endif // __DRAFT_TYPES_H__