/******************************************************************************
 *	File:			dslib.h
 *
 *	Created by:		Ivan Shishkin <codingdude@gmail.com>
 *
 *	History:		Created 04/01/13
 *
 *	Copyright (c) 2013 All rights reserved
 *
 *****************************************************************************/

#ifndef __DRAFT_DSLIB_H__
#define __DRAFT_DSLIB_H__

/******************************************************************************
*	Array
******************************************************************************/

/**
 *	Returns a pointer to a newly created array.
 *	@param stride The size of an array element
 *	@return Pointer to the array_t structure. If the function fails, return value is NULL
 */
array_t *array_create(int stride);

/**
 *	Creates a new array and copies the specified elements to them.
 *	@param first Position of the first element in the range of elements to be copied.
 *	@param last Position of the last element in the range of elements to be copied.
 *	@param stride The size of an array element
 *	@return Pointer to the array_t structure. If the function fails, return value is NULL
 */
array_t *array_create_from_existing(const void *first, const void *last, int stride);

/**
 *	Destroys an array of elements.
 *	@param arr A pointer to an array being destroyed.
 */
void array_destroy(array_t *arr);

/**
 *	Disposes an array of elements.
 *	@param arr The pointer to an array being disposed.
 */
void array_clear(array_t *arr);

/**
 *	Reserves a minimum length of storage for an array, allocating space if necessary.
 *	@param arr The pointer to the target array_t structure.
 *	@param count The minimum length of storage to be allocated for the array.
 */
void array_reserve(array_t *arr, int count);

/**
 *	Specifies a new size for an array.
 *	@param arr The pointer to an array being resized.
 *	@param count The new size which is number of array elements.
 */
void array_resize(array_t *arr, int count);

/**
 *	Appends a new element to the array, allocating space if necessary.
 *	@param arr The pointer to target array_t structure.
 *	@param item The element being appended.
 */
void array_append(array_t *arr, const void *item);

/**
 *	Appends a range of an elements to the array, allocating space if necessary.
 *	@param arr The pointer to the target array_t structure.
 *	@param first Position of the first element in the range of elements being appended.
 *	@param last Position of the last element in the range of elements being appended.
 */
void array_append_range(array_t *arr, const void *first, const void *last);

/**
 *	Inserts a new element to the array, allocating space if necessary.
 *	@param arr The pointer to target array_t structure.
 *	@param pos The insert position.
 *	@param item The element being appended
 */
void array_insert(array_t *arr, int pos, const void *item);

/**
 *	Inserts a range of an elements to the array, allocating space if necessary.
 *	@param arr The pointer to the target array_t structure.
 *	@param pos The insert position.
 *	@param first Position of the first element in the range of elements being inserted.
 *	@param last Position of the last element in the range of elements being inserted.
 */
void array_insert_range(array_t *arr, int pos, const void *first, const void *last);

/**
 *	Sets an element in the array, allocating space if index out of bounds.
 *	@param arr The pointer to target array_t structure.
 *	@param pos Position of the element being added or replaced.
 *	@param item Pointer to the new element.
 */
void array_set(array_t *arr, int pos, const void *item);

/**
 *	Sets a range of an elements to the array, allocating space if range out of bounds.
 *	@param arr The pointer to the target array_t structure.
 *	@param pos Position of the first element being added or replaced.
 *	@param first Position of the first element in the range of elements to set.
 *	@param last Position of the last element in the range of elements to set.
 */
void array_set_range(array_t *arr, int pos, const void *first, const void *last);

/**
 *	Removes last element from an array.
 *	@param arr The pointer to the target array_t structure.
 */
void array_remove(array_t *arr);

/**
 *	Removes a range of elements from the end of an array.
 *	@param arr The pointer to the target array_t structure.
 *	@param count The number of elements being removed.
 */
void array_remove_range(array_t *arr, int count);

/**
 *	Removes specified element from an array.
 *	@param arr The pointer to the target array_t structure.
 *	@param item Pointer to the element being removed.
 */
void array_erase(array_t *arr, void *item);

/**
 *	Removes a range of specified elements from an array.
 *	@param arr The pointer to the target array_t structure.
 *	@param count The number of elements being removed.
 *	@param first Position of the first element in the range of elements being removed.
 *	@param last Position of the last element in the range of elements being removed.
 */
void array_erase_range(array_t *arr, void *first, void *last);

/**
 *	Returns current array size in elements
 *	@param arr The pointer to the target array_t structure.
 *	@return Number of elements in array.
 */
int array_size(const array_t *arr);

/**
 *	Returns element size of the array
 *	@param arr The pointer to the target array_t structure.
 *	@return Element size in bytes.
 */
int array_stride(const array_t *arr);

/**
 *	Returns pointer to the first element of the array.
 *	@param arr The pointer to the target array_t structure.
 *	@return Pointer to the first element. If arr is empty, returns NULL.
 */
void *array_first(const array_t *arr);

/**
 *	Returns pointer to the last element of the array.
 *	@param arr The pointer to the target array_t structure.
 *	@return Pointer to the last element. If arr is empty, returns NULL.
 */
void *array_last(const array_t *arr);

/**
 *	Returns pointer to the next element of the array.
 *	@param arr The pointer to the target array_t structure.
 *	@param item Pointer to the current element.
 *	@return Pointer to the next element. If arr is empty or out of bounds, then returns NULL.
 */
void *array_next(const array_t *arr, const void *item);

/**
 *	Returns pointer to the previous element of the array.
 *	@param arr The pointer to the target array_t structure.
 *	@param item Pointer to the current element.
 *	@return Pointer to the previous element. If arr is empty or out of bounds, then returns NULL.
 */
void *array_prev(const array_t *arr, const void *item);

/**
 *	Returns pointer to the specified element of the array.
 *	@param arr The pointer to the target array_t structure.
 *	@param pos Element index.
 *	@return Pointer to the specified element. If arr is empty or out of bounds, then returns NULL.
 */
void *array_at(const array_t *arr, int pos);

/******************************************************************************
*	List
******************************************************************************/

/**
 *	Creates a new list node.
 *	@return The pointer to the list_t structure which is single list element.
 */
list_t *list_create();

/**
 *	Removes an elements from a list without freeing element data.
 *	@param list The root element of the list being destroyed.
 */
void list_destroy(list_t *list);

/**
 *	Removes an elements from a list and free elements data.
 *	@param list The root element of the list being disposed.
 *	@param dup The pointer to the function used to free user data.
 */
void list_dispose(list_t *list, release_func release);

/**
 *	Copies a range of elements from one list into another.
 *	Only the pointers will be copied, but actual data is not.
 *	@param first The head element of the list being copied.
 *	@param	last The last element being copied. This parameter may be NULL.
 *	@return Returns the head element of the new list.
 */
list_t *list_clone(const list_t *first, const list_t *last);

/**
 *	Makes a copy of each list element, in addition to copying the list container itself.
 *	@param first The head element of the list being copied.
 *	@param	last The last element of the list being copied. This parameter may be NULL.
 *	@param dup The pointer to the function used to copy user data from one list into another.
 *	@return Returns the head element of the new list.
 */
list_t *list_duplicate(const list_t *first, const list_t *last, duplicate_func dup);

/**
 *	Adds a new element at the end of a list.
 *	@param list The pointer to the list_t structure. This parameter might be NULL
 *	@param data The data to the new element.
 *	@return The new end of the list.
 */
list_t *list_append(list_t *list, void *data);

/**
 *	Adds a new element at the start of a list.
 *	@param list The pointer to the list_t structure. This parameter might be NULL.
 *	@param data The data to the new element.
 *	@return The new start of the list.
 */
list_t *list_prepend(list_t *list, void *data);

/**
 *	Sets the new data link for the element at the given position.
 *	@param list The pointer to the list_t structure.
 *	@param pos The position of the element being changed.
 *	@param data The data to the new element.
 *	@return If pos is the right position, this function returns an element at given position.
 *			Otherwise, this function returns zero.
 */
list_t *list_set(list_t *list, int pos, void *data);

/**
 *	Inserts the new element at the given position.
 *	@param list The pointer to the list_t structure.
 *	@pos pos The position to insert the element.
 *	@param data The data to the new element.
 *	@return If pos is the right position, this function returns an element at given position.
 *			Otherwise, this function returns zero.
 */
list_t *list_insert(list_t *list, int pos, void *data);

/**
 *	Inserts a range of a linked list elements at the given position.
 *	@param list The pointer to the list_t structure.
 *	@param pos The position to insert the elements.
 *	@param first The first element in a range of elements being inserted.
 *	@param last The last element in a range of elements being inserted.
 *	@return If pos is the right position, this function returns an element at the given position.
 *			Otherwise, this function returns zero.
 */
list_t *list_insert_range(list_t *list, int pos, const list_t *first, const list_t *last);

/**
 *	Removes given element from a list.
 *	@param item The element being removed.
 *	@return If given element at the end of the list, this function returns the previous element,
 *			otherwise this function returns the next element or zero if list is empty.
 */
list_t *list_remove(list_t *item);

/**
 *	Removes first element from a list.
 *	@param item The element being removed.
 *	@return Rreturns the next element of a list or zero if list is empty.
 */
list_t *list_remove_front(list_t *item);

/**
 *	Removes last element from a list.
 *	@param item The element being removed.
 *	@return Returns the previous element of a list or zero if list is empty.
 */
list_t *list_remove_back(list_t *item);

/**
 *	Returns the first element of a list.
 *	@param list The pointer to the list_t structure.
 *	@return Returns the first element in a list, or NULL if the list has no elements.
 */
list_t *list_first(const list_t *list);

/**
 *	Returns the last element of a list.
 *	@param list - The pointer to the list_t structure.
 *	@return The last element in a list, or NULL if the list has no elements.
 */
list_t *list_last(const list_t *list);

/**
 *	Returns the next element of a list.
 *	@param list The pointer to the list_t structure which is the current element.
 *	@return The the next element of a list, or NULL if there are no more elements.
 */
list_t *list_next(const list_t *list);

/**
 *	Returns the previous element of a list.
 *	@param list The pointer to the list_t structure which is the current element.
 *	@return The the previous element of a list, or NULL if there are no previous elements.
 */
list_t *list_prev(const list_t *list);

/**
 *	Returns the element of a list at the given position.
 *	@param list The pointer to the list_t structure.
 *	@return The element at the given position, or NULL if the position is off the end of the list.
 */
list_t *list_at(const list_t *list, int pos);

/**
 *	Gets the data of the element at the given position.
 *	@param list The pointer to the list_t structure.
 *	@return The data of the element at the given position.
 */
void *list_data(const list_t *list);

/**
 *	Gets the number of elements in a list.
 *	@param list The pointer to the list_t structure.
 *	@return The number of elements in a list.
 */
int list_size(const list_t *list);

/**
 *	Sorts a list using a given comparsion function. The algorithm used is a stable sort.
 *	@param list The pointer to the list_t structure.
 /	@param cmp The comparsion function used to sort the list.
 */
list_t *list_sort(list_t *list, compare_func cmp);

/**
 *	Finds the element in a list which contains the given data.
 *	@param list The pointer to the list_t structure.
 *	@param cmp The function to call for each element.
 *			It should return 0 when the desired element is found.
 *	@return Returns the found list element, or NULL if it is not found.
 */
list_t *list_find(list_t *list, void *data, compare_func cmp);

/**
 *	Reverses a list. It simply switches the next and prev pointers of each element.
 *	@param list The pointer to the list_t structure.
 *	@return Returns the start of the reversed list
 */
list_t *list_reverse(list_t *list);

/******************************************************************************
*	Tree
******************************************************************************/

/**
 *	Creates a new tree.
 *	@param cf The function used to order the nodes in the tree.
 */
tree_t *tree_create(compare_func cf);

/**
 *	Removes all nodes from the tree.
 *	@param tree The pointer to the tree_t structure.
 *	@param release The function used to release key-value pairs. This parameter might be NULL.
 */
void tree_clear(tree_t *tree, release_func release);

/**
 *	Destroys the tree.
 *	@param tree The pointer to the tree_t structure.
 *	@param release The function used to release key-value pairs. This parameter might be NULL.
 */
void tree_destroy(tree_t *tree, release_func release);

/**
 *	Greates a copy of a given tree.
 *	@param tree The pointer to the tree_t structure.
 *	@param dup The function used to duplicate key-value pairs.
 *		If this parameter is NULL, then only the pointers will be copied.
 *	@return The pointer to the tree_t structure which is a result of operation.
 */
tree_t *tree_clone(tree_t *tree, duplicate_func dup);

/**
 *	Inserts a key-value pair into a tree, even if the given key already exists in the tree.
 *	@param tree The pointer to the tree_t structure.
 *	@param data The pointer to the key-value pair.
 *	@return The pointer to the node_t structure pointing to the newly inserted node.
 */
node_t *tree_insert(tree_t *tree, void *data);

/**
 *	Inserts a key-value pair into a tree.
 *	If the given key already exists in the tree its corresponding value is set to the new value.
 *	@param tree The pointer to the tree_t structure.
 *	@param data The pointer to the key-value pair.
 *	@param release The old value is freed using that function. This parameter might be NULL.
 *	@return The pointer to the node_t structure pointing to the newly inserted node.
 */
node_t *tree_replace(tree_t *tree, void *data, release_func release);

/**
 *	Removes a key-value pair from a tree.
 *	@param tree The pointer to the tree_t structure.
 *	@param node The node to remove.
 *	@param release The function used to release key-value pairs. This parameter might be NULL.
 */
node_t *tree_erase(tree_t *tree, node_t *node, release_func release);

/**
 *	Gets the node corresponding to the given key in key-value pair.
 *	@param tree The pointer to the tree_t structure.
 *	@param node The pointer to the key-value pair with the given key.
 *	@return The pointer to the node_t structure which is a result of operation.
 */
node_t *tree_find(const tree_t *tree, const void *key);

/**
 *	Gets the first node of the tree.
 *	@param tree The pointer to the tree_t structure.
 *	@return The pointer to the node_t structure which is a result of operation, or NULL if the tree has no elements.
 *
 */
node_t *tree_first(const tree_t *tree);

/**
 *	Gets the last node of the tree.
 *	@param tree The pointer to the tree_t structure.
 *	@return The pointer to the node_t structure which is a result of operation, or NULL if the tree has no elements.
 */
node_t *tree_last(const tree_t *tree);

/**
 *	Gets the next node of the tree.
 *	@param tree The pointer to the tree_t structure.
 *	@param node The pointer to the given node.
 *	@return The pointer to the node_t structure which is a result of operation, or NULL if there are no more key-value pairs.
 */
node_t *tree_next(const tree_t *tree, const node_t *node);

/**
 *	Gets the next node of the tree.
 *	@param tree The pointer to the tree_t structure.
 *	@param node The pointer to the given node.
 *	@return The pointer to the node_t structure which is a result of operation, or NULL if there are no more key-value pairs.
 */
node_t *tree_prev(const tree_t *tree, const node_t *node);

/**
 *	Returns the pointer to the key-value pair.
 *	@param tree The pointer to the tree_t structure.
 *	@param node The pointer to the given node.
 *	@return The pointer to the key-value pair if node exists, otherwise returns NULL.
 */
void *tree_data(const tree_t *tree, const node_t *node);

/**
 *	Gets the number of nodes in a tree.
 *	@param tree The pointer to the tree_t structur
 *	@return Number of nodes in a tree.
 */
int tree_size(const tree_t *tree);

#endif // __DRAFT_DSLIB_H__