/******************************************************************************
 *	File:			platform.h
 *
 *	Created by:		Ivan Shishkin <codingdude@gmail.com>
 *
 *	History:		Created 19/01/13
 *
 *	Copyright (c) 2013 All rights reserved
 *
 *****************************************************************************/

#ifndef __DRAFT_PLATFORM_H__
#define __DRAFT_PLATFORM_H__

#define _USE_MATH_DEFINES

#include <stdlib.h>
#include <memory.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <stdarg.h>
#include <float.h>

#if defined(_MSC_VER)
#if (_MSC_VER < 1600)

#define NOMINMAX
#include <basetsd.h>
#include <stdint.h>

typedef UINT64		uint64_t;
typedef INT64		int64_t;
typedef UINT32		uint32_t;
typedef INT32		int32_t;
typedef UINT16		uint16_t;
typedef INT16		int16_t;
typedef UINT8		uint8_t;
typedef INT8		int8_t;
typedef	UINT_PTR	uintptr_t;
typedef	INT_PTR		intptr_t;

const int16_t INT16_MIN = -32768;

const int16_t INT16_MAX = 0x7fff;
const int32_t INT32_MAX = 0x7fffffff;

const uint8_t UINT8_MAX = 0xff;
const uint16_t UINT16_MAX = 0xffff;
const uint32_t UINT32_MAX = 0xffffffff;
const uint64_t UINT64_MAX = 0xffffffffffffffffull;

#ifdef _WIN64
typedef UINT64 uint_fast32_t;
typedef INT64 int_fast32_t;
typedef UINT64 uint_fast16_t;
typedef INT64 int_fast16_t;

const uint_fast32_t UINT_FAST32_MAX = UINT64_MAX;

#else
typedef _w64 UINT32 uint_fast32_t;
typedef _w64 INT32 int_fast32_t;
typedef _w64 UINT32 uint_fast16_t;
typedef _w64 INT32 int_fast16_t;

const uint32_t UINT_FAST32_MAX = UINT32_MAX;

#endif // _WIN64
#else
#include <stdint.h>
#endif

#define debug_break() _asm { int 3 }
#elif defined(__GNUC__)

#include <stdint.h>
#include <inttypes.h>

#if defined(__APPLE__)
#include <TargetConditionals.h>
#if TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR
#define debug_break() kill(getpid(), SIGINT);
#elif TARGET_OS_MAC
#define debug_break() asm("int $3")
#endif
#else
#define debug_break() asm("int $3")
#endif

#else
#error unknown build environment
#endif

#endif // __DRAFT_PLATFORM_H__