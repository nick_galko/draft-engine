/******************************************************************************
 *	File:			debug.c
 *
 *	Created by:		Ivan Shishkin <codingdude@gmail.com>
 *
 *	History:		Created 19/01/13
 *
 *	Copyright (c) 2013 All rights reserved
 *
 *****************************************************************************/

#include "platform.h"
#include "types.h"
#include "debug.h"

static void default_output_func(const char *msg) {
	puts(msg);
}

static debug_output_func output_func = default_output_func;

debug_output_func debug_redirect_output(debug_output_func out) {
	debug_output_func prev = output_func;
	output_func = out;
	return(prev);
}

debug_output_func debug_output() {
	return(output_func);
}

debug_output_func debug_default_output() {
	return(default_output_func);
}

void debug_message(const char *format, ...) {
	va_list args;
	int length;
	char *message, c;

	va_start(args, format);
#if defined(_MSC_VER)
	length = _vscprintf(format, args);
#else
	length = vsnprintf(&c, 1, format, args);
#endif
	if (length > 0) {
		message = (char*) malloc(length + 10);
		vsprintf(message, format, args);
		output_func(message);
		free(message);
	}
	va_end(args);
}

void debug_warning(const char *format, ...) {
	int length, pos;
	va_list args;
	char *message, c;

	const char *prefix = "!WARNING: ";

	va_start(args, format);
#if defined(_MSC_VER)
	length = _vscprintf(format, args);
#else
	length = vsnprintf(&c, 1, format, args);
#endif
	if (length > 0) {
		pos = strlen(prefix);
		length += pos + 10;
		message = (char*) malloc(length);
		sprintf(message, "%s", prefix);
		vsprintf(message + pos, format, args);
		output_func(message);
		free(message);
	}
	va_end(args);
}

void debug_error(const char *format, ...) {
	int length, pos;
	va_list args;
	char *message, c;

	const char *prefix = "!FATAL ERROR: ";

	va_start(args, format);
#if defined(_MSC_VER)
	length = _vscprintf(format, args);
#else
	length = vsnprintf(&c, 1, format, args);
#endif
	if (length > 0) {
		pos = strlen(prefix);
		length += pos + 10;
		message = (char*) malloc(length);
		sprintf(message, "%s", prefix);
		vsprintf(message + pos, format, args);
		output_func(message);
		free(message);
	}
	va_end(args);

	exit(0);
}

void debug_info(const char *msg, const char *file, int line) {
	char *message, codestr[255];
	const char *slash, *slash2, *name;
	int length;

	slash = strrchr(file, '\\');
	slash2 = strrchr(file, '/');
	if (slash < slash2)
		slash = slash2;

	name = slash ? slash + 1: file;

	sprintf(codestr, "%s(%i)", name, line);

	length = strlen(msg);
	if (length) {
		length += strlen(codestr) + 10;
		message = (char*) malloc(length);
		sprintf(message, "%s at %s", msg, codestr);
		output_func(msg);
		free(message);
	}
}
