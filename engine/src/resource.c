/******************************************************************************
 *	File:			resource.c
 *
 *	Created by:		Ivan Shishkin <codingdude@gmail.com>
 *
 *	History:		Created 02/02/13
 *
 *	Copyright (c) 2013 All rights reserved
 *
 *****************************************************************************/

#include "platform.h"
#include "types.h"
#include "debug.h"
#include "dslib.h"
#include "filesystem.h"
#include "render.h"
#include "resource.h"
#include "collision.h"

enum entity_file_format {
	TYPE_CHUNK_ID,
	TRANSFORMS_CHUNK_ID,
	LIGHT_CHUNK_ID,
	SURFACE_CHUNK_ID
};

enum surface_file_format {
	MESH_CHUNK_ID,
	MAPS_CHUNK_ID
};

enum mesh_file_format {
	INDEX_CHUNK_ID,
	VERTEX_CHUNK_ID,
	BBOX_CHUNK_ID
};

/******************************************************************************
*	Resource implementation
******************************************************************************/

static tree_t *resources = NULL;

static int resource_compare_func(const void *key1, const void *key2) {
	int res;
	resource_t *r1, *r2;

	debug_assert(key1);
	debug_assert(key2);

	r1 = (resource_t*) key1;
	r2 = (resource_t*) key2;
	res = strcmp(r1->name, r2->name);

	return(res);
}

static void resource_release_func(void *data) {
	resource_t *r;

	debug_assert(data);
	r = (resource_t*) data;
	if (r->loaded || r->prepared)
		r->unload_fn(data);
	free(r->name);
	free(data);
}

void resource_system_init() {
	resource_system_shutdown();
	resources = tree_create(resource_compare_func);
	debug_message("resource system has been successfuly initialized");
}

void resource_system_shutdown() {
	if (resources) {
		tree_destroy(resources, resource_release_func);
		resources = NULL;
		debug_message("resource system has been shutted down");
	}
}

resource_t *resource_get(const char *name) {
	node_t *node;
	resource_t key, *val;

	if (!resources)
		debug_error("resource system is used but not initialized");

	val = NULL;
	key.name = strdup(name);
	node = tree_find(resources, &key);
	if (node) {
		val = (resource_t*) tree_data(resources, node);
		val->refs++;
	}

	free(key.name);

	return(val);
}

int resource_exists(const char *name) {
	resource_t key;
	int res = 0;

	debug_assert(name && name[0]);
	key.name = strdup(name);
	if (tree_find(resources, &key))
		res = 1;
	free(key.name);

	return(res);
}

int resource_release(resource_t *r) {
	int count;
	node_t *node;
	char *name;

	if (!resources)
		debug_error("resource system is used but not initialized");

	debug_assert(r);
	count = --r->refs;
	debug_assert(count >= 0);
	if (count == 0) {
		name = strdup(r->name);
		node = tree_find(resources, r);
		if (node) {
			tree_erase(resources, node, resource_release_func);
			debug_message("resource '%s' has been released successfuly", name);
		} else
			debug_warning("can't release resource '%s', not found", name);
		free(name);
	}

	return(count);
}

void resource_destroy(resource_t *r) {
	int k = 0;
	while (resource_release(r)) {
		debug_assert(k++ < 99999);
	}
}

const char *resource_name(const resource_t *r) {
	debug_assert(r);
	return(r->name);
}

int resource_type(const resource_t *r) {
	debug_assert(r);
	return(r->type);
}

int resource_loaded(const resource_t *r) {
	debug_assert(r);
	return(r->loaded);
}

int resource_prepared(const resource_t *r) {
	debug_assert(r);
	return(r->prepared);
}

int resource_reference_count(const resource_t *r) {
	debug_assert(r);
	return(r->refs);
}

/******************************************************************************
*	Mesh implementation
******************************************************************************/

static void *set_s8(void *p, int8_t val) {
	int8_t * s8p = (int8_t*) p;
	*s8p = val;
	return(++s8p);
}

static void *set_s16(void *p, int16_t val) {
	int16_t * s16p = (int16_t*) p;
	*s16p = val;
	return(++s16p);
}

static void *set_s32(void *p, int32_t val) {
	int32_t * s32p = (int32_t*) p;
	*s32p = val;
	return(++s32p);
}

static void *set_f32(void *p, float val) {
	float * f32p = (float*) p;
	*f32p = val;
	return(++f32p);
}

static void mesh_set_vertex_declaration(mesh_t *m, const mesh_data_t *md) {
	int i, np, nn, nt, nbn, nb, nw, nm, ntc, count = 0;
	array_t *uvmap;

	debug_assert(m);
	debug_assert(md);

	// declare the vertex structure
	np = array_size(md->points);
	if (np) {
		m->vdecl |= VE_POINT;
		m->vsize = 12;
		count = np;
	}
	nn = array_size(md->normals);
	if (nn) {
		if (!count)
			count = nn;
		if (count != nn)
			goto fail;
		m->vdecl |= VE_NORMAL;
		m->vsize += 12;
	}
	nt = array_size(md->tangents);
	if (nt) {
		if (!count)
			count = nt;
		if (count != nt)
			goto fail;
		m->vdecl |= VE_TANGENT;
		m->vsize += 12;
	}
	nbn = array_size(md->binormals);
	if (nbn) {
		if (!count)
			count = nbn;
		if (count != nbn)
			goto fail;
		m->vdecl |= VE_BINORMAL;
		m->vsize += 12;
	}
	nb = array_size(md->bones);
	if (nb) {
		if (!count)
			count = nb;
		if (count != nb)
			goto fail;
		m->vdecl |= VE_BONES;
		m->vsize += 4;
	}
	nw = array_size(md->weights);
	if (nw) {
		if (!count)
			count = nw;
		if (count != nw)
			goto fail;
		m->vdecl |= VE_WEIGHTS;
		m->vsize += 8;
	}
	nm = array_size(md->uv_maps);
	if (nm < 0 || nm > 8)
		goto fail;
	if (nm != 0) {
		m->vdecl |= (VE_TEXCOORDS1 << (nm-1));
		m->vsize += 8 * nm;
	}
	for(i = 0; i < nm; i++) {
		uvmap = *(array_t**) array_at(md->uv_maps, i);
		ntc = array_size(uvmap);
		if (!count)
			count = ntc;
		if (count != ntc)
			goto fail;
	}
	m->vcount = count;

	return;

fail:
	// normally we never got here
	debug_error("mesh_set_vertex_declaration : mesh '%s' has invalid vertex format", m->resource.name);
}

mesh_t *mesh_create(const char *name) {
	int size;
	mesh_t *m;

	debug_assert(name && name[0]);
	if (!name || !name[0]) {
		debug_warning("mesh_create : invalid parameter");
		return(NULL);
	}

	if (resource_exists(name))
		debug_error("mesh_create : resource '%s' already exists", name);

	size = sizeof(mesh_t);
	m = (mesh_t*) malloc(size);
	memset(m, 0, size);
	m->resource.name = strdup(name);
	m->resource.refs = 1;
	m->resource.type = RT_MESH;
	m->resource.unload_fn = (resource_unload_func) mesh_unload;
	m->vbuffer = 0;
	m->ibuffer = 0;

	tree_insert(resources, m);

	return(m);
}

int mesh_release(mesh_t *m) {
	return(resource_release((resource_t*) m));
}

void mesh_destroy(mesh_t *m) {
	resource_destroy((resource_t*) m);
}

int mesh_load(mesh_t *m) {
	int ib, vb;
	reader_t *r;
	int res = 0;

	debug_assert(m);
	if (!m) {
		debug_warning("mesh_load : null referenced pointer");
		return(0);
	}
	if (m->resource.loaded) {
		debug_message("mesh '%s' already loaded", m->resource.name);
		return(1);
	}

	debug_message("start loading mesh '%s'", m->resource.name);
	if (!m->resource.prepared) {
		r = fs_reader_open();
		if (!fs_reader_load(r, PA_MESHES, m->resource.name)) {
			debug_warning("can't load mesh '%s',"
				"resource not found", m->resource.name);
		} else
			res = mesh_read(m, r);
		fs_reader_close(r);
	}
	if (res) {
		ib = render_create_index_buffer(
			array_size(m->idata) / 2,
			array_first(m->idata)
			);

		vb = render_create_vertex_buffer(
			m->vsize,
			m->vcount,
			array_first(m->vdata)
			);

		if (ib == 0 || vb == 0) {
			debug_warning("mesh_load : unable to create "
				"hardware buffers for mesh '%s'", m->resource.name);
			return(0);
		}

		m->icount = array_size(m->idata) / 2;
		array_destroy(m->idata);
		m->ibuffer = ib;
		array_destroy(m->vdata);
		m->vbuffer = vb;

		m->resource.prepared = 0;
		m->resource.loaded = 1;

		debug_message("mesh '%s' successfully loaded", m->resource.name);
	}

	return(res);
}

void mesh_unload(mesh_t *m) {
	if (!m) {
		debug_warning("mesh_unload : null referenced pointer");
		return;
	}
	if (!m->resource.loaded && !m->resource.prepared)
		return;

	// unload
	if (m->resource.loaded) {
		render_destroy_index_buffer(m->ibuffer);
		render_destroy_vertex_buffer(m->vbuffer);
		m->resource.loaded = 0;
		m->vbuffer = 0;
		m->ibuffer = 0;
	}
	// unprepare
	else if (m->resource.prepared) {
		array_destroy(m->idata);
		array_destroy(m->vdata);
		m->idata = NULL;
		m->vdata = NULL;
		m->resource.prepared = 0;
	}

	// common data
	m->vdecl = 0;
	m->vsize = 0;
	m->vcount = 0;

	bbox_identity(&m->bbox);

	debug_message("mesh '%s' successfully unloaded", m->resource.name);
}

int mesh_read(mesh_t *m, reader_t *r) {
	reader_t *r1, *r2;
	int size;
	void *p1, *p2;

	debug_message("reading mesh data '%s'", m->resource.name);

	if (!m || !r) {
		debug_warning("mesh_read : invalid parameter");
		return(0);
	}
	
	mesh_unload(m);

	r1 = reader_find(r, MESH_CHUNK_ID, 0);
	if (!r1)
		goto fail;
	
	r2 = reader_find(r1, INDEX_CHUNK_ID, 0);
	if (r2) {
		size = reader_size(r2);
		p1 = reader_data(r2);
		p2 = (void*)((int)p1 + size);
		m->idata = array_create(sizeof(char));
		array_append_range(m->idata, p1, p2);
		fs_reader_close(r2);
	} else
		goto fail;

	r2 = reader_find(r1, VERTEX_CHUNK_ID, 0);
	if (r2) {
		m->vcount = reader_r_u32(r2);
		m->vdecl = reader_r_u32(r2);
		m->vsize = reader_r_u32(r2);
		size = reader_r_u32(r2);
		p1 = reader_ptr(r2);
		p2 = (void*)((int)p1 + size - 1);
		m->vdata = array_create(sizeof(char));
		array_append_range(m->vdata, p1, p2);
		fs_reader_close(r2);
	} else
		goto fail;

	r2 = reader_find(r1, BBOX_CHUNK_ID, 0);
	if (r2)
		reader_read(r2, &m->bbox, sizeof(bbox_t));
	else
		goto fail;

	fs_reader_close(r1);
	m->resource.prepared = 1;
	return(1);

fail:
	fs_reader_close(r1);
	array_clear(m->idata);
	array_clear(m->vdata);
	m->resource.prepared = 0;
	debug_warning("mesh_read : invalid mesh '%s' file format", m->resource.name);
	return(0);
}

int mesh_write(mesh_t *m, writer_t *w) {
	writer_t *w1, *w2;

	if (!m || !w) {
		debug_warning("mesh_write : invalid parameters");
		return(0);
	}
	if (!m->resource.prepared) {
		debug_warning("unable to write mesh '%s', resource is empty", m->resource.name);
		return(0);
	}

	// create mesh chunk
	w1 = fs_writer_open();
	writer_set_id(w1, MESH_CHUNK_ID);
	debug_message("start writing mesh '%s'", m->resource.name);

	// write indices
	w2 = fs_writer_open();
	writer_set_id(w2, INDEX_CHUNK_ID);
	writer_write(w2, array_first(m->idata), array_size(m->idata) * array_stride(m->idata));
	writer_append(w2, w1);
	fs_writer_close(w2);
	debug_message("\t* %i indices saved", array_size(m->idata));

	// write vertices
	w2 = fs_writer_open();
	writer_set_id(w2, VERTEX_CHUNK_ID);
	writer_w_u32(w2, m->vcount);
	writer_w_u32(w2, m->vdecl);
	writer_w_u32(w2, m->vsize);
	writer_w_u32(w2, array_size(m->vdata) * array_stride(m->vdata));
	writer_write(w2, array_first(m->vdata), array_size(m->vdata) * array_stride(m->vdata));
	writer_append(w2, w1);
	fs_writer_close(w2);
	debug_message("\t* %i vertices saved", m->vcount);

	// write bounding box
	w2 = fs_writer_open();
	writer_set_id(w2, BBOX_CHUNK_ID);
	writer_write(w2, &m->bbox, sizeof(bbox_t));
	writer_append(w2, w1);
	fs_writer_close(w2);
	debug_message("\t* bounding box saved");

	// write whole mesh
	writer_append(w1, w);
	fs_writer_close(w1);
	debug_message("mesh '%s' data written", m->resource.name);

	return(1);
}

int mesh_read_file(mesh_t *m, const char *file) {
	int res;
	reader_t *r;

	res = 0;
	r = fs_reader_open();
	if (fs_reader_load_file(r, file)) {
		if (mesh_read(m, r))
			res = 1;
		fs_reader_close(r);
	}

	return(res);
}

int mesh_write_file(mesh_t *m, const char *file) {
	int res;
	writer_t *w;

	res = 0;
	w = fs_writer_open();
	if (mesh_write(m, w)) {
		if (fs_writer_save_file(w, file))
			res = 1;
		fs_writer_close(w);
	}

	return(res);
}

void mesh_build(mesh_t *m, const mesh_data_t *md) {
	int i, j;
	void *pv, *first, *last;
	float *pf;
	int8_t *ps8;
	array_t *uv_map;

	debug_assert(m);
	debug_assert(md);

	if (!m || !md) {
		debug_warning("mesh_build : invalid parameter");
		return;
	}

	if (array_stride(md->indices) != sizeof(int16_t)) {
		debug_warning("mesh '%s' has invalid face format", m->resource.name);
		return;
	}

	mesh_unload(m);

	bbox_identity(&m->bbox);

	first = array_first(md->indices);
	last = array_last(md->indices);
	m->idata = array_create_from_existing(first, last, sizeof(int16_t));

	mesh_set_vertex_declaration(m, md);
	m->vdata = array_create(m->vsize);
	array_resize(m->vdata, m->vcount);
	for (i = 0; i < m->vcount; i++) {
		pv = array_at(m->vdata, i);
		if (m->vdecl & VE_POINT) {
			pf = (float*) array_at(md->points, i);
			pv = set_f32(pv, pf[0]);
			pv = set_f32(pv, pf[1]);
			pv = set_f32(pv, pf[2]);
			bbox_merge_point(&m->bbox, &m->bbox, (vector3_t*) pf);
		}
		if (m->vdecl & VE_NORMAL) {
			pf = (float*) array_at(md->normals, i);
			pv = set_f32(pv, pf[0]);
			pv = set_f32(pv, pf[1]);
			pv = set_f32(pv, pf[2]);
		}
		if (m->vdecl & VE_TANGENT) {
			pf = (float*) array_at(md->tangents, i);
			pv = set_f32(pv, pf[0]);
			pv = set_f32(pv, pf[1]);
			pv = set_f32(pv, pf[2]);
		}
		if (m->vdecl & VE_BINORMAL) {
			pf = (float*) array_at(md->binormals, i);
			pv = set_f32(pv, pf[0]);
			pv = set_f32(pv, pf[1]);
			pv = set_f32(pv, pf[2]);
		}
		if (m->vdecl & VE_BONES) {
			ps8 = (int8_t*) array_at(md->bones, i);
			pv = set_s8(pv, ps8[0]);
			pv = set_s8(pv, ps8[1]);
			pv = set_s8(pv, ps8[2]);
			pv = set_s8(pv, ps8[3]);
		}
		if (m->vdecl & VE_WEIGHTS) {
			pf = (float*) array_at(md->weights, i);
			pv = set_s16(pv, pf[0] * 65535);
			pv = set_s16(pv, pf[1] * 65535);
			pv = set_s16(pv, pf[2] * 65535);
			pv = set_s16(pv, pf[3] * 65535);
		}
		for (j = 0; j < array_size(md->uv_maps); j++) {
			uv_map = *(array_t**) array_at(md->uv_maps, j);
			pf = (float*) array_at(uv_map, i);
			pv = set_f32(pv, pf[0]);
			pv = set_f32(pv, pf[1]);
		}
	}

	m->resource.prepared = 1;
}


void mesh_set_bbox(mesh_t *m, const bbox_t *bb) {
	debug_assert(m);
	debug_assert(bb);
	m->bbox = *bb;
}

int mesh_get_vb(const mesh_t *m) {
	debug_assert(m);
	return(m->vbuffer);
}

int mesh_get_ib(const mesh_t *m) {
	debug_assert(m);
	return(m->ibuffer);
}

bbox_t *mesh_get_bbox(mesh_t *m) {
	debug_assert(m);
	return(&m->bbox);
}

int mesh_get_vertex_decl(const mesh_t *m) {
	debug_assert(m);
	return(m->vdecl);
}

int mesh_get_vertex_count(const mesh_t *m) {
	debug_assert(m);
	return(m->vcount);
}

/******************************************************************************
*	Texture implementation
******************************************************************************/

static int is_power_of_2(int side) {
	int test = 2;
	while (test <= 2048) {
		if (test == side)
			return(1);
		test = test << 1;
	}
	return(0);
}

static int texture_validate(texture_t *t) {
	if (t->depth > 1 && t->layers > 1) {
		debug_warning("unable to interpret texture '%s'"
			" both as 3D texture and 2D texture array");
		return(0);
	}
	if (t->format , 0 || t->format > 28) {
		debug_warning("texture '%s' has unknown pixel format");
		return(0);
	}
	if (t->channel < 0 || t->channel > 12) {
		debug_warning("texture '%s' has unknown color channel type");
		return(0);
	}
	if (t->height > 2048) {
		debug_warning("texture '%s' side is greater than 2048 pixels");
		return(0);
	}
	if (!is_power_of_2(t->width) || !is_power_of_2(t->height)) {
		debug_warning("texture '%s' size is not a power of two");
		return(0);
	}
	return(1);
}

texture_t *texture_create(const char *name) {
	texture_t *t;

	if (!name || !name[0])
		debug_error("texture_create : invalid parameter");

	if (resource_exists(name))
		debug_error("texture_create : resource '%s' already exists", name);

	t = (texture_t*) malloc(sizeof(texture_t));
	t->resource.name = strdup(name);
	t->resource.type = RT_TEXTURE;
	t->resource.refs = 1;
	t->resource.unload_fn = (resource_unload_func) texture_unload;
	t->resource.loaded = 0;
	t->resource.prepared = 0;

	tree_insert(resources, t);

	return(t);
}

int texture_release(texture_t *t) {
	resource_release((resource_t*)t);
}

void texture_destroy(texture_t *t) {
	resource_destroy((resource_t*)t);
}

int texture_load(texture_t *t) {
	reader_t *r;
	void *raw;
	int texmap, res = 0;

	debug_assert(t);
	if (!t) {
		debug_warning("texture_load : invalid parameter");
		return(res);
	}

	if (t->resource.loaded) {
		debug_message("texture '%s' already loaded", t->resource.name);
		return(1);
	}
	if (!t->resource.prepared) {
		r = fs_reader_open();
		if (!fs_reader_load(r, PA_TEXTURES, t->resource.name)) {
			debug_warning("can't load texture '%s',"
				"resource not found", t->resource.name);
			fs_reader_close(r);
			return(res);
		} else
			res = texture_read(t, r);
		fs_reader_close(r);
	}
	if (res) {
		if (t->depth > 1)
			texmap = render_create_texmap_3d(
				t->width,
				t->height,
				t->depth,
				t->mipmaps,
				t->channel,
				t->format,
				t->flags,
				array_at(t->raw, 0)
			);
		else
			texmap = render_create_texmap_2d(
				t->width,
				t->height,
				t->layers,
				t->mipmaps,
				t->channel,
				t->format,
				t->flags,
				array_at(t->raw, 0)
			);

		array_destroy(t->raw);
		t->texmap = texmap;
		t->resource.loaded = 1;
		t->resource.prepared = 0;
	}

	debug_message("texture '%s' successfully loaded", t->resource.name);

	return(res);
}

void texture_unload(texture_t *t) {
	if (!t) {
		debug_warning("texture_unload : null referenced pointer");
		return;
	}
	if (t->resource.loaded)
		render_destroy_texmap(t->texmap);
	if (t->resource.prepared)
		array_destroy(t->raw);

	t->flags = 0;

	debug_message("texture '%s' successfully unloaded", t->resource.name);
}

int texture_read(texture_t *t, reader_t *r) {
	void *first, *last;
	int size, faces;
	int order, bits;
	char magic[4];
	uint64_t format;

	debug_assert(t);
	debug_assert(r);

	if (!t || !r || !r->size) {
		debug_warning("texture_read : invalid parameter");
		return(0);
	}

	magic[0] = reader_r_s8(r);
	magic[1] = reader_r_s8(r);
	magic[2] = reader_r_s8(r);
	magic[3] = 0; reader_advance(r, 1);

	if (strcmp(magic, "PVR") != 0) {
		debug_warning("can't read texture'%s', wrong file format", t->resource.name);
		return(0);
	}

	if (t->resource.loaded)
		render_destroy_texmap(t->texmap);
	if (t->resource.prepared)
		array_destroy(t->raw);
	t->flags = 0;

	// reading the pixel format
	reader_advance(r, 4);
	format = reader_r_u64(r);
	order = ((uint32_t)(format)) & 0xffffffff;
	bits = ((uint32_t)(format >> 32)) & 0xffffffff;
	if (bits) {
		if ((order == 'abgr' && bits == 0x08080808)
			|| (order == '\0bgr' && bits == 0x00080808))
			t->format = RGBA8888;
		else if ((order == 'bgra' && bits == 0x08080808)
			|| (order == 'bgr\0' && bits == 0x08080800))
			t->format = RGBA8888;
		else
			debug_error("can't read texture '%s', unknown pixel format", t->resource.name);
	} else
		t->format = format;

	// reading the channel type
	reader_advance(r, 4);
	t->channel = reader_r_u32(r);
	if (t->channel == 0)
		t->channel = CHANNEL_UBYTE;
	if (t->channel == 1)
		t->channel = CHANNEL_SBYTE;
	if (t->channel == 4)
		t->channel = CHANNEL_USHORT;
	if (t->channel == 5)
		t->channel = CHANNEL_SSHORT;
	if (t->channel == 8)
		t->channel = CHANNEL_UINT;
	if (t->channel == 9)
		t->channel = CHANNEL_SINT;

	// reading dimensions
	t->height = reader_r_u32(r);
	t->width = reader_r_u32(r);
	t->depth = reader_r_u32(r);
	if (t->depth > 1)
		t->flags |= TEXMAP_3D;

	// reading the size of a texture array
	t->layers = reader_r_u32(r);
	if (t->layers > 1)
		t->flags |= TEXMAP_2D_ARRAY;
	else if (!(t->flags & TEXMAP_3D))
		t->flags |= TEXMAP_2D;

	// reading the number of a cubemap faces
	faces = reader_r_u32(r);
	if (faces == 6)
		t->flags |= TEXMAP_CUBEMAP;

	// reading the number of a mipmaps
	t->mipmaps = reader_r_u32(r);

	if (!texture_validate(t))
		return(0);

	size = reader_r_u32(r);
	if (size > 0)
		reader_advance(r, size);

	size = reader_size(r) - reader_tell(r);
	first = reader_ptr(r);
	last = (void*)((int)first + size - 1);
	t->raw = array_create_from_existing(first, last, sizeof(char));

	t->resource.prepared = 1;

	debug_message("texture '%s' data successfully readed", t->resource.name);

	return(1);
}

int texture_write(texture_t *t, writer_t *w) {
	void *data;
	int size;

	debug_assert(w);
	debug_assert(t);

	if (!w || !t) {
		debug_warning("texture_write : invalid parameter");
		return(0);
	}

	if (!t->resource.prepared) {
		debug_warning("can't write texture '%s', not initialized");
		return(0);
	}

	// writting magic
	writer_w_u32(w, '\3RVP');

	// writting flags
	writer_w_u32(w, 0);

	// writting pixel format
	writer_w_u64(w, t->format);

	// writting colour space
	writer_w_u32(w, 1);

	// writting channel type
	writer_w_u32(w, t->channel);

	// writting height
	writer_w_u32(w, t->height);

	// writting width
	writer_w_u32(w, t->width);

	// writting depth
	writer_w_u32(w, t->depth);

	// writing texture array size
	writer_w_u32(w, t->layers);

	// writting number of cubemap faces
	if (t->flags & TEXMAP_CUBEMAP)
		writer_w_u32(w, 6);
	else
		writer_w_u32(w, 1);

	// writing mipmap count
	writer_w_u32(w, t->mipmaps);

	// writing metadata
	writer_w_u32(w, 0);

	// writing pixels
	data = array_first(t->raw);
	size = array_size(t->raw);
	writer_write(w, data, size);

	debug_message("texture '%s' successfully writed");

	return(1);
}

int texture_read_file(texture_t *t, const char *file) {
	reader_t *r;
	int res = 0;

	r = fs_reader_open();
	if (fs_reader_load_file(r, file)) {
		if (texture_read(t, r))
			res = 1;
	}

	fs_reader_close(r);

	return(res);
}

int texture_write_file(texture_t *t, const char *file) {
	writer_t *w;
	int res = 0;

	w = fs_writer_open();
	if (texture_write(t, w)) {
		if (fs_writer_save_file(w, file))
			res = 1;
	}

	fs_writer_close(w);

	return(res);
}

int texture_width(texture_t *t) {
	debug_assert(t);
	return(t->width);
}

int texture_height(texture_t *t) {
	debug_assert(t);
	return(t->height);
}

int texture_mipmaps(texture_t *t) {
	debug_assert(t);
	return(t->mipmaps);
}

int texture_format(texture_t *t) {
	debug_assert(t);
	return(t->format);
}

void *texture_data(texture_t *t) {
	debug_assert(t);
	return(array_first(t->raw));
}

int texture_size(texture_t *t) {
	debug_assert(t);
	return(array_size(t->raw));
}