/******************************************************************************
 *	File:			maths.c
 *
 *	Created by:		Ivan Shishkin <codingdude@gmail.com>
 *
 *	History:		Created 23/09/12
 *
 *	Copyright (c) 2012 All rights reserved
 *
 *****************************************************************************/

#include "platform.h"
#include "types.h"
#include "debug.h"
#include "maths.h"

static float fsign(float x) {
	return((x < 0) ? -1.0f : 1.0f);
}

/******************************************************************************
 * 2D Vector implementation
 *****************************************************************************/

float vector2_dot(const vector2_t *v1, const vector2_t *v2) {
	debug_assert(v1); debug_assert(v2);
	return(v1->x * v2->x
		+ v1->y * v2->y);
}

float vector2_cross(const vector2_t *v1, const vector2_t *v2) {
	debug_assert(v1); debug_assert(v2);
	return(v1->x * v2->y
		- v1->y * v2->x);
}

vector2_t *vector2_add(vector2_t *out, const vector2_t *v1, const vector2_t *v2) {
	debug_assert(out); debug_assert(v1); debug_assert(v2);
	out->x = v1->x + v2->x;
	out->y = v1->y + v2->y;
	return(out);
}

vector2_t *vector2_substract(vector2_t *out, const vector2_t *v1, const vector2_t *v2) {
	debug_assert(out); debug_assert(v1); debug_assert(v2);
	out->x = v1->x - v2->x;
	out->y = v1->y - v2->y;
	return(out);
}

float vector2_length(const vector2_t *v) {
	debug_assert(v); return(sqrtf(vector2_length_square(v)));
}

float vector2_length_square(const vector2_t *v) {
	debug_assert(v);
	return(v->x * v->x
		+ v->y * v->y);
}

vector2_t *vector2_scale(vector2_t *out, const vector2_t *v, float s) {
	debug_assert(out); debug_assert(v);
	out->x = v->x * s;
	out->y = v->y * s;
	return(out);
}

vector2_t *vector2_normalize(vector2_t *out, const vector2_t *v) {
	float length = vector2_length(v);
	if (length != 0.0f) {
		out->x = v->x / length;
		out->y = v->y / length;
	}
	return(out);
}

vector2_t *vector2_minimize(vector2_t *out, const vector2_t *v1, const vector2_t *v2) {
	debug_assert(out); debug_assert(v1); debug_assert(v2);
	out->x = min(v1->x, v1->y);
	out->y = min(v1->y, v2->y);
	return(out);
}

vector2_t *vector2_maximize(vector2_t *out, const vector2_t *v1, const vector2_t *v2) {
	debug_assert(out); debug_assert(v1); debug_assert(v2);
	out->x = max(v1->x, v1->y);
	out->y = max(v1->y, v2->y);
	return(out);
}

vector2_t *vector2_transform(vector2_t *out, const vector2_t *v, const matrix_t *m) {
	float w;
	debug_assert(out); debug_assert(v); debug_assert(m);
	w = m->m41 * v->x + m->m42 * v->y + m->m44;
	debug_assert(w != 0.0f);
	out->x = (v->x * m->m11 + v->y * m->m12) / w;
	out->y = (v->x * m->m21 + v->y * m->m22) / w;
	return(out);
}

/******************************************************************************
 * 3D Vector implementation
 *****************************************************************************/

float vector3_dot(const vector3_t *v1, const vector3_t *v2) {
	debug_assert(v1); debug_assert(v2);
	return(v1->x * v2->x
		+ v1->y * v2->y
		+ v1->z * v2->z);
}

vector3_t *vector3_cross(vector3_t *out, const vector3_t *v1, const vector3_t *v2) {
	debug_assert(out); debug_assert(v1); debug_assert(v2);
	out->x = v1->y * v2->z - v1->z * v2->y;
	out->y = v1->z * v2->x - v1->x * v2->z;
	out->z = v1->x * v2->y - v1->y * v2->x;
	return(out);
}

vector3_t *vector3_add(vector3_t *out, const vector3_t *v1, const vector3_t *v2) {
	debug_assert(out); debug_assert(v1); debug_assert(v2);
	out->x = v1->x + v2->x;
	out->y = v1->y + v2->y;
	out->z = v1->z + v2->z;
	return(out);
}

vector3_t *vector3_substract(vector3_t *out, const vector3_t *v1, const vector3_t *v2) {
	debug_assert(out); debug_assert(v1); debug_assert(v2);
	out->x = v1->x - v2->x;
	out->y = v1->y - v2->y;
	out->z = v1->z - v2->z;
	return(out);
}

float vector3_length_square(const vector3_t *v) {
	debug_assert(v);
	return(v->x * v->x
		+ v->y * v->y
		+ v->z * v->z);
}

float vector3_length(const vector3_t *v) {
	debug_assert(v);
	return(sqrtf(vector3_length_square(v)));
}

vector3_t *vector3_scale(vector3_t *out, const vector3_t *v, float s) {
	debug_assert(out); debug_assert(v);
	out->x = v->x * s;
	out->y = v->y * s;
	out->z = v->z * s;
	return(out);
}

vector3_t *vector3_normalize(vector3_t *out, const vector3_t *v) {
	float length;
	debug_assert(out);
	length = vector3_length(v);
	if (length != 0.0f) {
		out->x = v->x / length;
		out->y = v->y / length;
		out->z = v->z / length;
	}
	return(out);
}

vector3_t *vector3_minimize(vector3_t *out, const vector3_t *v1, const vector3_t *v2) {
	debug_assert(out); debug_assert(v1); debug_assert(v2);
	out->x = min(v1->x, v2->x);
	out->y = min(v1->y, v2->y);
	out->z = min(v1->z, v2->z);
	return(out);
}

vector3_t *vector3_maximize(vector3_t *out, const vector3_t *v1, const vector3_t *v2) {
	debug_assert(out); debug_assert(v1); debug_assert(v2);
	out->x = max(v1->x, v2->x);
	out->y = max(v1->y, v2->y);
	out->z = max(v1->z, v2->z);
	return(out);
}

vector3_t *vector3_transform(vector3_t *out, const vector3_t *v, const matrix_t *m) {
	float w;
	debug_assert(out); debug_assert(v); debug_assert(m);
	w = m->m41 * v->x + m->m42 * v->y + m->m43 * v->z + m->m44;
	debug_assert(w != 0.0f);
	out->x = (v->x * m->m11 + v->y * m->m12 + v->z * m->m13 + m->m14) / w;
	out->y = (v->x * m->m21 + v->y * m->m22 + v->z * m->m23 + m->m24) / w;
	out->z = (v->x * m->m31 + v->y * m->m32 + v->z * m->m33 + m->m34) / w;
	return(out);
}

/******************************************************************************
 * 4D Vector implementation
 *****************************************************************************/

float vector4_dot(const vector4_t *v1, const vector4_t *v2) {
	debug_assert(v1); debug_assert(v2);
	return(v1->x * v2->x
		+ v1->y * v2->y
		+ v1->z * v2->z
		+ v1->w * v2->w);
}

vector4_t *vector4_cross(vector4_t *out, const vector4_t *v1, const vector4_t *v2, const vector4_t *v3) {
	debug_assert(out); debug_assert(v1); debug_assert(v2); debug_assert(v3);
	out->x = v1->y * (v2->z * v3->w - v3->z * v2->w) - v1->z * (v2->y * v3->w - v3->y * v2->w) + v1->w * (v2->y * v3->z - v3->y * v2->z);
	out->y = -v1->x * (v2->z * v3->w - v2->w * v3->z) + v1->z * (v2->x * v3->w - v3->x * v2->w) - v1->w * (v2->x * v3->z - v3->x * v2->z);
	out->z = v1->x * (v2->y * v3->w - v2->w * v3->y) - v1->y * (v2->x * v3->w - v2->w * v3->x) + v1->w * (v1->x * v3->y - v2->y * v3->x);
	out->w = -v1->x * (v2->y * v3->z - v3->y * v2->z) + v1->y * (v2->x * v3->y - v2->z * v3->x) - v1->z * (v2->x * v3->y - v2->y * v3->x);
	return(out);
}

vector4_t *vector4_add(vector4_t *out, const vector4_t *v1, const vector4_t *v2) {
	debug_assert(out); debug_assert(v1); debug_assert(v2);
	out->x = v1->x + v2->x;
	out->y = v1->y + v2->y;
	out->z = v1->z + v2->z;
	out->w = v1->w + v2->w;
	return(out);
}

vector4_t *vector4_substract(vector4_t *out, const vector4_t *v1, const vector4_t *v2) {
	debug_assert(out); debug_assert(v1); debug_assert(v2);
	out->x = v1->x - v2->x;
	out->y = v1->y - v2->y;
	out->z = v1->z - v2->z;
	out->w = v1->w - v2->w;
	return(out);
}

float vector4_length_square(const vector4_t *v) {
	debug_assert(v); return v->x * v->x + v->y * v->y + v->z * v->z + v->w * v->w;
}

float vector4_length(const vector4_t *v) {
	debug_assert(v); return sqrtf(vector4_length_square(v));
}

vector4_t *vector4_scale(vector4_t *out, const vector4_t *v, float s) {
	debug_assert(out); debug_assert(v);
	out->x = v->x * s;
	out->y = v->y * s;
	out->z = v->z * s;
	out->w = v->w * s;
	return(out);
}

vector4_t *vector4_normalize(vector4_t *out, const vector4_t *v) {
	float length;
	debug_assert(out);
	length = vector4_length(v);
	if (length != 0.0f) {
		out->x = v->x / length;
		out->y = v->y / length;
		out->z = v->z / length;
		out->w = v->w / length;
	}
	return(out);
}

vector4_t *vector4_minimize(vector4_t *out, const vector4_t *v1, const vector4_t *v2) {
	debug_assert(out); debug_assert(v1); debug_assert(v2);
	out->x = min(v1->x, v2->x);
	out->y = min(v1->y, v2->y);
	out->z = min(v1->z, v2->z);
	out->w = min(v1->w, v2->w);
	return(out);
}

vector4_t *vector4_maximize(vector4_t *out, const vector4_t *v1, const vector4_t *v2) {
	debug_assert(out); debug_assert(v1); debug_assert(v2);
	out->x = max(v1->x, v2->x);
	out->y = max(v1->y, v2->y);
	out->z = max(v1->z, v2->z);
	out->w = max(v1->w, v2->w);
	return(out);
}

vector4_t *vector4_transform(vector4_t *out, const vector4_t *v, const matrix_t *m) {
	debug_assert(out); debug_assert(v); debug_assert(m);
	out->x = v->x * m->m11 + v->y * m->m12 + v->z * m->m13 + v->w * m->m14;
	out->y = v->x * m->m21 + v->y * m->m22 + v->z * m->m23 + v->w * m->m24;
	out->z = v->x * m->m31 + v->y * m->m32 + v->z * m->m33 + v->w * m->m34;
	out->w = v->x * m->m41 + v->y * m->m42 + v->z * m->m43 + v->w * m->m44;
	return(out);
}

/******************************************************************************
 * 4D Matrix implementation
 *****************************************************************************/

matrix_t *matrix_identity(matrix_t *out) {
	out->m11 = 1.0f; out->m12 = 0.0f; out->m13 = 0.0f; out->m14 = 0.0f;
	out->m21 = 0.0f; out->m22 = 1.0f; out->m23 = 0.0f; out->m24 = 0.0f;
	out->m31 = 0.0f; out->m32 = 0.0f; out->m33 = 1.0f; out->m34 = 0.0f;
	out->m41 = 0.0f; out->m42 = 0.0f; out->m43 = 0.0f; out->m44 = 1.0f;
	return(out);
}

int matrix_is_identity(const matrix_t *m) {
	debug_assert(m);
	return (
		m->m11 == 1.0f && m->m12 == 0.0f && m->m13 == 0.0f && m->m14 == 0.0f &&
		m->m21 == 0.0f && m->m22 == 1.0f && m->m23 == 0.0f && m->m24 == 0.0f &&
		m->m31 == 0.0f && m->m32 == 0.0f && m->m33 == 1.0f && m->m34 == 0.0f &&
		m->m41 == 0.0f && m->m42 == 0.0f && m->m43 == 0.0f && m->m44 == 1.0f
	);
}

float matrix_determinant(const matrix_t *m) {
	debug_assert(m);
	return (
		m->m11 * (m->m22 * (m->m33 * m->m44 - m->m43 * m->m34) - m->m23 * (m->m32 * m->m44 - m->m42 * m->m34) + m->m21 * (m->m32 * m->m43 - m->m42 * m->m33))
		- m->m12 * (m->m21 * (m->m33 * m->m44 - m->m43 * m->m34) - m->m23 * (m->m31 * m->m44 - m->m41 * m->m34) + m->m24 * (m->m31 * m->m43 - m->m41 * m->m33))
		+ m->m13 * (m->m21 * (m->m32 * m->m44 - m->m42 * m->m34) - m->m22 * (m->m31 * m->m44 - m->m41 * m->m34) + m->m24 * (m->m31 * m->m42 - m->m41 * m->m32))
		- m->m14 * (m->m21 * (m->m32 * m->m43 - m->m42 * m->m33) - m->m22 * (m->m31 * m->m43 - m->m41 * m->m33) + m->m23 * (m->m31 * m->m42 - m->m41 * m->m32))
	);
}

matrix_t *matrix_transpose(matrix_t *out, const matrix_t *m) {
	debug_assert(out); debug_assert(m);
	out->m11 = m->m11; out->m12 = m->m21; out->m13 = m->m31; out->m14 = m->m41;
	out->m21 = m->m12; out->m22 = m->m22; out->m23 = m->m32; out->m24 = m->m42;
	out->m31 = m->m13; out->m32 = m->m23; out->m33 = m->m33; out->m34 = m->m43;
	out->m41 = m->m14; out->m42 = m->m24; out->m43 = m->m34; out->m44 = m->m44;
	return(out);
}

void matrix_decompose(vector3_t *translation, quaternion_t *rotation, vector3_t *scale, const matrix_t *m) {
	matrix_t q;
	size_t row, col;
	float inv, dot, det;
	debug_assert(m); debug_assert(translation); debug_assert(rotation); debug_assert(scale);

	inv = 1.0f / sqrtf(m->m[0][0]*m->m[0][0]
		+ m->m[1][0]*m->m[1][0] +
		m->m[2][0]*m->m[2][0]);
	q.m[0][0] = m->m[0][0]*inv;
	q.m[1][0] = m->m[1][0]*inv;
	q.m[2][0] = m->m[2][0]*inv;

	dot = q.m[0][0]*m->m[0][1] + q.m[1][0]*m->m[1][1] +
		q.m[2][0]*m->m[2][1];
	q.m[0][1] = m->m[0][1]-dot*q.m[0][0];
	q.m[1][1] = m->m[1][1]-dot*q.m[1][0];
	q.m[2][1] = m->m[2][1]-dot*q.m[2][0];
	inv = 1.0f / sqrtf(q.m[0][1]*q.m[0][1] + q.m[1][1]*q.m[1][1] +
		q.m[2][1]*q.m[2][1]);
	q.m[0][1] *= inv;
	q.m[1][1] *= inv;
	q.m[2][1] *= inv;

	dot = q.m[0][0]*m->m[0][2] + q.m[1][0]*m->m[1][2] +
		q.m[2][0]*m->m[2][2];
	q.m[0][2] = m->m[0][2]-dot*q.m[0][0];
	q.m[1][2] = m->m[1][2]-dot*q.m[1][0];
	q.m[2][2] = m->m[2][2]-dot*q.m[2][0];
	dot = q.m[0][1]*m->m[0][2] + q.m[1][1]*m->m[1][2] +
		q.m[2][1]*m->m[2][2];
	q.m[0][2] -= dot*q.m[0][1];
	q.m[1][2] -= dot*q.m[1][1];
	q.m[2][2] -= dot*q.m[2][1];
	inv = 1.0f / sqrtf(q.m[0][2]*q.m[0][2] + q.m[1][2]*q.m[1][2] +
		q.m[2][2]*q.m[2][2]);
	q.m[0][2] *= inv;
	q.m[1][2] *= inv;
	q.m[2][2] *= inv;

	// guarantee that orthogonal matrix has determinant 1 (no reflections)
	det = q.m[0][0]*q.m[1][1]*q.m[2][2] + q.m[0][1]*q.m[1][2]*q.m[2][0] +
		q.m[0][2]*q.m[1][0]*q.m[2][1] - q.m[0][2]*q.m[1][1]*q.m[2][0] -
		q.m[0][1]*q.m[1][0]*q.m[2][2] - q.m[0][0]*q.m[1][2]*q.m[2][1];

	if ( det < 0.0 )
	{
		for (row = 0; row < 3; row++)
		for (col = 0; col < 3; col++)
			q.m[row][col] = -q.m[row][col];
	}

	// the translation component
	translation->v[0] = m->m[0][3];
	translation->v[1] = m->m[1][3];
	translation->v[2] = m->m[2][3];

	// the rotation component
	quaternion_rotation_matrix(rotation, &q);

	// the scaling component
	scale->v[0] = q.m[0][0]*m->m[0][0] + q.m[1][0]*m->m[1][0] +
		q.m[2][0]*m->m[2][0];
	scale->v[1] = q.m[0][1]*m->m[0][1] + q.m[1][1]*m->m[1][1] +
		q.m[2][1]*m->m[2][1];
	scale->v[2] = q.m[0][2]*m->m[0][2] + q.m[1][2]*m->m[1][2] +
		q.m[2][2]*m->m[2][2];
}

matrix_t *matrix_add(matrix_t *out, const matrix_t *m1, const matrix_t *m2) {
	debug_assert(out); debug_assert(m1); debug_assert(m2);

	out->m[0][0] = m1->m[0][0] + m2->m[0][0];
	out->m[0][1] = m1->m[0][1] + m2->m[0][1];
	out->m[0][2] = m1->m[0][2] + m2->m[0][2];
	out->m[0][3] = m1->m[0][3] + m2->m[0][3];

	out->m[1][0] = m1->m[1][0] + m2->m[1][0];
	out->m[1][1] = m1->m[1][1] + m2->m[1][1];
	out->m[1][2] = m1->m[1][2] + m2->m[1][2];
	out->m[1][3] = m1->m[1][3] + m2->m[1][3];

	out->m[2][0] = m1->m[2][0] + m2->m[2][0];
	out->m[2][1] = m1->m[2][1] + m2->m[2][1];
	out->m[2][2] = m1->m[2][2] + m2->m[2][2];
	out->m[2][3] = m1->m[2][3] + m2->m[2][3];

	out->m[3][0] = m1->m[3][0] + m2->m[3][0];
	out->m[3][1] = m1->m[3][1] + m2->m[3][1];
	out->m[3][2] = m1->m[3][2] + m2->m[3][2];
	out->m[3][3] = m1->m[3][3] + m2->m[3][3];

	return(out);
}

matrix_t *matrix_substract(matrix_t *out, const matrix_t *m1, const matrix_t *m2) {
	debug_assert(out); debug_assert(m1); debug_assert(m2);

	out->m[0][0] = m1->m[0][0] - m2->m[0][0];
	out->m[0][1] = m1->m[0][1] - m2->m[0][1];
	out->m[0][2] = m1->m[0][2] - m2->m[0][2];
	out->m[0][3] = m1->m[0][3] - m2->m[0][3];

	out->m[1][0] = m1->m[1][0] - m2->m[1][0];
	out->m[1][1] = m1->m[1][1] - m2->m[1][1];
	out->m[1][2] = m1->m[1][2] - m2->m[1][2];
	out->m[1][3] = m1->m[1][3] - m2->m[1][3];

	out->m[2][0] = m1->m[2][0] - m2->m[2][0];
	out->m[2][1] = m1->m[2][1] - m2->m[2][1];
	out->m[2][2] = m1->m[2][2] - m2->m[2][2];
	out->m[2][3] = m1->m[2][3] - m2->m[2][3];

	out->m[3][0] = m1->m[3][0] - m2->m[3][0];
	out->m[3][1] = m1->m[3][1] - m2->m[3][1];
	out->m[3][2] = m1->m[3][2] - m2->m[3][2];
	out->m[3][3] = m1->m[3][3] - m2->m[3][3];

	return(out);
}

matrix_t *matrix_multiply(matrix_t *out, const matrix_t *m1, const matrix_t *m2) {
	matrix_t m3, m4;

	debug_assert(out); debug_assert(m1); debug_assert(m2);

	m3 = *m1;
	m4 = *m2;

	out->m[0][0] = m3.m[0][0] * m4.m[0][0] + m3.m[0][1] * m4.m[1][0] + m3.m[0][2] * m4.m[2][0] + m3.m[0][3] * m4.m[3][0];
	out->m[0][1] = m3.m[0][0] * m4.m[0][1] + m3.m[0][1] * m4.m[1][1] + m3.m[0][2] * m4.m[2][1] + m3.m[0][3] * m4.m[3][1];
	out->m[0][2] = m3.m[0][0] * m4.m[0][2] + m3.m[0][1] * m4.m[1][2] + m3.m[0][2] * m4.m[2][2] + m3.m[0][3] * m4.m[3][2];
	out->m[0][3] = m3.m[0][0] * m4.m[0][3] + m3.m[0][1] * m4.m[1][3] + m3.m[0][2] * m4.m[2][3] + m3.m[0][3] * m4.m[3][3];

	out->m[1][0] = m3.m[1][0] * m4.m[0][0] + m3.m[1][1] * m4.m[1][0] + m3.m[1][2] * m4.m[2][0] + m3.m[1][3] * m4.m[3][0];
	out->m[1][1] = m3.m[1][0] * m4.m[0][1] + m3.m[1][1] * m4.m[1][1] + m3.m[1][2] * m4.m[2][1] + m3.m[1][3] * m4.m[3][1];
	out->m[1][2] = m3.m[1][0] * m4.m[0][2] + m3.m[1][1] * m4.m[1][2] + m3.m[1][2] * m4.m[2][2] + m3.m[1][3] * m4.m[3][2];
	out->m[1][3] = m3.m[1][0] * m4.m[0][3] + m3.m[1][1] * m4.m[1][3] + m3.m[1][2] * m4.m[2][3] + m3.m[1][3] * m4.m[3][3];

	out->m[2][0] = m3.m[2][0] * m4.m[0][0] + m3.m[2][1] * m4.m[1][0] + m3.m[2][2] * m4.m[2][0] + m3.m[2][3] * m4.m[3][0];
	out->m[2][1] = m3.m[2][0] * m4.m[0][1] + m3.m[2][1] * m4.m[1][1] + m3.m[2][2] * m4.m[2][1] + m3.m[2][3] * m4.m[3][1];
	out->m[2][2] = m3.m[2][0] * m4.m[0][2] + m3.m[2][1] * m4.m[1][2] + m3.m[2][2] * m4.m[2][2] + m3.m[2][3] * m4.m[3][2];
	out->m[2][3] = m3.m[2][0] * m4.m[0][3] + m3.m[2][1] * m4.m[1][3] + m3.m[2][2] * m4.m[2][3] + m3.m[2][3] * m4.m[3][3];

	out->m[3][0] = m3.m[3][0] * m4.m[0][0] + m3.m[3][1] * m4.m[1][0] + m3.m[3][2] * m4.m[2][0] + m3.m[3][3] * m4.m[3][0];
	out->m[3][1] = m3.m[3][0] * m4.m[0][1] + m3.m[3][1] * m4.m[1][1] + m3.m[3][2] * m4.m[2][1] + m3.m[3][3] * m4.m[3][1];
	out->m[3][2] = m3.m[3][0] * m4.m[0][2] + m3.m[3][1] * m4.m[1][2] + m3.m[3][2] * m4.m[2][2] + m3.m[3][3] * m4.m[3][2];
	out->m[3][3] = m3.m[3][0] * m4.m[0][3] + m3.m[3][1] * m4.m[1][3] + m3.m[3][2] * m4.m[2][3] + m3.m[3][3] * m4.m[3][3];

	return(out);
}

matrix_t *matrix_inverse(matrix_t *out, const matrix_t *m) {
	float inv_det;
	float v0, v1, v2, v3, v4, v5;
	float t00, t10, t20, t30;
	float m00, m01, m02, m03;
	float m10, m11, m12, m13;
	float m20, m21, m22, m23;
	float m30, m31, m32, m33;

	debug_assert(out); debug_assert(m);

	m00 = m->m[0][0], m01 = m->m[0][1], m02 = m->m[0][2], m03 = m->m[0][3];
	m10 = m->m[1][0], m11 = m->m[1][1], m12 = m->m[1][2], m13 = m->m[1][3];
	m20 = m->m[2][0], m21 = m->m[2][1], m22 = m->m[2][2], m23 = m->m[2][3];
	m30 = m->m[3][0], m31 = m->m[3][1], m32 = m->m[3][2], m33 = m->m[3][3];

	v0 = m20 * m31 - m21 * m30;
	v1 = m20 * m32 - m22 * m30;
	v2 = m20 * m33 - m23 * m30;
	v3 = m21 * m32 - m22 * m31;
	v4 = m21 * m33 - m23 * m31;
	v5 = m22 * m33 - m23 * m32;

	t00 = + (v5 * m11 - v4 * m12 + v3 * m13);
	t10 = - (v5 * m10 - v2 * m12 + v1 * m13);
	t20 = + (v4 * m10 - v2 * m11 + v0 * m13);
	t30 = - (v3 * m10 - v1 * m11 + v0 * m12);

	inv_det = 1.0f / (t00 * m00 + t10 * m01 + t20 * m02 + t30 * m03);

	out->m[0][0] = t00 * inv_det;
	out->m[1][0] = t10 * inv_det;
	out->m[2][0] = t20 * inv_det;
	out->m[3][0] = t30 * inv_det;

	out->m[0][1] = - (v5 * m01 - v4 * m02 + v3 * m03) * inv_det;
	out->m[1][1] = + (v5 * m00 - v2 * m02 + v1 * m03) * inv_det;
	out->m[2][1] = - (v4 * m00 - v2 * m01 + v0 * m03) * inv_det;
	out->m[3][1] = + (v3 * m00 - v1 * m01 + v0 * m02) * inv_det;

	v0 = m10 * m31 - m11 * m30;
	v1 = m10 * m32 - m12 * m30;
	v2 = m10 * m33 - m13 * m30;
	v3 = m11 * m32 - m12 * m31;
	v4 = m11 * m33 - m13 * m31;
	v5 = m12 * m33 - m13 * m32;

	out->m[0][2] = + (v5 * m01 - v4 * m02 + v3 * m03) * inv_det;
	out->m[1][2] = - (v5 * m00 - v2 * m02 + v1 * m03) * inv_det;
	out->m[2][2] = + (v4 * m00 - v2 * m01 + v0 * m03) * inv_det;
	out->m[3][2] = - (v3 * m00 - v1 * m01 + v0 * m02) * inv_det;

	v0 = m21 * m10 - m20 * m11;
	v1 = m22 * m10 - m20 * m12;
	v2 = m23 * m10 - m20 * m13;
	v3 = m22 * m11 - m21 * m12;
	v4 = m23 * m11 - m21 * m13;
	v5 = m23 * m12 - m22 * m13;

	out->m[0][3] = - (v5 * m01 - v4 * m02 + v3 * m03) * inv_det;
	out->m[1][3] = + (v5 * m00 - v2 * m02 + v1 * m03) * inv_det;
	out->m[2][3] = - (v4 * m00 - v2 * m01 + v0 * m03) * inv_det;
	out->m[3][3] = + (v3 * m00 - v1 * m01 + v0 * m02) * inv_det;

	return(out);
}

matrix_t *matrix_scalling(matrix_t *out, float sx, float sy, float sz) {
	debug_assert(out);
	out->m11 = sx;	out->m12 = 0;	out->m13 = 0;	out->m14 = 0;
	out->m21 = 0;	out->m22 = sy;	out->m23 = 0;	out->m24 = 0;
	out->m31 = 0;	out->m32 = 0;	out->m33 = sz;	out->m34 = 0;
	out->m41 = 0;	out->m42 = 0;	out->m43 = 0;	out->m44 = 1.0f;
	return(out);
}

matrix_t *matrix_translation(matrix_t *out, float x, float y, float z) {
	debug_assert(out);
	out->m11 = 0;	out->m12 = 0;	out->m13 = 0;	out->m14 = x;
	out->m21 = 0;	out->m22 = 0;	out->m23 = 0;	out->m24 = y;
	out->m31 = 0;	out->m32 = 0;	out->m33 = 0;	out->m34 = z;
	out->m41 = 0;	out->m42 = 0;	out->m43 = 0;	out->m44 = 1.0f;
	return(out);
}

matrix_t *matrix_rotation_axis(matrix_t *out, const vector3_t *v, float angle) {
	float sine, cosine;
	float xsin, ysin, zsin;
	float one_minus_cos;
	float x2, y2, z2;
	float xym, xzm,yzm;

	debug_assert(out); debug_assert(v);

	cosine = cosf(angle);
	sine = sinf(angle);
	one_minus_cos = 1.0f - cosine;
	x2 = v->x * v->x;
	y2 = v->y * v->y;
	z2 = v->z * v->z;
	xym = v->x * v->y * one_minus_cos;
	xzm = v->x * v->z * one_minus_cos;
	yzm = v->y * v->z * one_minus_cos;
	xsin = v->x * sine;
	ysin = v->y * sine;
	zsin = v->z * sine;

	out->m[0][0] = x2 * one_minus_cos + cosine;
	out->m[0][1] = xym - zsin;
	out->m[0][2] = xzm + ysin;
	out->m[1][0] = xym + zsin;
	out->m[1][1] = y2 * one_minus_cos + cosine;
	out->m[1][2] = yzm - xsin;
	out->m[2][0] = xzm - ysin;
	out->m[2][1] = yzm + xsin;
	out->m[2][2] = z2 * one_minus_cos + cosine;
	out->m[0][3] = 0.0f;
	out->m[1][3] = 0.0f;
	out->m[2][3] = 0.0f;
	out->m[3][3] = 1.0f;

	return(out);
}

matrix_t *matrix_rotation_quaternion(matrix_t *out, const quaternion_t *q) {
	float tx, ty, tz;
	float twx, twy, twz;
	float txx, txy, txz;
	float tyy, tyz, tzz;

	debug_assert(out); debug_assert(q);

	tx  = q->x + q->x;
	ty  = q->y + q->y;
	tz  = q->z + q->z;
	twx = tx * q->w;
	twy = ty * q->w;
	twz = tz * q->w;
	txx = tx * q->x;
	txy = ty * q->x;
	txz = tz * q->x;
	tyy = ty * q->y;
	tyz = tz * q->y;
	tzz = tz * q->z;

	out->m[0][0] = 1.0f - (tyy + tzz);
	out->m[0][1] = txy - twz;
	out->m[0][2] = txz + twy;
	out->m[0][3] = 0;
	out->m[1][0] = txy + twz;
	out->m[1][1] = 1.0f - (txx + tzz);
	out->m[1][2] = tyz - twx;
	out->m[1][3] = 0;
	out->m[2][0] = txz - twy;
	out->m[2][1] = tyz + twx;
	out->m[2][2] = 1.0f - (txx + tyy);
	out->m[2][3] = 0;
	out->m[3][0] = 0;
	out->m[3][1] = 0;
	out->m[3][2] = 0;
	out->m[3][3] = 1.0f;

	return(out);
}

matrix_t *matrix_rotation_yaw_pitch_roll(matrix_t *out, float yaw, float pitch, float roll) {
	matrix_t xm, ym, zm, yzm;
	float cosine, sine;

	debug_assert(out);

	cosine = cosf(pitch);
	sine = sinf(pitch);
	xm.m11 = 1.0f;		xm.m12 = 0.0f;		xm.m13 = 0.0f;		xm.m14 = 0.0f;
	xm.m11 = 0.0f;		xm.m12 = cosine;	xm.m13 = -sine;		xm.m14 = 0.0f;
	xm.m11 = 0.0f;		xm.m12 = sine;		xm.m13 = cosine;	xm.m14 = 0.0f;
	xm.m11 = 0.0f;		xm.m12 = 0.0f;		xm.m13 = 0.0f;		xm.m14 = 1.0f;

	cosine = cosf(yaw);
	sine = sinf(yaw);
	ym.m11 = cosine;	ym.m12 = 0.0f;		ym.m13 = sine;		ym.m14 = 0.0f;
	ym.m11 = 0.0f;		ym.m12 = 1.0f;		ym.m13 = 0.0f;		ym.m14 = 0.0f;
	ym.m11 = -sine;		ym.m12 = 0.0f;		ym.m13 = cosine;	ym.m14 = 0.0f;
	ym.m11 = 0.0f;		ym.m12 = 0.0f;		ym.m13 = 0.0f;		ym.m14 = 1.0f;

	cosine = cosf(roll);
	sine = sinf(roll);
	zm.m11 = cosine;	zm.m12 = -sine;		zm.m13 = 0.0f;		zm.m14 = 0.0f;
	zm.m11 = sine;		zm.m12 = cosine;	zm.m13 = 0.0f;		zm.m14 = 0.0f;
	zm.m11 = 0.0f;		zm.m12 = 0.0f;		zm.m13 = 1.0f;		zm.m14 = 0.0f;
	zm.m11 = 0.0f;		zm.m12 = 0.0f;		zm.m13 = 0.0f;		zm.m14 = 1.0f;

	matrix_multiply(&yzm, &ym, &zm);
	matrix_multiply(out, &xm, &yzm);

	return(out);
}

matrix_t *matrix_transformation(matrix_t *out, const vector3_t *translation,
	const quaternion_t *rotation, const vector3_t *scale) {
	quaternion_t r;
	vector3_t t, s;
	matrix_t rot3x3;

	debug_assert(out);

	if (translation) t = *translation; else { t.x = 0; t.y = 0; t.z = 0; }
	if (rotation) r = *rotation; else { r.x = 0; r.y = 0; r.z = 0; r.w = 1.0f; }
	if (scale) s = *scale; else { s.x = 1.0f; s.y = 1.0f; s.z = 1.0f; }

	matrix_rotation_quaternion(&rot3x3, &r);

	// set up final matrix with scale, rotation and translation
	out->m[0][0] = s.x * rot3x3.m[0][0]; out->m[0][1] = s.y * rot3x3.m[0][1]; out->m[0][2] = s.z * rot3x3.m[0][2]; out->m[0][3] = t.x;
	out->m[1][0] = s.x * rot3x3.m[1][0]; out->m[1][1] = s.y * rot3x3.m[1][1]; out->m[1][2] = s.z * rot3x3.m[1][2]; out->m[1][3] = t.y;
	out->m[2][0] = s.x * rot3x3.m[2][0]; out->m[2][1] = s.y * rot3x3.m[2][1]; out->m[2][2] = s.z * rot3x3.m[2][2]; out->m[2][3] = t.z;

	// no projection term
	out->m[3][0] = 0; out->m[3][1] = 0; out->m[3][2] = 0; out->m[3][3] = 1.0f;

	return(out);
}

matrix_t *matrix_look_at(matrix_t *out, const vector3_t *eye,
	const vector3_t *at, const vector3_t *up) {
	float x, y, z;
	vector3_t xaxis, yaxis, zaxis;
	debug_assert(out); debug_assert(eye); debug_assert(at); debug_assert(up);

	vector3_substract(&zaxis, at, eye);
	vector3_normalize(&zaxis, &zaxis);
	vector3_cross(&xaxis, up, &zaxis);
	vector3_normalize(&xaxis, &xaxis);
	vector3_cross(&yaxis, &zaxis, &xaxis);
	vector3_normalize(&yaxis, &yaxis);

	x = -vector3_dot(&xaxis, eye);
	y = -vector3_dot(&yaxis, eye);
	z = -vector3_dot(&zaxis, eye);

	out->m11 = xaxis.x;	out->m12 = yaxis.x;	out->m13 = zaxis.x;	out->m14 = x;
	out->m21 = xaxis.y;	out->m22 = yaxis.y;	out->m23 = zaxis.y;	out->m24 = y;
	out->m31 = xaxis.z;	out->m32 = yaxis.z;	out->m33 = zaxis.z;	out->m34 = z;
	out->m41 = 0.0f;	out->m32 = 0.0f;	out->m33 = 0.0f;	out->m34 = 1.0f;

	return(out);
}

matrix_t *matrix_perspective(matrix_t *out, float w, float h, float zn, float zf) {
	debug_assert(out);
	debug_assert(w);
	debug_assert(h);
	debug_assert(zf != zn);

	out->m11 = 2.0f * zn / w;
	out->m12 = 0.0f;
	out->m13 = 0.0f;
	out->m14 = 0.0f;

	out->m21 = 0.0f;
	out->m22 = 2.0f * zn / h;
	out->m23 = 0.0f;
	out->m24 = 0.0f;

	out->m31 = 0.0f;
	out->m32 = 0.0f;
	out->m33 = zf / (zn - zf);
	out->m34 = zn * zf / (zn - zf);

	out->m41 = 0.0f;
	out->m42 = 0.0f;
	out->m43 = -1.0f;
	out->m44 = 0.0f;

	return(out);
}

matrix_t *matrix_perspective_fov(matrix_t *out, float fov, float aspect, float zn, float zf) {
	float sx, sy, ndepth;

	debug_assert(out);
	debug_assert(fov > 0);
	debug_assert(fov < M_PI * 2);

	sy = 1.0f / tanf(fov * 0.5f);
	sx = sy / aspect;
	ndepth = zn - zf;

	out->m11 = sx;
	out->m12 = 0.0f;
	out->m13 = 0.0f;
	out->m14 = 0.0f;

	out->m21 = 0.0f;
	out->m22 = sy;
	out->m23 = 0.0f;
	out->m24 = 0.0f;

	out->m31 = 0.0f;
	out->m32 = 0.0f;
	out->m33 = zf / ndepth;
	out->m34 = zn * zf / ndepth;

	out->m41 = 0.0f;
	out->m42 = 0.0f;
	out->m43 = -1.0f;
	out->m44 = 0.0f;

	return(out);
}

matrix_t *matrix_ortho(matrix_t *out, float w, float h, float zn, float zf) {
	debug_assert(out);

	debug_assert(out);
	debug_assert(w);
	debug_assert(h);
	debug_assert(zf != zn);

	out->m11 = 2.0f * w;
	out->m12 = 0.0f;
	out->m13 = 0.0f;
	out->m14 = 0.0f;

	out->m21 = 0.0f;
	out->m22 = 2.0f * h;
	out->m23 = 0.0f;
	out->m24 = 0.0f;

	out->m31 = 0.0f;
	out->m32 = 0.0f;
	out->m33 = 1 / (zn - zf);
	out->m34 = zn / (zn - zf);

	out->m41 = 0.0f;
	out->m42 = 0.0f;
	out->m43 = 0.0f;
	out->m44 = 1.0f;

	return(out);
}

/******************************************************************************
 * Quaternion implementation
 *****************************************************************************/

quaternion_t *quaternion_identity(quaternion_t *out) {
	debug_assert(out);
	out->x = 0; out->y = 0; out->z = 0; out->w = 1.0f;
	return(out);
}

int quaternion_is_identity(const quaternion_t *q) {
	debug_assert(q);
	return(q->x == 0 && q->y == 0 && q->z == 0 && q->w == 1.0f);
}

float quaternion_length(const quaternion_t *q) {
	debug_assert(q);
	return(sqrtf(q->x * q->x + q->y * q->y + q->z * q->z));
}

float quaternion_length_square(const quaternion_t *q) {
	debug_assert(q);
	return(q->x * q->x + q->y * q->y + q->z * q->z);
}

void quaternion_to_axis_angle(const quaternion_t *q, vector3_t *axis, float *angle) {
	float len2, inv_len;

	debug_assert(q);
	debug_assert(axis);
	debug_assert(angle);

	len2 = q->x * q->x + q->y * q->y + q->z * q->z;
	if (len2 > 0) {
		*angle = 2.0f * acosf(q->w);
		inv_len = 1.0f / sqrtf(len2);
		axis->x = q->x * inv_len;
		axis->y = q->y * inv_len;
		axis->z = q->z * inv_len;
	} else {
		*angle = 0.0f;
		axis->x = 1.0f;
		axis->y = 0.0f;
		axis->z = 0.0f;
	}
}

void quaternion_to_yaw_pitch_roll(const quaternion_t *q, float *yaw, float *pitch, float *roll) {
	float m[15], L;
	float x, y, z, w;
	float x2, y2, z2, w2;
	float xx, yy, zz, ww;
	float xy_dist;

	int inv;

	debug_assert(q);
	debug_assert(yaw);
	debug_assert(pitch);
	debug_assert(roll);

	x = q->x;
	y = q->y;
	z = q->z;
	w = q->w;

	xx = x * x;
	yy = y * y;
	zz = z * z;
	ww = w * w;

	x2 = x * 2.0f;
	y2 = y * 2.0f;
	z2 = z * 2.0f;
	w2 = w * 2.0f;

	L = xx + yy + zz + ww;

	m[0] = (ww + xx - yy - zz) / L;
	m[1] = (x2 * y + w2 * z) / L;
	m[2] = (x2 * z - w2 * y) / L;
	m[4] = (x2 * y - w2 * z) / L;
	m[5] = (ww + yy - xx - zz) / L;
	m[6] = (y2 * z + w2 * x) / L;
	m[10] = (ww + zz - xx - yy) / L;

	xy_dist = sqrt(m[0] * m[0] + m[1] * m[1]);
	if(xy_dist > 0.00001f) {
		inv = m[10] > 0;
		*yaw = (inv) ? atan2(m[1], m[0]) : -atan2(m[1], -m[0]);
		*pitch = (inv) ? atan2(-m[2], xy_dist) : -atan2(m[2], -xy_dist);
		*roll = (inv) ? atan2(m[6], m[10]) : -atan2(m[6], -m[10]);
	}
	else {
		*yaw = atan2(-m[4], m[5]);
		*pitch = atan2(-m[2], xy_dist);
		*roll = 0.0f;
	}
}

quaternion_t *quaternion_rotation_matrix(quaternion_t *q, const matrix_t *m) {
	float trace;
	float root;
	float* quat[3] = { &q->x, &q->y, &q->z };

	static size_t next[3] = { 1, 2, 0 };
	size_t i = 0, j, k;

	debug_assert(q);
	debug_assert(m);

	trace = m->m11 +m->m22 + m->m33;
	if (trace > 0.0f) {
		// |w| > 1/2, may as well choose w > 1/2
		root = sqrtf(trace + 1.0f);  // 2w
		q->w = 0.5f * root;
		root = 0.5f / root;  // 1/(4w)
		q->x = (m->m32 - m->m23) * root;
		q->y = (m->m13 - m->m31) * root;
		q->z = (m->m21 - m->m12) * root;
	} else {
		// |w| <= 1/2
		if (m->m22 > m->m11)
		    i = 1;
		if (m->m33 > m->m[i][i] )
		    i = 2;
		j = next[i];
		k = next[j];

		root = sqrtf(m->m[i][i] - m->m[j][j] - m->m[k][k] + 1.0f);
		*quat[i] = 0.5f * root;
		root = 0.5f / root;
		q->w = (m->m[k][j] - m->m[j][k]) * root;
		*quat[j] = (m->m[j][i] + m->m[i][j]) * root;
		*quat[k] = (m->m[k][i] + m->m[i][k]) * root;
	}

	return(q);
}

quaternion_t *quaternion_rotation_axis(quaternion_t *q, const vector3_t *axis, float angle) {
	float a2, sine;

	debug_assert(q);
	debug_assert(axis);

	a2 =  0.5f * angle;
	sine = sinf(a2);
	q->w = cosf(a2);
	q->x = sine * axis->x;
	q->y = sine * axis->y;
	q->z = sine * axis->z;

	return(q);
}

quaternion_t *quaternion_multiply(quaternion_t *out, const quaternion_t *q1, const quaternion_t *q2) {
	quaternion_t qt1, qt2;

	debug_assert(out);
	debug_assert(q1);
	debug_assert(q2);

	qt1 = *q1; qt2 = *q2;
	out->w = qt1.w * qt2.w - qt1.x * qt2.x - qt1.y * qt2.y - qt1.z * qt2.z;
	out->x = qt1.w * qt2.x + qt1.x * qt2.w + qt1.y * qt2.z - qt1.z * qt2.y;
	out->y = qt1.w * qt2.y + qt1.y * qt2.w + qt1.z * qt2.x - qt1.x * qt2.z;
	out->z = qt1.w * qt2.z + qt1.z * qt2.w + qt1.x * qt2.y - qt1.y * qt2.x;

	return(out);
}

quaternion_t *quaternion_rotation_yaw_pitch_roll(quaternion_t *q, float yaw, float pitch, float roll) {
	vector3_t axis;
	quaternion_t qx, qy, qz, qyz;

	debug_assert(q);

	axis.x = 0; axis.y = 1.0f; axis.z = 0;
	quaternion_rotation_axis(&qy, &axis, yaw);

	axis.x = 1.0f; axis.y = 0; axis.z = 0;
	quaternion_rotation_axis(&qx, &axis, pitch);

	axis.x = 0; axis.y = 0; axis.z = 1.0f;
	quaternion_rotation_axis(&qz, &axis, roll);

	quaternion_multiply(&qyz, &qy, &qz);
	quaternion_multiply(q, &qx, &qyz);

	return(q);
}

quaternion_t *quaternion_normalize(quaternion_t *out, const quaternion_t *q) {
	float len, factor;

	debug_assert(out);
	debug_assert(q);

	len = sqrtf(q->w * q->w + q->x * q->x + q->y * q->y + q->z * q->z);
	debug_assert(len);

	factor = 1.0f / len;
	out->w = q->w * factor;
	out->x = q->x * factor;
	out->y = q->y * factor;
	out->z = q->z * factor;

	return(out);
}

quaternion_t *quaternion_inverse(quaternion_t *out, const quaternion_t *q) {
	float norm, inv;

	debug_assert(out);
	debug_assert(q);

	norm = q->w * q->w + q->x * q->x + q->y * q->y + q->z * q->z;
	if (norm > 0.0f) {
	    inv = 1.0f / norm;
		out->w = q->w * inv;
		out->x = -q->x * inv;
		out->y = -q->y * inv;
		out->z = -q->z * inv;
	} else {
	    out->w = 0;
		out->x = 0;
		out->y = 0;
		out->z = 0;
	    return(0);
	}

	return(out);
}

/******************************************************************************
 * Plane implementation
 *****************************************************************************/

float plane_dot(const plane_t *p, const vector4_t *v) {
	debug_assert(p);
	debug_assert(v);
	return(
		p->a * v->x
		+ p->b * v->y
		+ p->c * v->z
		+ p->d * v->w
		);
}

float plane_dot_coord(const plane_t *p, vector3_t *v) {
	debug_assert(p);
	debug_assert(v);
	return(
		p->a * v->x
		+ p->b * v->y
		+ p->c * v->z
		+ p->d
		);
}

float plane_dot_normal(const plane_t *p, vector3_t *v) {
	debug_assert(p);
	debug_assert(v);
	return(
		p->a * v->x
		+ p->b * v->y
		+ p->c * v->z
		);
}

plane_t *plane_normalize(plane_t *out, const plane_t *p) {
	float length;

	debug_assert(out);
	debug_assert(p);

	length = sqrtf(p->a * p->a + p->b * p->b + p->c * p->c);
	if (length) {
		out->a /= length;
		out->b /= length;
		out->c /= length;
		out->d /= length;
	} else {
		out->a = 0;
		out->b = 0;
		out->c = 0;
		out->d = 0;
	}

	return(out);
}

plane_t *plane_from_point_normal(plane_t *out, const vector3_t *p, const vector3_t *n) {
	debug_assert(out);
	debug_assert(p);
	debug_assert(n);

	out->a = n->x;
	out->b = n->x;
	out->c = n->x;
	out->d = -vector3_dot(n, p);

	return(out);
}

vector3_t *plane_intersect_line(vector3_t *out, const plane_t *p, const vector3_t *p1, const vector3_t *p2) {
	vector3_t direction, normal;
	float dot, temp;

	debug_assert(out);
	debug_assert(p1);
	debug_assert(p2);

	normal.x = p->a;
	normal.y = p->b;
	normal.z = p->c;

	direction.x = p2->x - p1->x;
	direction.y = p2->y - p1->y;
	direction.z = p2->z - p1->z;

	dot = vector3_dot(&normal, &direction);
	if (!dot)
		return NULL;

	temp = (p->d + vector3_dot(&normal, p1)) / dot;

	out->x = p1->x - temp * direction.x;
	out->y = p1->y - temp * direction.y;
	out->z = p1->z - temp * direction.z;

	return out;
}

plane_t *plane_from_points(plane_t *out, const vector3_t *p1, const vector3_t *p2, const vector3_t *p3) {
	vector3_t edge1, edge2, normal, nnormal;

	edge1.x = 0.0f; edge1.y = 0.0f; edge1.z = 0.0f;
	edge2.x = 0.0f; edge2.y = 0.0f; edge2.z = 0.0f;

	vector3_substract(&edge1, p2, p1);
	vector3_substract(&edge2, p3, p1);

	vector3_cross(&normal, &edge1, &edge2);
	vector3_normalize(&nnormal, &normal);
	plane_from_point_normal(out, p1, &nnormal);

	return(out);
}

plane_t *plane_transform(plane_t *out, const plane_t *p, const matrix_t *m) {
	debug_assert(out);
	debug_assert(p);
	debug_assert(m);

	out->a = m->m[0][0] * p->a + m->m[0][1] * p->b + m->m[0][2] * p->c + m->m[0][3] * p->d;
	out->b = m->m[1][0] * p->a + m->m[1][1] * p->b + m->m[1][2] * p->c + m->m[1][3] * p->d;
	out->c = m->m[2][0] * p->a + m->m[2][1] * p->b + m->m[2][2] * p->c + m->m[2][3] * p->d;
	out->d = m->m[3][0] * p->a + m->m[3][1] * p->b + m->m[3][2] * p->c + m->m[3][3] * p->d;

	return(out);
}
