/******************************************************************************
 *	File:			render.c
 *
 *	Created by:		Ivan Shishkin <codingdude@gmail.com>
 *
 *	History:		Created 25/11/12
 *
 *	Copyright (c) 2012 All rights reserved
 *
 *****************************************************************************/

#include "platform.h"
#include "types.h"
#include "debug.h"
#include "dslib.h"
#include "maths.h"
#include "render.h"

#if defined(_WIN32)

#include <Windows.h>

#include <gl/GL.h>
#include <glext.h>

#elif defined(ANDROID)

#include <EGL/egl.h>
#include <GLES/gl.h>
#include <GLES/glext.h>

#else

#include <OpenGLES/ES1/gl.h>
#include <OpenGLES/ES1/glext.h>

#endif

#if defined(GL_VERSION_ES_CM_1_0) \
	|| defined(GL_VERSION_ES_CL_1_0) \
	|| defined(GL_VERSION_ES_CM_1_1) \
	|| defined(GL_VERSION_ES_CL_1_1)

#define GL_VERSION_OPENGL_ES

#define GL_BYTE                           0x1400
#define GL_UNSIGNED_BYTE                  0x1401
#define GL_SHORT                          0x1402
#define GL_UNSIGNED_SHORT                 0x1403
#define GL_INT                            0x1404
#define GL_UNSIGNED_INT                   0x1405
#define GL_FLOAT                          0x1406
#define GL_2_BYTES                        0x1407
#define GL_3_BYTES                        0x1408
#define GL_4_BYTES                        0x1409
#define GL_DOUBLE                         0x140A

#define GL_COMPRESSED_RGB_S3TC_DXT1_EXT   0x83F0
#define GL_COMPRESSED_RGBA_S3TC_DXT1_EXT  0x83F1
#define GL_COMPRESSED_RGBA_S3TC_DXT3_EXT  0x83F2
#define GL_COMPRESSED_RGBA_S3TC_DXT5_EXT  0x83F3

#define GL_TEXTURE_CUBE_MAP               0x8513
#define GL_TEXTURE_2D_ARRAY               0x8C1A
#define GL_TEXTURE_3D                     0x806F
#define GL_TEXTURE_MAX_LEVEL              0x813D

#else

static PFNGLBINDBUFFERPROC				glBindBuffer				= NULL;
static PFNGLDELETEBUFFERSPROC			glDeleteBuffers				= NULL;
static PFNGLGENBUFFERSPROC				glGenBuffers				= NULL;
static PFNGLBUFFERDATAPROC				glBufferData				= NULL;
static PFNGLTEXIMAGE3DPROC				glTexImage3D				= NULL;
static PFNGLCOMPRESSEDTEXIMAGE2DPROC	glCompressedTexImage2D		= NULL;
static PFNGLCOMPRESSEDTEXIMAGE3DPROC	glCompressedTexImage3D		= NULL;

#endif

static void *g_hwnd	= NULL;
static void *g_hdc	= NULL;
static void *g_hrc	= NULL;

static tree_t *g_texmaps = NULL;
static tree_t *g_vbuffers = NULL;
static tree_t *g_ibuffers = NULL;

static matrix_t	g_transforms[MATRIX_MAX_TYPES];
static int		g_transforms_changed[MATRIX_MAX_TYPES];

static int		current_texture = 0;

typedef struct {
	int		format;
	int		width;
	int		height;
	int		bits;
} compressed_block_info_t;

static compressed_block_info_t compressed_block_info[] = {
	// | pixel format	|	width	|	height	|	bits	|
	{ PVRTC_2BPP_RGB,			8,			4,		64		},
	{ PVRTC_2BPP_RGBA,			8,			4,		64		},
	{ PVRTC_4BPP_RGB,			4,			4,		64		},
	{ PVRTC_4BPP_RGBA,			4,			4,		64		},
	{ PVRTCII_2BPP,				8,			4,		64		},
	{ PVRTCII_4BPP,				4,			4,		64		},
	{ ETC1,						4,			4,		64		},
	{ ETC2_RGB,					4,			4,		64		},
	{ ETC2_RGBA,				4,			4,		128		},
	{ ETC2_RGBA1,				4,			4,		64		},
	{ EACR11U,					4,			4,		64		},
	{ EACR11S,					4,			4,		64		},
	{ EACRG11U,					4,			4,		128		},
	{ EACRG11S,					4,			4,		128		},
	{ DXT1,						4,			4,		64		},
	{ DXT3,						4,			4,		128		},
	{ DXT5,						4,			4,		128		},
	{ RGBA8888,					1,			1,		32		},
	{ RGBA4444,					1,			1,		16		},
	{ RGBA5551,					1,			1,		16		},
	{ BGRA8888,					1,			1,		32		},
	{ RGB888,					1,			1,		24		},
	{ RGB565,					1,			1,		16		},
	{ ALPHA,					1,			1,		8		}
};

static const compressed_block_info_t *get_compressed_blockInfo(pixel_format_t fmt) {
	for (int i = 0; i < sizeof(compressed_block_info) / sizeof(compressed_block_info_t); i++) {
		compressed_block_info_t *info = &compressed_block_info[i];
		if (info->format == fmt) {
			return info;
		}
	}
	return NULL;
}

static int int_compare_fn(const int i1, const int i2) {
	if (i1 < i2)
		return(-1);
	if (i1 > i2)
		return(1);
	return(0);
}

static int render_create_context() {
#if defined (_WIN32)

	int pf;
	PIXELFORMATDESCRIPTOR pfd;

	memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize			= sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion		= 1;
	pfd.dwFlags			= PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType		= PFD_TYPE_RGBA;
	pfd.cColorBits		= 32;
	pfd.cDepthBits		= 32;
	pfd.iLayerType		= PFD_MAIN_PLANE;

	if (!IsWindow((HWND)g_hwnd))
		goto fail;

	g_hdc = GetDC((HWND)g_hwnd);
	if (!g_hdc)
		goto fail;

	pf = ChoosePixelFormat((HDC)g_hdc, &pfd);
	if (!pf)
		goto fail;

	if (!SetPixelFormat((HDC)g_hdc, pf, &pfd))
		goto fail;

	g_hrc = wglCreateContext((HDC)g_hdc);
	if (!g_hrc)
		goto fail;

	if (!wglMakeCurrent((HDC)g_hdc, (HGLRC)g_hrc))
		goto fail;

#endif

	return(1);

fail:
	debug_error("rendergl : unable to create device context");
}

static void render_destroy_context() {
#if defined(_WIN32)

	wglDeleteContext((HGLRC)g_hrc);

#endif // _WIN32
}

static int render_is_extension_supported(const char *ext) {
	const unsigned char *extensions;
	const unsigned char *start;
	unsigned char *where, *term;

	extensions = glGetString(GL_EXTENSIONS);
	start = extensions;

	where = (unsigned char *) strchr(ext, ' ');
	if (where || *ext == '\0')
		return(0);


	for (;;) {
		where = (unsigned char*) strstr((const char*) start, ext);
		if (!where)
			break;
		term = where + strlen(ext);
		if (where == start || *(where - 1) == ' ')
			if(*term == ' ' || *term == '\0')
				return(1);
		start = term;
	}

	return(0);
}

static int render_init_extensions() {
#if defined(_WIN32)

	glBindBuffer = (PFNGLBINDBUFFERPROC) wglGetProcAddress("glBindBuffer");
	debug_assert(glBindBuffer);

	glDeleteBuffers = (PFNGLDELETEBUFFERSPROC) wglGetProcAddress("glDeleteBuffers");
	debug_assert(glDeleteBuffers);

	glGenBuffers = (PFNGLGENBUFFERSPROC) wglGetProcAddress("glGenBuffers");
	debug_assert(glGenBuffers);

	glBufferData = (PFNGLBUFFERDATAPROC) wglGetProcAddress("glBufferData");
	debug_assert(glBufferData);

	glTexImage3D = (PFNGLTEXIMAGE3DPROC) wglGetProcAddress("glTexImage3D");
	debug_assert(glTexImage3D);

	glCompressedTexImage2D = (PFNGLCOMPRESSEDTEXIMAGE2DPROC) wglGetProcAddress("glCompressedTexImage2D");
	debug_assert(glCompressedTexImage2D);

	glCompressedTexImage3D = (PFNGLCOMPRESSEDTEXIMAGE3DPROC) wglGetProcAddress("glCompressedTexImage3D");
	debug_assert(glCompressedTexImage3D);

#endif // _WIN32

	return(1);

fail:
	debug_error("rendergl : unable to initialize opengl extensions, "
		"some of them is not supported by the current platform");

	return(0);
}

static void render_commit_transforms() {
	matrix_t tm, modelview;

	if (g_transforms_changed[MATRIX_MODEL]
		|| g_transforms_changed[MATRIX_VIEW]) {
			matrix_multiply(
				&modelview,
				&g_transforms[MATRIX_VIEW],
				&g_transforms[MATRIX_MODEL]
			);
			matrix_transpose(&tm, &modelview);
			glMatrixMode(GL_MODELVIEW);
			glLoadMatrixf((float*)&tm);
			g_transforms_changed[MATRIX_MODEL] = 0;
			g_transforms_changed[MATRIX_VIEW] = 0;
		}
		if (g_transforms_changed[MATRIX_PROJECTION]) {
			glMatrixMode(GL_PROJECTION);
			matrix_transpose(&tm, &g_transforms[MATRIX_PROJECTION]);
			glLoadMatrixf((float*)&tm);
			g_transforms_changed[MATRIX_PROJECTION] = 0;
		}
}

static int render_to_internal_format(int format) {
	switch (format) {
#if defined(GL_VERSION_OPENGL_ES)

	case PVRTC_2BPP_RGB:
		return(GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG);
	case PVRTC_2BPP_RGBA:
		return(GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG);
	case PVRTC_4BPP_RGB:
		return(GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG);
	case PVRTC_4BPP_RGBA:
		return(GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG);
#if defined(ANDROID)
	case ETC1:
		return(GL_ETC1_RGB8_OES);
#endif

#endif // OpenGL ES

	case DXT1:
		return(GL_COMPRESSED_RGBA_S3TC_DXT1_EXT);
	case DXT3:
		return(GL_COMPRESSED_RGBA_S3TC_DXT3_EXT);
	case DXT5:
		return(GL_COMPRESSED_RGBA_S3TC_DXT5_EXT);
	case RGBA8888:
		return(GL_RGBA);
	default:
		return(GL_INVALID_ENUM);
	}
}

static int render_is_compressed_image(int format) {
	return(!(format > 18 && format < 22));
}

static int render_image_size(int format, int depth, int width, int height) {
	const compressed_block_info_t *info = get_compressed_block_info(format);
	if (!info) {
		return 0;
	}

	return ceil(width / (float)info->width)
		* ceil(height / (float)info->height)
		* depth * info->bits / 8;
}

static int render_get_target_texture(int flags)
{
	int target;

	target = 0;
	if (flags & TEXMAP_CUBEMAP)
		target = GL_TEXTURE_CUBE_MAP;
	else if (flags & TEXMAP_2D)
		target = GL_TEXTURE_2D;
	else if (flags & TEXMAP_2D_ARRAY)
		target = GL_TEXTURE_2D_ARRAY;
	else if (flags & TEXMAP_3D)
		target = GL_TEXTURE_3D;

	return target;
}

static int get_pixel_type(int channel)
{
	int type = 0;
	switch (channel) {
	case CHANNEL_UBYTE:
		return(GL_UNSIGNED_BYTE);
	case CHANNEL_SBYTE:
		return(GL_BYTE);
	case CHANNEL_USHORT:
		return(GL_UNSIGNED_SHORT);
	case CHANNEL_SSHORT:
		return(GL_SHORT);
	case CHANNEL_UINT:
		return(GL_UNSIGNED_INT);
	case CHANNEL_SINT:
		return(GL_INT);
	case CHANNEL_FLOAT:
		return(GL_FLOAT);
	default:
		debug_error("rendergl : unknown color channel type");
	}
}

void render_init(void *hwnd)
{
	g_hwnd = hwnd;
	if (!render_create_context())
		return;
	if (!render_init_extensions())
		return;

	g_texmaps = tree_create((compare_func) int_compare_fn);
	g_vbuffers = tree_create((compare_func) int_compare_fn);
	g_ibuffers = tree_create((compare_func) int_compare_fn);

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	debug_message("render system successfully initialized");
}

void render_shutdown() {
	tree_destroy(g_texmaps, (release_func) render_destroy_texmap);
	tree_destroy(g_vbuffers, (release_func) render_destroy_vertex_buffer);
	tree_destroy(g_ibuffers, (release_func) render_destroy_index_buffer);

	render_destroy_context();

	g_hrc = NULL;
	g_hdc = NULL;
	g_hwnd = NULL;

	g_texmaps = NULL;
	g_vbuffers = NULL;
	g_ibuffers = NULL;

	debug_message("render system successfully shutted down");
}

void render_resize(int w, int h) {
	glViewport(0, 0, w, h);
}

void render_set_clear_color(const vector4_t *color) {
	debug_assert(color);
	glClearColor(color->x, color->y, color->z, color->w);
}

void render_swap_buffers() {
#if defined (_WIN32)
	SwapBuffers((HDC)g_hdc);
#endif

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	render_commit_transforms();
}

void render_bind_matrix(int type, const matrix_t *m) {
	debug_assert(m);
	debug_assert(type >= 0 && type < MATRIX_MAX_TYPES);

	switch (type) {
	case MATRIX_MODEL:
		g_transforms[MATRIX_MODEL] = *m;
		g_transforms_changed[MATRIX_MODEL] = 1;
		break;

	case MATRIX_VIEW:
		g_transforms[MATRIX_VIEW] = *m;
		g_transforms_changed[MATRIX_VIEW] = 1;
		break;

	case MATRIX_PROJECTION:
		g_transforms[MATRIX_PROJECTION] = *m;
		g_transforms_changed[MATRIX_PROJECTION] = 1;
		break;

	default:
		debug_warning("rendergl_bind_matrix : invalid parameter");
	};
}

int render_create_vertex_buffer(int vsize, int vcount, const void *vdata) {
	unsigned buffer;

	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBufferData(GL_ARRAY_BUFFER, vsize * vcount, vdata, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	tree_insert(g_vbuffers, (void*)buffer);

	return(buffer);
}

int render_create_index_buffer(int count, const void *data) {
	unsigned buffer;

	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(short), data, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	tree_insert(g_ibuffers, (void*)buffer);

	return(buffer);
}

void render_destroy_vertex_buffer(int vb) {
	node_t *node;

	node = tree_find(g_vbuffers, (void*)vb);
	if (node) {
		glDeleteBuffers(1, (unsigned*) &vb);
		tree_erase(g_vbuffers, node, NULL);
	}
}

void render_destroy_index_buffer(int ib) {
	node_t *node;

	node = tree_find(g_ibuffers, (void*)ib);
	if (node) {
		glDeleteBuffers(1, (unsigned*) &ib);
		tree_erase(g_ibuffers, node, NULL);
	}
}

int render_create_texmap_2d(int w, int h, int array_size,
	int mipmaps, int channel, int format, int flags, const void *data) {
		int i, compressed, size, iformat, type, min_filter, mag_filter;
		unsigned texmap, target;

		iformat = render_to_internal_format(format);
		if (iformat == GL_INVALID_ENUM) {
			debug_warning("rendergl : image has an unsupported pixel format");
			return(0);
		}

		compressed = render_is_compressed_image(format);
		size = render_image_size(format, array_size, w, h);
		target = render_get_target_texture(flags);
		type = get_pixel_type(channel);

		debug_assert(iformat != GL_TEXTURE_3D);

		glGenTextures(1, &texmap);
		glBindTexture(GL_TEXTURE_2D, texmap);

		if (mipmaps > 1) {
			glTexParameteri(target, GL_TEXTURE_MAX_LEVEL, mipmaps-1);
			glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		} else {
			glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		}

		glTexParameteri(target, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(target, GL_TEXTURE_WRAP_T, GL_REPEAT);

		for (i = 0; i < mipmaps; i++) {
			if (compressed) {
				if (array_size > 1)
#					if defined(GL_VERSION_OPENGL_ES)
						debug_error("rendergl : texture arrays is not supported "
							"by the current version of opengl es");
#					else
						glCompressedTexImage3D(target, i, iformat, w, h ,array_size, 0, size, data);
#					endif
				else
					glCompressedTexImage2D(target, i, iformat, w, h, 0, size, data);
			} else {
				if (array_size > 1)
#					if defined(GL_VERSION_OPENGL_ES)
						debug_error("rendergl : texture arrays is not supported "
							"by the current version of opengl es");
#					else
						glTexImage3D(target, i, iformat, w, h , array_size, 0, iformat, type, data);
#					endif
				else
					glTexImage2D(target, i, iformat, w, h, 0, iformat, type, data);
			}

			w = max(1, (w >> 1));
			h = max(1, (h >> 1));
			data = (void*)((int)data + size);
			size = render_image_size(array_size, format, w, h);
		}

		glBindTexture(target, 0);

		return(texmap);
}

int render_create_texmap_3d(int w, int h, int d,
	int mipmaps, int channel, int format, int flags, const void *data) {
		debug_assert(!"rendergl_create_texmap_3d : not implemented");
}

void render_destroy_texmap(int tmap) {
	node_t *node;

	node = tree_find(g_texmaps, (void*)tmap);
	if (node) {
		glDeleteTextures(1, (unsigned*) &tmap);
		tree_erase(g_ibuffers, node, NULL);
	}
}

void render_bind_texmap(int id, int flags) {
	if (current_texture != id) {
		glBindTexture(render_get_target_texture(flags), id);
		current_texture = id;
	}
}

void render_draw(int vb, int vdecl, int ib, int count) {
	render_commit_transforms();

	if (vdecl == VE_POINT | VE_NORMAL | VE_TEXCOORDS1) {
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_NORMAL_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glBindBuffer(GL_ARRAY_BUFFER, vb);
		glVertexPointer(3, GL_FLOAT, 32, (void*) NULL);
		glNormalPointer(GL_FLOAT, 32, (void*) 12);
		glTexCoordPointer(2, GL_FLOAT, 32, (void*) 24);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ib);
		glDrawElements(GL_TRIANGLES, count * 3, GL_UNSIGNED_SHORT, NULL);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_NORMAL_ARRAY);
		glDisableClientState(GL_VERTEX_ARRAY);
	} else
		debug_error("rendergl : unsupported vertex format");
}
