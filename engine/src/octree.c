/******************************************************************************
 *	File:			octree.c
 *
 *	Created by:		Ivan Shishkin <codingdude@gmail.com>
 *
 *	History:		Created 25/03/13
 *
 *	Copyright (c) 2013 All rights reserved
 *
 *****************************************************************************/

#include <platform.h>
#include <types.h>
#include <debug.h>
#include <dslib.h>
#include <collision.h>
#include <maths.h>
#include <scene.h>
#include <octree.h>

static int node_compare_fn(const int n1, const int n2) {
	if (n1 < n2)
		return(-1);
	else if (n1 > n2)
		return(1);
	return(0);
}

static int octree_is_twice_size(const octree_t *oct, const scene_node_t *node) {
	vector3_t r, d, c;
	bbox_t bbox;

	debug_assert(oct);
	debug_assert(node);

	if (bbox_is_infinite(&node->bbox))
		return(0);

	bbox_get_size(&d, &bbox);
	bbox_get_half_size(&r, &oct->bbox);

	return((d.x <= r.x) && (d.y <= r.y) && (d.z <= r.z));
}

static octree_t *octree_get_octant(const octree_t *oct, const scene_node_t *node, int *x, int *y, int *z) {
	vector3_t c, nc;
	int ix, iy, iz;

	debug_assert(oct);
	debug_assert(x && y && z);

	bbox_get_center(&c, &oct->bbox);
	bbox_get_center(&nc, &node->bbox);

	ix = (nc.x > c.x) ? 1 : 0;
	iy = (nc.y > c.y) ? 1 : 0;
	iz = (nc.z > c.z) ? 1 : 0;

	if (x) *x = ix;
	if (y) *y = iy;
	if (z) *z = iz;

	return(oct->octants[ix][iy][iz]);
}

octree_t *octree_init(octree_t *oct, const bbox_t *bbox, int depth) {
	int i, j, k;

	debug_assert(oct);
	debug_assert(bbox);
	
	oct->parent = NULL;

	for (i = 0; i < 2; i++) {
		for (j = 0; j < 2; j++) {
			for (k = 0; k < 2; k++)
				oct->octants[i][j][k] = NULL;
		}
	}

	oct->bbox = *bbox;
	oct->depth = depth;

	oct->nodes = NULL;

	return(oct);
}

void octree_destroy(octree_t *oct) {
	int i, j, k;

	if (!oct)
		return;

	oct->parent = NULL;

	for (i = 0; i < 2; i++) {
		for (j = 0; j < 2; j++) {
			for (k = 0; k < 2; k++) {
				octree_destroy(oct->octants[i][j][k]);
				free(oct->octants[i][j][k]);
				oct->octants[i][j][k] = NULL;
			}
		}
	}

	list_destroy(oct->nodes);
	oct->nodes = NULL;
}

void octree_add_node(octree_t *oct, scene_node_t *node, int depth) {
	vector3_t _min, _max;
	octree_t *octant;
	int x, y, z;

	if (depth < oct->depth && octree_is_twice_size(oct, node)) {
		octant = octree_get_octant(oct, node, &x, &y, &z);
		if (!octant) {
			octant = oct->octants[x][y][z] = (octree_t*) malloc(sizeof(octree_t));
			octree_init(octant, &node->bbox, depth);
			octant->parent = oct;

			if (x) {
				_min.x = (oct->bbox.minimum.x + oct->bbox.maximum.x) * 0.5f;
				_max.x = oct->bbox.maximum.x;
			} else {
				_min.x = oct->bbox.minimum.x;
				_max.x = (oct->bbox.minimum.x + oct->bbox.maximum.x) * 0.5f;
			}
			if (y) {
				_min.y = (oct->bbox.minimum.y + oct->bbox.maximum.y) * 0.5f;
				_max.y = oct->bbox.maximum.y;
			} else {
				_min.y = oct->bbox.minimum.y;
				_max.y = (oct->bbox.minimum.y + oct->bbox.maximum.y) * 0.5f;
			}
			if (z) {
				_min.z = (oct->bbox.minimum.z + oct->bbox.maximum.z) * 0.5f;
				_max.z = oct->bbox.maximum.z;
			} else {
				_min.z = oct->bbox.minimum.z;
				_max.z = (oct->bbox.minimum.z + oct->bbox.maximum.z) * 0.5f;
			}

			bbox_set_extents(&octant->bbox, &_min, &_max);
		}
		octree_add_node(octant, node, depth++);
	} else {
		oct->nodes = list_prepend(oct->nodes, node);
		node->octant = oct;
	}
}

void octree_remove_node(scene_node_t *node) {
	list_t *ls;

	debug_assert(node);

	if (node->octant) {
		ls = list_find(node->octant->nodes, node, (compare_func) node_compare_fn);
		if (ls) {
			if (ls == node->octant->nodes)
				node->octant->nodes = list_first(list_remove(ls));
			else
				list_remove(ls);
		}
	}

	node->octant = NULL;
}

void octree_update_node(octree_t *oct, scene_node_t *node) {
	debug_assert(oct);
	debug_assert(node);

	if (!node->octant) {
		if (!scene_node_is_in(node, &oct->bbox)) {
			oct->nodes = list_prepend(oct->nodes, node);
			node->octant = oct;
		} else 
			octree_add_node(oct, node, 0);
	}

	if (!scene_node_is_in(node, &node->octant->bbox)) {
		octree_remove_node(node);

		if (!scene_node_is_in(node, &oct->bbox)) {
			oct->nodes = list_prepend(oct->nodes, node);
			node->octant = oct;
		} else 
			octree_add_node(oct, node, 0);
	}
}

void octree_walk_tree(const octree_t *oct, camera_t *cam, array_t *queue) {
	scene_node_t *node;
	list_t *next;
	octree_t *child;
	int i, j, k;
	bbox_t clip;
	vector3_t hs;

	debug_assert(oct);
	debug_assert(cam);
	debug_assert(queue);

	bbox_get_half_size(&hs, &oct->bbox);
	vector3_add(&clip.maximum, &oct->bbox.maximum, &hs);
	vector3_substract(&clip.minimum, &oct->bbox.minimum, &hs);
	clip.infinite = 0;

	if (frustum_is_visible_box(&cam->frustum, &clip)) {
		next = oct->nodes;
		while (next) {
			node = (scene_node_t*) list_data(next);
			next = list_next(next);

			if (frustum_is_visible_box(&cam->frustum, &node->bbox))
				array_append(queue, &node);
		}
	}

	for (i = 0; i < 2; i++) {
		for (j = 0; j < 2; j++) {
			for (k = 0; k < 2; k++) {
				if ((child = oct->octants[i][j][k]) != NULL)
					octree_walk_tree(child, cam, queue);
			}
		}
	}
}