/******************************************************************************
 *	File:			dslib.c
 *
 *	Created by:		Ivan Shishkin <codingdude@gmail.com>
 *
 *	History:		Created 04/01/13
 *
 *	Copyright (c) 2013 All rights reserved
 *
 *****************************************************************************/

#include "platform.h"
#include "types.h"
#include "debug.h"
#include "dslib.h"

/******************************************************************************
*	Array implementation
******************************************************************************/

array_t *array_create(int stride) {
	int size;
	array_t *arr;

	if (stride <= 0)
		return(NULL);

	size = sizeof(array_t);
	arr = (array_t*) malloc(size);
	if (!arr)
		return(NULL);

	memset(arr, 0, size);
	arr->stride = stride;

	return(arr);
}

array_t *array_create_from_existing(const void *first, const void *last, int stride) {
	array_t *arr;

	if(!first || !last || first > last)
		return(NULL);

	arr = array_create(stride);
	if (!arr)
		return(NULL);

	array_append_range(arr, first, last);

	return(arr);
}

void array_destroy(array_t *arr) {
	array_clear(arr);
	free(arr);
}

void array_clear(array_t *arr) {
	if (!arr)
		return;

	free(arr->data);

	arr->size = 0;
	arr->reserved = 0;
	arr->data = 0;
}

void array_reserve(array_t *arr, int count) {
	void *data;
	int size, reserved;

	if (!arr)
		return;

	size = arr->stride * count;

	reserved = 1;
	while (reserved < size && reserved > 0)
		reserved <<= 1;

	if (size > arr->reserved) {
		data = realloc(arr->data, reserved);
		if (data == NULL)
			debug_assert(!"not enough memory");
		arr->data = data;
		arr->reserved = reserved;
	}
}

void array_resize(array_t *arr, int count) {
	array_reserve(arr, count);
	arr->size = count;
}

void array_append(array_t *arr, const void *elem) {
	if (arr);
		array_insert(arr, arr->size, elem);
}

void array_append_range(array_t *arr, const void *first, const void *last) {
	if (arr);
		array_insert_range(arr, arr->size, first, last);
}

void array_insert(array_t *arr, int pos, const void *item) {
	array_insert_range(arr, pos, item, item);
}

void array_insert_range(array_t *arr, int pos, const void *first, const void *last) {
	int size, length, count;
	void *begin, *end;

	if (!arr || !first || !last || pos < 0 || first > last)
		return;

	size = (int) last - (int) first + arr->stride;
	count = size / arr->stride + max(arr->size, pos);
	length = (arr->size - pos) * arr->stride;
	
	array_reserve(arr, count);

	begin = (void*) ((int) arr->data + pos * arr->stride);
	end = (void*) ((int) begin + size);
	if (length > 0)
		memmove(end, begin, length);
	memcpy(begin, first, size);

	arr->size = count;
}

void array_set(array_t *arr, int pos, const void *elem) {
	array_set_range(arr, pos, elem, elem);
}

void array_set_range(array_t *arr, int pos, const void *first, const void *last) {
	int size, count;
	void *buffer;

	if (!arr || !first || !last || pos < 0 || first > last)
		return;

	size = (int) last - (int) first + arr->stride;
	count = size / arr->stride + pos;
	array_reserve(arr, count);

	buffer = (void*) ((int) arr->data + pos * arr->stride);
	memcpy(buffer, first, size);

	arr->size = (arr->size > count) ? arr->size : count;
}

void array_remove(array_t *arr) {
	array_erase(arr, array_last(arr));
}

void array_remove_range(array_t *arr, int count) {
	int pos;
	void *first, *last;

	if (count <= 0)
		return;

	pos = array_size(arr) - count;
	first = array_at(arr, pos);
	last = array_last(arr);

	array_erase_range(arr, first, last);
}

void array_erase(array_t *arr, void *elem) {
	array_erase_range(arr, elem, elem);
}

void array_erase_range(array_t *arr, void *first, void *last) {
	int length;
	void *src;

	debug_assert(arr);
	debug_assert(first >= array_first(arr));
	debug_assert(last <= array_last(arr));
	debug_assert(first <= last);

	src = (void*)((int) last + arr->stride);
	if (first != src) {
		length = (int) array_last(arr) - (int) last + arr->stride;
		memmove(first, src, length);
	}

	length = (int) first - (int) last + arr->stride;
	arr->size -= length / arr->stride;
}

int array_size(const array_t *arr) {
	if (arr)
		return(arr->size);
	return(0);
}

int array_stride(const array_t *arr) {
	if (arr)
		return(arr->stride);
	return(0);
}

void *array_first(const array_t *arr) {
	if (!arr || !arr->size)
		return(NULL);
	return(arr->data);
}

void *array_last(const array_t *arr) {
	if (!arr || !arr->size)
		return(NULL);
	return((void*)((int)arr->data + (arr->size - 1) * arr->stride));
}

void *array_next(const array_t *arr, const void *item) {
	if (item < array_first(arr)
		|| item >= array_last(arr))
		return(NULL);

	return((void*) ((int) item + arr->stride));
}

void *array_prev(const array_t *arr, const void *item) {
	if (item <= array_first(arr)
		|| item > array_last(arr))
		return(NULL);

	return((void*) ((int) item - arr->stride));
}

void *array_at(const array_t *arr, int pos) {
	void *item;

	item = (void*) ((int) array_first(arr) + pos * array_stride(arr));
	if (item < array_first(arr)
		|| item > array_last(arr))
		return(NULL);

	return(item);
}

/******************************************************************************
*	List implementation
******************************************************************************/

static list_t *list_merge(list_t *left, list_t *right, compare_func cf)
{
	int cmp;
	list_t list, *next, *prev;

	prev = NULL;
	next = &list;
	while (left && right) {
		cmp = cf(left->data, right->data);
		if (cmp <= 0) {
			next->next = left;
			left = left->next;
		} else {
			next->next = right;
			right = right->next;
		}
		next = next->next;
		next->prev = prev;
		prev = next;
	}

	next->next = left ? left : right;
	next->next->prev = next;

	return(list.next);
}

list_t *list_create() {
	int size;
	list_t *list;

	size = sizeof(list_t);
	list = (list_t*) malloc(size);
	if (!list)
		debug_assert(!"not enough memory");

	memset(list, 0, size);
	return(list);
}

void list_destroy(list_t *list) {
	list_t *prev, *next;

	if (!list)
		return;

	next = list;
	prev = list->prev;
	if (prev)
		prev->next = NULL;

	while (next) {
		prev = next;
		next = next->next;
		free(prev);
	}
}

void list_dispose(list_t *list, release_func release) {
	list_t *next;

	next = list;
	while (next) {
		release(next->data);
		next = next->next;
	}

	list_destroy(list);
}

list_t *list_clone(const list_t *first, const list_t *last) {
	list_t *cur, *prev, *beg;
	const list_t *next;

	beg = NULL;
	prev = NULL;
	next = first;
	while (next) {
		cur = list_create();
		if (next == first)
			beg = cur;

		cur->prev = prev;
		cur->next = next->next;
		cur->data = next->data;

		if (prev)
			prev->next = cur;
		prev = cur;

		if (next == last) {
			cur->next = NULL;
			break;
		}

		next = next->next;
	}

	return(beg);
}

list_t *list_duplicate(const list_t *first, const list_t *last, duplicate_func dup) {
	list_t *beg, *next;
	const list_t *source;

	source = first;
	beg = list_clone(first, last);
	next = beg;
	while (next) {
		next->data = dup(source->data);
		next = next->next;
		source = source->next;
	}

	return(beg);
}

list_t *list_append(list_t *list, void *data) {
	list_t *last, *end;

	end = list_create();
	end->data = data;

	last = list_last(list);
	if (last) {
		end->prev = last;
		last->next = end;
	}

	return(end);
}

list_t *list_prepend(list_t *list, void *data) {
	list_t *beg;

	beg = list_create();
	beg->data = data;

	if (list) {
		beg->prev = list->prev;
		beg->next = list;
		 if (list->prev)
			 list->prev->next = beg;
		list->prev = beg;
	}
	else
		beg->prev = NULL;

	return(beg);
}

list_t *list_set(list_t *list, int pos, void *data) {
	list_t *item;

	if (!list)
		return(NULL);

	item = list_at(list, pos);
	if (item)
		item->data = data;

	return(item);
}

list_t *list_insert(list_t *list, int pos, void *data) {
	list_t *cur, *ins;

	ins = NULL;
	cur = list_at(list, pos);
	if (cur) {
		ins = list_create();
		ins->data = data;
		ins->prev = cur->prev;
		ins->next = cur;
		cur->prev = ins;
		if (ins->prev)
			ins->prev->next = ins;
	}

	return(ins);
}

list_t *list_insert_range(list_t *list, int pos, const list_t *first, const list_t *last) {
	list_t *range, *beg, *end;

	if (!list)
		return(NULL);

	range = list_clone(first, last);
	if (range) {
		beg = list_at(list, pos);
		end = list_next(beg);
		beg->next = range;
		range->prev = beg;
		if (end) {
			end->prev = list_last(range);
			end->prev->next = end;
		}
	}

	return(range);
}

list_t *list_remove(list_t *list) {
	list_t *prev, *next;

	if (!list)
		return(NULL);

	prev = list->prev;
	next = list->next;

	if (prev)
		prev->next = next;
	if (next)
		next->prev = prev;

	list->prev = NULL;
	list->next = NULL;
	list_destroy(list);

	return(next);
}

list_t *list_remove_front(list_t *list) {
	return(list_remove(list_first(list)));
}

list_t *list_remove_back(list_t *list) {
	return(list_remove(list_last(list)));
}

list_t *list_first(const list_t *list) {
	list_t *prev;

	if (!list)
		return(NULL);

	prev = (list_t*) list;
	while (prev->prev)
		prev = prev->prev;

	return(prev);
}

list_t *list_last(const list_t *list) {
	list_t *next;

	if (!list)
		return(NULL);

	next = (list_t*) list;
	while (next->next)
		next = next->next;

	return(next);
}

list_t *list_next(const list_t *list) {
	if (!list)
		return(NULL);
	return list->next;
}

list_t *list_prev(const list_t *list) {
	if (!list)
		return(NULL);
	return list->prev;
}

list_t *list_at(const list_t *list, int pos) {
	list_t *next;

	next = (list_t*) list;
	while (pos-- && next)
		next = next->next;

	return(next);
}

void *list_data(const list_t *list) {
	if (!list)
		return(NULL);
	return(list->data);
}

int list_size(const list_t *list) {
	int i;
	list_t *next;

	if (!list)
		return(0);

	i = 0;
	next = (list_t*) list;
	while (next) {
		next = next->next;
		i++;
	}

	return i;
}

list_t *list_sort(list_t *list, compare_func cmp) {
	list_t *right, *left;

	debug_assert(cmp);

	if (!list)
		return(NULL);
	if (!list->next)
		return(list);

	right = list;
	left = list->next;
	while ((left = left->next) != NULL) {
		if ((left = left->next) == NULL)
			break;
		right = right->next;
	}

	left = right->next;
	right->next = NULL;
	right = list;

	right = list_sort(right, cmp);
	left = list_sort(left, cmp);

	return(list_merge(right, left, cmp));
}

list_t *list_find(list_t *list, void *data, compare_func cmp) {
	debug_assert(cmp);

	while (list) {
		if (cmp(data, list->data) == 0)
			return(list);
		list = list->next;
	}

	return(NULL);
}

list_t *list_reverse(list_t *list) {
	list_t *last;
  
	last = NULL;
	while (list) {
		last = list;
		list = last->next;
		last->next = last->prev;
		last->prev = list;
	}
	
	return last;
}

/******************************************************************************
*	Tree implementation
******************************************************************************/

enum {
	RED,
	BLACK
};

static node_t *tree_nil(const tree_t *tree) {
	debug_assert(tree);
	return((node_t*) &tree->nil);
}

static node_t *tree_root(const tree_t *tree) {
	debug_assert(tree);
	return((node_t*) &tree->root);
}

static void tree_node_rotate_left(tree_t *tree, node_t *node) {
	node_t *child;

	child = node->right;
    node->right = child->left;

    if (child->left != tree_nil(tree))
        child->left->parent = node;
    child->parent = node->parent;

	if (node->parent == tree_nil(tree))
		tree->root.left = child;
    if (node == node->parent->left)
		node->parent->left = child;
    else
		node->parent->right = child;

    child->left = node;
    node->parent = child;
}

static void tree_node_rotate_right(tree_t *tree, node_t *node) {
	node_t *child;

	child = node->left;
    node->left = child->right;

    if (child->right != tree_nil(tree))
        child->right->parent = node;
    child->parent = node->parent;

	if (node->parent == tree_nil(tree))
		tree->root.left = child;
    if (node == node->parent->left)
		node->parent->left = child;
    else
		node->parent->right = child;

    child->right = node;
    node->parent = child;

}

static node_t *tree_successor(const tree_t *tree, const node_t *node) {
	node_t *succ;
	
	if ((succ = node->right) != tree_nil(tree)) {
		while (succ->left != tree_nil(tree))
			succ = succ->left;
	} else {
		for (succ = node->parent; node == succ->right; succ = succ->parent)
			node = succ;
		if (succ == tree_root(tree))
			succ = tree_nil(tree);
	}
	return(succ);
}

static node_t *tree_predecessor(const tree_t *tree, const node_t *node) {
	node_t *predc;
	if ((predc = node->left) != tree_nil(tree)) {
		while (predc->right != tree_nil(tree))
			predc = predc->right;
	} else {
		for (predc = node->parent; node == predc->left; predc = predc->parent)
			node = predc;
		if (predc == tree_root(tree))
			predc = tree_nil(tree);
	}
	return(predc);
}

static node_t *_tree_insert(tree_t *tree, void *data, int replace, release_func release) {
	int res;
	node_t *node, *parent, *newnode;

	node = tree->root.left;
	parent = tree_root(tree);

	while (node != tree_nil(tree)) {
		parent = node;
		if ((res = tree->cmp(data, node->data)) == 0 && replace) {
			if (release)
				release(node->data);
			node->data = data;
			return node;
		}
		node = (res <= 0) ? node->left : node->right;
	}

	tree->count++;

	node = newnode = (node_t*) malloc(sizeof(node_t));
	node->data = data;
	node->left = node->right = tree_nil(tree);
	node->parent = parent;
	if (parent == tree_root(tree) || tree->cmp(data, parent->data) <= 0)
		parent->left = node;
	else
		parent->right = node;
	node->color = RED;

	while (node->parent->color == RED) {
		if (node->parent == node->parent->parent->left) {
			if (node->parent->parent->right->color == RED) {
				node->parent->color = BLACK;
				node->parent->parent->color = RED;
				node->parent->parent->right->color = BLACK;
				node = node->parent->parent;
			} else {
				if (node == node->parent->right) {
					node = node->parent;
					tree_node_rotate_left(tree, node);
				}
				node->parent->color = BLACK;
				node->parent->parent->color = RED;
				tree_node_rotate_right(tree, node->parent->parent);
			}
		} else {
			if (node->parent->parent->left->color == RED) {
				node->parent->color = BLACK;
				node->parent->parent->color = RED;
				node->parent->parent->left->color = BLACK;
				node = node->parent->parent;
			} else {
				if (node == node->parent->left) {
					node = node->parent;
					tree_node_rotate_right(tree, node);
				}
				node->parent->color = BLACK;
				node->parent->parent->color = RED;
				tree_node_rotate_left(tree, node->parent->parent);
			}
		}
	}
	tree->root.left->color = BLACK;

	return(newnode);
}

static void tree_repair(tree_t *tree, node_t *node) {
	node_t *parent, *sibling, *nil;

	if (!tree || !node)
		return;

	parent = node->parent;
	nil = tree_nil(tree);
	while (node != tree->root.left && node->color == BLACK) {
		if (node == parent->left) {
			sibling = parent->right;
			if (sibling->color == RED) {
				sibling->color = BLACK;
				parent->color = RED;
				tree_node_rotate_left(tree, parent);
				sibling = parent->right;
			}
			if (sibling->left->color == BLACK && sibling->right->color == BLACK) {
				if (sibling != nil) sibling->color = RED;
				node = parent;
				parent = node->parent;
			} else {
				if (sibling->right->color == BLACK) {
					if (sibling->left != nil) sibling->left->color = BLACK;
					sibling->color = RED;
					tree_node_rotate_right(tree, sibling);
					sibling = parent->right;
				}
				sibling->color = parent->color;
				if (parent != nil) parent->color = BLACK;
				if (sibling->right->color != BLACK) {
					sibling->right->color = BLACK;
				}
				tree_node_rotate_left(tree, parent);
				node = tree->root.left;
			}
		} else {
			sibling = parent->left;
			if (sibling->color == RED) {
				sibling->color = BLACK;
				parent->color = RED;
				tree_node_rotate_right(tree, parent);
				sibling = parent->left;
			}
			if (sibling->right->color == BLACK && sibling->left->color == BLACK) {
				if (sibling != nil) sibling->color = RED;
				node = parent;
				parent = node->parent;
			} else {
				if (sibling->left->color == BLACK) {
					if (sibling->right != nil) sibling->right->color = BLACK;
					sibling->color = RED;
					tree_node_rotate_left(tree, sibling);
					sibling = parent->left;
				}
				sibling->color = parent->color;
				if (parent != nil) parent->color = BLACK;
				if (sibling->left->color != BLACK) {
					sibling->left->color = BLACK;
				}
				tree_node_rotate_right(tree, parent);
				node = tree->root.left;
			}
		}
	}
	node->color = BLACK;
}

tree_t *tree_create(compare_func cf) {
	tree_t *tree;
	int size;

	size = sizeof(tree_t);
	tree = (tree_t*) malloc(size);

	tree->count = 0;
	tree->cmp = cf;

	tree->nil.parent = tree_nil(tree);
	tree->nil.left = tree_nil(tree);
	tree->nil.right = tree_nil(tree);
	tree->nil.color = BLACK;
	tree->nil.data = NULL;

	tree->root = tree->nil;

	return(tree);
}

void tree_clear(tree_t *tree, release_func release) {
	node_t *node;

	node = tree_first(tree);
	while (node != NULL)
		node = tree_erase(tree, node, release);

	tree_root(tree)->left = tree_nil(tree);
}

void tree_destroy(tree_t *tree, release_func release) {
	tree_clear(tree, release);
	free(tree);
}

tree_t *tree_clone(tree_t *tree, duplicate_func dup) {
	node_t *tmp, *node, *next;
	tree_t *newtree = NULL;

	debug_assert(tree);

	newtree = tree_create(tree->cmp);
	next = tree_first(tree);
	node = tree_root(newtree);
	while (next != NULL) {
		tmp = (node_t*) malloc(sizeof(node_t));
		tmp->left = next->left != tree_nil(tree) ? next->left : tree_nil(newtree);
		tmp->right = next->right != tree_nil(tree) ? next->right : tree_nil(newtree);
		tmp->color = next->color;
		tmp->parent = node;

		tmp->data = dup ? dup(next->data) : next->data;

		if (next == next->parent->left)
			node->left = tmp;
		else
			node->right = tmp;

		next = tree_next(tree, next);
		node = tmp;
	}

	return(newtree);
}

node_t *tree_insert(tree_t *tree, void *data) {
	return(_tree_insert(tree, data, 0, NULL));
}

node_t *tree_replace(tree_t *tree, void *data, release_func release) {
	return(_tree_insert(tree, data, 1, release));
}

node_t *tree_erase(tree_t *tree, node_t *node, release_func release) {
	node_t *nil, *x, *y, *next;

	nil = tree_nil(tree);
	if (!node || node == nil)
		return(NULL);

	next = tree_successor(tree, node);
	if (node->left == nil || node->right == nil)
		y = node;
	else
		y = next;

	x = (y->left != nil) ? y->left : y->right;

	x->parent = y->parent;
	if (y->parent == &tree->root)
		tree->root.left = x;
	else if (y == y->parent->left)
		y->parent->left = x;
	else
		y->parent->right = x;

	if (y->color == BLACK)
		tree_repair(tree, x);

	if (y != node) {
		y->left = node->left;
		y->right = node->right;
		y->parent = node->parent;
		y->color = node->color;
		node->left->parent = y;
		node->right->parent = y;
		if (node == node->parent->left)
			node->parent->left = y;
		else
			node->parent->right = y;
	}

	tree->count--;
	if (release)
		release(node->data);
	free(node);

	return((next != nil) ? next : NULL);
}

node_t *tree_find(const tree_t *tree, const void *key) {
	int cmp;
	node_t *node;

	if (tree == NULL)
		return(NULL);

	node = tree->root.left;
	while (node != tree_nil(tree)) {
		cmp = tree->cmp(key, node->data);
		if (cmp < 0)
			node = node->left;
		else if (cmp > 0)
			node = node->right;
		else
			return(node);
	}

	return(NULL);
}

node_t *tree_first(const tree_t *tree) {
	node_t *next, *nil, *node;

	node = NULL;
	nil = tree_nil(tree);
	next = tree->root.left;
	while (next != nil) {
		node = next;
		next = node->left;
	}

	return(node);
}

node_t *tree_last(const tree_t *tree) {
	node_t *next, *nil, *node;

	node = NULL;
	nil = tree_nil(tree);
	next = tree->root.left;
	while (next != nil) {
		node = next;
		next = node->right;
	}

	return(node);
}

node_t *tree_next(const tree_t *tree, const node_t *node) {
	node_t *next;

	next = tree_successor(tree, node);
	if (next != tree_nil(tree))
		return(next);

	return(NULL);
}

node_t *tree_prev(const tree_t *tree, const node_t *node) {
	node_t *prev;

	prev = tree_predecessor(tree, node);
	if (prev != tree_nil(tree))
		return(prev);

	return(NULL);
}

void *tree_data(const tree_t *tree, const node_t *node) {
	if (node && node != tree_nil(tree))
		return(node->data);
	return(NULL);
}

int tree_size(const tree_t *tree) {
	if (tree)
		return(tree->count);
	return(0);
}