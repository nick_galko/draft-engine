/******************************************************************************
 *	File:			scene.c
 *
 *	Created by:		Ivan Shishkin <codingdude@gmail.com>
 *
 *	History:		Created 27/02/13
 *
 *	Copyright (c) 2013 All rights reserved
 *
 *****************************************************************************/

#include "platform.h"
#include "types.h"
#include "dslib.h"
#include "debug.h"
#include "maths.h"
#include "render.h"
#include "resource.h"
#include "collision.h"
#include "octree.h"
#include "scene.h"

/******************************************************************************
*	Scene implementation
******************************************************************************/

static scene_node_t		*root = NULL;

static camera_t			*active_camera = NULL;

static tree_t			*entities = NULL;
static tree_t			*nodes = NULL;
static tree_t			*cameras = NULL;

static array_t			*render_queue;

static octree_t			octree;

static int entity_compare_fn(const entity_t *s1, const entity_t *s2) {
	return(strcmp(s1->name, s2->name));
}

static int node_compare_fn(const scene_node_t *n1, const scene_node_t *n2) {
	return(strcmp(n1->name, n2->name));
}

static int camera_compare_fn(const camera_t *n1, const camera_t *n2) {
	return(strcmp(n1->name, n2->name));
}

static void surface_release_func(surface_t *surf) {
	if (surf) {
		texture_release(surf->texture);
		mesh_release(surf->mesh);
		free((char*) surf->entity.name);
		free(surf);
	}
}

static void camera_release_func(camera_t *cam) {
	if (cam) {
		free((char*)cam->name);
		free(cam);
	}
}

static void scene_update() {
	const matrix_t *view, *proj;
	int view_changed, proj_changed;

	if (active_camera) {
		view_changed = camera_view_changed(active_camera);
		proj_changed = camera_proj_changed(active_camera);

		camera_update(active_camera);

		if (view_changed) {
			view = camera_get_view(active_camera);
			render_bind_matrix(MATRIX_VIEW, view);
		}
		if (proj_changed) {
			proj = camera_get_proj(active_camera);
			render_bind_matrix(MATRIX_PROJECTION, proj);
		}

		scene_node_update(root);

		if (active_camera)
			octree_walk_tree(&octree, active_camera, render_queue);
	}
}

int scene_init() {
	vector3_t _max, _min;
	bbox_t bbox;
	int size;

	if (!root) {
		size = sizeof(scene_node_t);
		root = (scene_node_t*) malloc(size);
		root->name = strdup("root");
		root->entities = NULL;
		root->parent = NULL;
		root->children = array_create(sizeof(scene_node_t*));
		root->modified = 1;
		root->moved = 1;

		root->translation.x = 0;
		root->translation.y = 0;
		root->translation.z = 0;

		root->rotation.x = 0;
		root->rotation.y = 0;
		root->rotation.z = 0;
		root->rotation.w = 1.0f;

		root->scale.x = 1.0f;
		root->scale.y = 1.0f;
		root->scale.z = 1.0f;

		entities = tree_create((compare_func) entity_compare_fn);
		nodes = tree_create((compare_func) node_compare_fn);
		cameras = tree_create((compare_func) camera_compare_fn);
		render_queue = array_create(sizeof(scene_node_t*));

		bbox_set_infinite(&root->bbox);

		_max.x = _max.y = _max.z = 10000.0f;
		_min.x = _min.y = _min.z = -10000.f;
		bbox_set_extents(&bbox, &_min, &_max);
		octree_init(&octree, &bbox, 8);

		debug_message("scene successfully initialized");
	}
}

void scene_shutdown() {
	int i;

	if (root) {
		scene_clear();
		array_destroy(render_queue);
		tree_destroy(nodes, NULL);
		tree_destroy(cameras, NULL);
		tree_destroy(entities, NULL);

		array_destroy(root->children);
		free((char*) root->name);
		free(root);

		root = NULL;
		entities = NULL;
		nodes = NULL;
		active_camera = NULL;

		octree_destroy(&octree);

		debug_message("scene successfully shutted down");
	}
}

void scene_clear() {
	array_t *temp;
	scene_node_t **cur, **next;

	if (!root)
		debug_error("scene used while not initialized");

	temp = array_create_from_existing(
		array_first(root->children),
		array_last(root->children),
		array_stride(root->children)
		);
	next = (scene_node_t**) array_first(temp);
	while (next) {
		cur = next;
		next = (scene_node_t**) array_next(temp, cur);
		scene_node_destroy(*cur);
	}
	array_destroy(temp);

	tree_clear(entities, (release_func) surface_release_func);
	tree_clear(cameras, (release_func) camera_release_func);

	active_camera = NULL;
}

void scene_render() {
	int i, j;
	scene_node_t *node;
	const surface_t *surf;
	const entity_t *ent;


	if (!root)
		debug_error("scene is used while not initialized");

	scene_update();
	for (i = 0; i < array_size(render_queue); i++) {
		node = *(scene_node_t**) array_at(render_queue, i);
		if (!scene_node_lod_visible(node))
			continue;
		for (j = 0; j < array_size(node->entities); j++) {
			ent = *(entity_t**) array_at(node->entities, j);
			surf = scene_entity_get_surface(ent);
			if (surf && surf->mesh) {
				render_bind_matrix(MATRIX_MODEL, &node->world_tm);
				if (surf->texture)
					render_bind_texmap(
						surf->texture->texmap,
						surf->texture->flags
						);
				else
					render_bind_texmap(0, TEXMAP_2D);
				render_draw(
					surf->mesh->vbuffer,
					surf->mesh->vdecl,
					surf->mesh->ibuffer,
					surf->mesh->icount / 3
					);
			}
		}
	}

	array_clear(render_queue);
	render_swap_buffers();
}

scene_node_t *scene_get_root(void) {
	return(root);
}

/******************************************************************************
*	Entity implementation
******************************************************************************/

entity_t *scene_get_entity(const char *name) {
	node_t *node;
	entity_t *ent, key;

	if (!name || !name[0]) {
		debug_warning("scene_get_entity : null referenced pointer");
		return(0);
	}

	key.name = name;
	if (!(node = tree_find(entities, &key))) {
		return(0);
	}

	ent = (entity_t*) tree_data(entities, node);

	return(ent);
}

void scene_destroy_entity(entity_t *ent) {
	if (!ent) {
		debug_warning("scene_release_entity : invalid parameter");
		return;
	}
	ent->destroy_fn(ent);
}

int scene_entity_type(const entity_t *ent) {
	if (!ent) {
		debug_warning("scene_entity_type : invalid parameter");
		return(-1);
	}
	return(ent->type);
}

surface_t *scene_entity_get_surface(const entity_t *ent) {
	debug_assert(ent);

	if (ent->type == ENTITY_SURFACE)
		return((surface_t*)ent);

	return(NULL);
}

surface_t *scene_create_surface(const char *name, const char *mesh, const char *texture) {
	surface_t *surface;

	if (!root)
		debug_error("scene is used while not initialized");

	if (scene_get_entity(name))
		debug_error("scene_create_surface : entity '%s' already exists", name);

	surface = (surface_t*) malloc(sizeof(surface_t));
	surface->entity.name = strdup(name);
	surface->entity.type = ENTITY_SURFACE;
	surface->entity.destroy_fn = (entity_destroy_func) scene_destroy_surface;
	if (!(surface->mesh = (mesh_t*) resource_get(mesh)))
		surface->mesh = mesh_create(mesh);
	if (!(surface->texture = (texture_t*) resource_get(texture)))
		surface->texture = texture_create(texture);

	tree_insert(entities, surface);

	debug_message("surface '%s' has been created", name);

	return(surface);
}

void scene_destroy_surface(surface_t *surface) {
	node_t *node;
	int n;

	if (!surface) {
		debug_warning("scene_release_surface : null referenced pointer");
		return;
	}
	if (!(node = tree_find(entities, surface))) {
		debug_warning("can't release surface '%s', "
			"it is missing from the current scene",
			surface->entity.name
			);
		return;
	}

	texture_release(surface->texture);
	mesh_release(surface->mesh);
	tree_erase(entities, node, NULL);
	debug_message("surface '%s' has been released", surface->entity.name);
	free((char*) surface->entity.name);
	free(surface);
}

/******************************************************************************
*	Scene Node implementation
******************************************************************************/

scene_node_t *scene_node_create(const char *name) {
	scene_node_t *sn, key;

	if (!root)
		debug_error("scene is used while not initialized");

	if (!name || !name[0])
		debug_warning("scene_create_node : invalid parameter");

	key.name = name;
	if (tree_find(nodes, &key))
		debug_error("can't create node '%s', the node already exists", name);

	sn = (scene_node_t*) malloc(sizeof(scene_node_t));
	sn->name = strdup(name);
	sn->entities = array_create(sizeof(entity_t*));
	sn->children = array_create(sizeof(scene_node_t*));
	sn->parent = NULL;
	sn->lod = LOD_DISABLED;
	sn->range.v[0] = 0.0f;
	sn->range.v[1] = 100.0f;
	sn->translation.x = 0; sn->translation.y = 0; sn->translation.z = 0;
	sn->rotation.x = 0; sn->rotation.y = 0; sn->rotation.z = 0; sn->rotation.w = 1.0f;
	sn->scale.x = 1.0f; sn->scale.y = 1.0f; sn->scale.z = 1.0f;
	sn->modified = sn->moved = 1;
	sn->octant = NULL;

	bbox_identity(&sn->bbox);

	tree_insert(nodes, sn);

	debug_message("scene node '%s' has been created", name);

	return(sn);
}

void scene_node_destroy(scene_node_t *node) {
	int i;
	node_t *n;
	array_t *temp;
	entity_t *ent;
	scene_node_t **next, **cur;

	if (!root)
		debug_error("scene is used while not initialized");

	if (!node) {
		debug_warning("scene_destroy_node : invalid parameter");
		return;
	}

	if (!(n = tree_find(nodes, node))) {
		debug_warning("can't destroy scene node '%s', "
			"the node is missing from the current scene",
			node->name
			);
		return;
	}

	temp = array_create_from_existing(
		array_first(node->children),
		array_last(node->children),
		array_stride(node->children)
		);

	next = (scene_node_t**) array_first(temp);
	while (next) {
		cur = next;
		next = (scene_node_t**) array_next(temp, cur);
		scene_node_destroy(*cur);
	}

	array_destroy(temp);

	if (node->parent)
		scene_node_remove_child(node->parent, node);

	for (i = 0; i < array_size(node->entities); i++) {
		ent = *(entity_t**) array_at(node->entities, i);
		ent->destroy_fn(ent);
	}

	octree_remove_node(node);

	array_destroy(node->children);
	array_destroy(node->entities);

	debug_message("scene node '%s' has been destroyed", node->name);

	tree_erase(nodes, n, NULL);
	free((char*) node->name);
	free(node);
}

void scene_node_update(scene_node_t *node) {
	int i;
	entity_t *ent;
	surface_t *surf;
	scene_node_t *child;
	bbox_t extents;

	if (node->moved) {
		matrix_transformation(
			&node->tm,
			&node->translation,
			&node->rotation,
			&node->scale
			);
	}
	
	if (node->moved || (node->parent && node->parent->moved)) {
		node->moved = 1;
		if (node->parent)
			matrix_multiply(&node->world_tm, &node->parent->world_tm, &node->tm);
		else
			node->world_tm = node->tm;
	}

	if ((node->moved || node->modified) && !bbox_is_infinite(&node->bbox)) {
		bbox_identity(&extents);
		bbox_identity(&node->bbox);
		for (i = 0; i < scene_node_entity_count(node); i++) {
			ent = scene_node_get_entity(node, i);
			surf = scene_entity_get_surface(ent);
			if (surf && surf->mesh)
				bbox_merge(&extents, &extents, &surf->mesh->bbox);
		}

		bbox_transform(&extents, &extents, &node->world_tm);
		bbox_set_extents(&node->bbox, &extents.minimum, &extents.maximum);

		octree_update_node(&octree, node);
	}

	for (i = 0; i < scene_node_child_count(node); i++) {
		child = scene_node_get_child(node, i);
		scene_node_update(child);
	}

	node->moved = 0;
	node->modified = 0;
}

scene_node_t *scene_get_node(const char *name) {
	node_t *n;
	scene_node_t key, *result;

	if (!root)
		debug_error("scene is used while not initialized");

	if (!name || !name[0]) {
		debug_warning("scene_get_node : invalid parameter");
		return(NULL);
	}

	key.name = name;
	if (!(n = tree_find(nodes, &key))) {
		debug_warning("can't find scene node '%s', "
			"the node is missing from the current scene",
			name
			);
		return(NULL);
	}

	result = (scene_node_t*) tree_data(nodes, n);

	return(result);
}

void scene_node_add_child(scene_node_t *node, scene_node_t *child) {
	if (!node || !child) {
		debug_warning("scene_node_add_child : invalid parameter");
		return;
	}

	if (child->parent == node)
		return;

	if (child->parent)
		scene_node_remove_child(child->parent, child);

	array_append(node->children, &child);
	child->parent = node;
}

void scene_node_remove_child(scene_node_t *node, scene_node_t *child) {
	int i;
	scene_node_t **result;

	if (!node || !child) {
		debug_warning("scene_node_remove_child : invalid parameter");
		return;
	}

	for (i = 0; i < array_size(node->children); i++) {
		result = (scene_node_t**) array_at(node->children, i);
		if (*result == child) {
			array_erase(node->children, result);
			child->parent = NULL;
			return;
		}
	}

	debug_warning(
		"can't remove child node '%s' from node '%s', "
		"nothing is found",
		child->name,
		node->name
		);
}

int scene_node_child_count(const scene_node_t *node) {
	if (!node) {
		debug_warning("scene_node_child_count : invalid parameter");
		return(0);
	}
	return(array_size(node->children));
}

scene_node_t *scene_node_get_child(const scene_node_t *node, int i) {
	scene_node_t *sn;

	debug_assert(node && i < node->children->size);
	if (!node || i < 0 || i >= array_size(node->children)) {
		debug_warning("scene_node_get_child : invalid parameter");
		return(NULL);
	}

	sn = *(scene_node_t**) array_at(node->children, i);

	return(sn);
}

void scene_node_add_entity(scene_node_t *node, entity_t *ent) {
	int i;
	entity_t *result;

	if (!node || !ent) {
		debug_warning("scene_node_add_surface : invalid parameters");
		return;
	}

	for (i = 0; i < array_size(node->entities); i++) {
		result = *(entity_t**) array_at(node->entities, i);
		if (result == ent)
			return;
	}

	array_append(node->entities, &ent);
	node->modified = 1;
}

void scene_node_remove_entity(scene_node_t *node, entity_t *ent) {
	int i;
	entity_t **result;

	if (!node || !ent) {
		debug_warning("scene_node_remove_entity : invalid parameters");
		return;
	}

	for (i = 0; i < array_size(node->entities); i++) {
		result = (entity_t**) array_at(node->entities, i);
		if (*result == ent) {
			array_erase(node->entities, ent);
			node->modified = 1;
			return;
		}
	}

	debug_warning(
		"can't remove entity '%s' from node '%s', "
		"nothing is found",
		ent->name,
		node->name
		);
}

int scene_node_entity_count(const scene_node_t *node) {
	if (!node) {
		debug_warning("scene_node_entity_count : invalid parameter");
		return(0);
	}
	return(array_size(node->entities));
}

entity_t *scene_node_get_entity(const scene_node_t *node, int i) {
	entity_t *ent;

	debug_assert(node && i < node->entities->size);
	if (!node || i < 0 || i >= array_size(node->entities)) {
		debug_warning("scene_node_get_entity : invalid parameter");
		return(NULL);
	}

	ent = *(entity_t**) array_at(node->entities, i);

	return(ent);
}

void scene_node_rotate(scene_node_t *node, const quaternion_t *rotation) {
	if (!node || !rotation) {
		debug_warning("scene_node_rotate : invalid parameter");
		return;
	}
	quaternion_multiply(&node->rotation, &node->rotation, rotation);
	node->moved = 1;
}

void scene_node_translate(scene_node_t *node, const vector3_t *translation) {
	if (!node || !translation) {
		debug_warning("scene_node_translate : invalid parameter");
		return;
	}
	vector3_add(&node->translation, &node->translation, translation);
	node->moved = 1;
}

void scene_node_scale(scene_node_t *node, const vector3_t *scale) {
	if (!node || !scale) {
		debug_warning("scene_node_scale : invalid parameter");
		return;
	}
	node->scale.x *= scale->x;
	node->scale.y *= scale->y;
	node->scale.z *= scale->z;
	node->moved = 1;
}

void scene_node_set_rotation(scene_node_t *node, const quaternion_t *rotation) {
	if (!node || !rotation) {
		debug_warning("scene_node_set_rotation : invalid parameter");
		return;
	}
	node->rotation = *rotation;
	node->moved = 1;
}

void scene_node_set_translation(scene_node_t *node, const vector3_t *translation) {
	if (!node || !translation) {
		debug_warning("scene_node_translate : invalid parameter");
		return;
	}
	node->translation = *translation;
	node->moved = 1;
}

void scene_node_set_scale(scene_node_t *node, const vector3_t *scale) {
	if (!node || !scale) {
		debug_warning("scene_node_scale : invalid parameter");
		return;
	}
	node->scale = *scale;
	node->moved = 1;
}

const quaternion_t *scene_node_get_rotation(const scene_node_t *node) {
	if (!node) {
		debug_warning("scene_node_get_rotation : invalid parameter");
		return(NULL);
	}
	return(&node->rotation);
}

const vector3_t *scene_node_get_translation(const scene_node_t *node) {
	if (!node) {
		debug_warning("scene_node_get_translation : invalid parameter");
		return(NULL);
	}
	return(&node->translation);
}

const vector3_t *scene_node_get_scale(const scene_node_t *node) {
	if (!node) {
		debug_warning("scene_node_get_scale : invalid parameter");
		return(NULL);
	}
	return(&node->scale);
}

quaternion_t *scene_node_world_rotation(quaternion_t *out, const scene_node_t *node) {
	quaternion_t pr;

	if (!out || !node) {
		debug_warning("scene_node_world_rotation : invalid parameter");
		return(NULL);
	}

	if(node->parent) {
		quaternion_multiply(
			out,
			&node->rotation,
			scene_node_world_rotation(&pr, node->parent)
			);
	} else {
		*out = node->rotation;
	}

	return(out);
}

vector3_t *scene_node_world_translation(vector3_t *out, const scene_node_t *node) {
	vector3_t pt;

	if (!out || !node) {
		debug_warning("scene_node_world_translation : invalid parameter");
		return(NULL);
	}

	if(node->parent) {
		vector3_add(
			out,
			scene_node_world_translation(&pt, node->parent),
			&node->translation
			);
	} else {
		*out = node->translation;
	}

	return(out);
}

vector3_t *scene_node_world_scale(vector3_t *out, const scene_node_t *node) {
	vector3_t ps;

	if (!out || !node) {
		debug_warning("scene_node_world_scale : invalid parameter");
		return(NULL);
	}

	if(node->parent) {
		scene_node_world_scale(&ps, node->parent);
		out->x = node->scale.x * ps.x;
		out->y = node->scale.y * ps.y;
		out->z = node->scale.z * ps.z;
	} else {
		*out = node->scale;
	}

	return(out);
}

int scene_node_is_in(const scene_node_t *node, const bbox_t *bbox) {
	vector3_t center, size, nsize;

	if (bbox_is_infinite(bbox))
		return(1);

	bbox_get_center(&center, &node->bbox);
	if (
		bbox->maximum.x > center.x
		&& bbox->maximum.y > center.y
		&& bbox->maximum.z > center.z
		&& bbox->minimum.x < center.x
		&& bbox->minimum.y < center.y
		&& bbox->minimum.z < center.z
		)
		return(1);

	bbox_get_size(&size, bbox);
	bbox_get_size(&nsize, &node->bbox);

	return(nsize.x < size.x && nsize.y < size.y && nsize.z < size.z);
}

void scene_node_set_lodmode(scene_node_t *node, int mode) {
	if (!node) {
		debug_warning("scene_node_set_lodmode : invalid parameter");
		return;
	}
	node->lod = mode;
}

int scene_node_lod_mode(const scene_node_t *node) {
	if (!node) {
		debug_warning("scene_node_set_lodmode : invalid parameter");
		return -1;
	}
	return(node->lod);
}

void scene_node_set_range(scene_node_t *node, float _min, float _max) {
	if (!node || _min >= _max || _min < 0) {
		debug_warning("scene_node_set_lodmode : invalid parameter");
		return;
	}
	node->range.v[0] = _min;
	node->range.v[1] = _max;
}

int scene_node_lod_visible(scene_node_t *node) {
	vector3_t corners[8], temp;
	bbox_t bbox;
	int i;
	float w, h, k;

	if (!node) {
		debug_warning("scene_node_set_lodmode : invalid parameter");
		return(0);
	}

	if (node->lod == LOD_DISABLED)
		return(1);

	if (!active_camera)
		return(0);

	if (node->lod == LOD_PERCENTAGE) {
		bbox_identity(&bbox);
		bbox_get_corners(corners, &node->bbox);
		for (i = 0; i < 8; i++) {
			vector3_transform(&temp, &corners[i],
				&active_camera->frustum.viewproj);
			bbox_merge_point(&bbox, &bbox, &temp);
		}

		w = bbox.maximum.x - bbox.minimum.x;
		h = bbox.maximum.y - bbox.minimum.y;
		k = w * h * 100.0f;

		if ((k > 100.0f || k < node->range.v[1])
			&& (k > node->range.v[0]))
			return(1);
	} else if (node->lod == LOD_DISTANCE) {
		bbox_get_center(&corners[0], &node->bbox);
		vector3_substract(&temp, &corners[0], &active_camera->translation);
		k = vector3_length(&temp);
		if (k > node->range.v[0] && k < node->range.v[1])
			return(1);
	}

	return(0);
}

/******************************************************************************
*	Camera implementation
******************************************************************************/

camera_t *scene_create_camera(const char *name) {
	camera_t *cam;

	if (!root)
		debug_error("the scene is used while not initialized");

	if (!name || !name[0]) {
		debug_warning("scene_create_camera : invalid parameter");
		return(NULL);
	}

	if (tree_find(cameras, name))
		debug_error("camera with name '%s' already exists", name);

	cam = (camera_t*) malloc(sizeof(camera_t));
	cam->name = strdup(name);
	cam->fov = M_PI * 0.5f;
	cam->aspect = 1.3333f;
	cam->znear = 1.0f;
	cam->zfar = 100000.0f;
	cam->projection_type = PROJECTION_PERSPECTIVE_FOV;
	cam->projection_changed = 1;
	cam->translation.x = 0; cam->translation.y = 0; cam->translation.z = 0;
	cam->rotation.x = 0; cam->rotation.y = 0; cam->rotation.z = 0; cam->rotation.w = 1.0f;
	cam->scale.x = 1.0f; cam->scale.y = 1.0f; cam->scale.z = 1.0f;
	cam->view_changed = 1;

	tree_insert(cameras, cam);

	return(cam);
}

void scene_destroy_camera(camera_t *cam) {
	node_t *node;

	if (!root)
		debug_error("the scene is used while not initialized");

	node = tree_find(cameras, cam);
	if (node) {
		if (active_camera == cam)
			active_camera = NULL;
		tree_erase(cameras, node, (release_func) camera_release_func);
	}
	else
		debug_warning(
			"can't remove camera '%s "
			"from the scene, nothing is found",
			cam->name
		);
}

camera_t *scene_get_camera(const char *name) {
	node_t *node;
	camera_t *result, key;

	if (!root)
		debug_error("the scene is used while not initialized");

	key.name = name;
	node = tree_find(cameras, &key);
	if (node) {
		result = (camera_t*) tree_data(cameras, node);
		return(result);
	}
	else
		debug_warning(
			"can't find camera '%s "
			"in the scene, nothing is found",
			name
		);
}

camera_t *scene_get_active_camera() {
	return(active_camera);
}

void scene_set_active_camera(camera_t *cam) {
	if (!root)
		debug_error("the scene is used while not initialized");
	if (!cam) {
		debug_warning("scene_set_active_camera : invalid parameter");
		return;
	}
	active_camera = cam;
}

void camera_set_translation(camera_t *cam, const vector3_t *translations) {
	if (!cam) {
		debug_warning("camera_set_translation : invalid parameter");
		return;
	}
	cam->translation = *translations;
	cam->view_changed = 1;
}

void camera_set_rotation(camera_t *cam, const quaternion_t *rotations) {
	if (!cam || !rotations) {
		debug_warning("camera_set_translation : invalid parameter");
		return;
	}
	quaternion_normalize(&cam->rotation, rotations);
	cam->view_changed = 1;
}

void camera_set_scale(camera_t *cam, const vector3_t *scale) {
	if (!cam || !scale) {
		debug_warning("camera_set_scale : invalid parameter");
		return;
	}
	cam->scale = *scale;
	cam->view_changed = 1;
}

void camera_translate(camera_t *cam, const vector3_t *translations) {
	if (!cam || !translations) {
		debug_warning("camera_translate : invalid parameter");
		return;
	}
	vector3_add(&cam->translation, &cam->translation, translations);
	cam->view_changed = 1;
}

void camera_rotate(camera_t *cam, const quaternion_t *rotations) {
	quaternion_t q;

	if (!cam || !rotations) {
		debug_warning("camera_rotate : invalid parameter");
		return;
	}

	quaternion_multiply(&cam->rotation, &cam->rotation, rotations);
	cam->view_changed = 1;
}

void camera_scale(camera_t *cam, const vector3_t *scale) {
	if (!cam || !scale) {
		debug_warning("camera_scale : invalid parameter");
		return;
	}
	cam->scale.x *= scale->x;
	cam->scale.y *= scale->y;
	cam->scale.z *= scale->z;
	cam->view_changed = 1;
}

void camera_set_ortho(camera_t *cam, float w, float h, float znear, float zfar) {
	if (!cam) {
		debug_warning("camera_set_ortho : invalid parameter");
		return;
	}
	cam->width = w;
	cam->height = h;
	cam->znear = znear;
	cam->zfar = zfar;
	cam->projection_changed = 1;
	cam->projection_type = PROJECTION_ORTHO;
}

void camera_set_perspective(camera_t *cam, float w, float h, float znear, float zfar) {
	if (!cam) {
		debug_warning("camera_set_perspective : invalid parameter");
		return;
	}
	cam->width = w;
	cam->height = h;
	cam->znear = znear;
	cam->zfar = zfar;
	cam->projection_changed = 1;
	cam->projection_type = PROJECTION_PERSPECTIVE;
}

void camera_set_perspective_fov(camera_t *cam, float fovy, float aspect, float znear, float zfar) {
	if (!cam) {
		debug_warning("camera_set_perspective_fov : invalid parameter");
		return;
	}
	cam->fov = fovy;
	cam->aspect = aspect;
	cam->znear = znear;
	cam->zfar = zfar;
	cam->projection_changed = 1;
	cam->projection_type = PROJECTION_PERSPECTIVE_FOV;
}

void camera_update(camera_t *cam) {
	int viewproj_changed;

	if (!cam) {
		debug_warning("camera_set_perspective_fov : invalid parameter");
		return;
	}

	viewproj_changed = cam->view_changed || cam->projection_changed;

	if (cam->view_changed) {
		cam->view_changed = 0;
		frustum_set_view(&cam->frustum, &cam->translation, &cam->scale, &cam->rotation);
	}
	if (cam->projection_changed) {
		cam->projection_changed = 0;
		switch (cam->projection_type) {
		case PROJECTION_PERSPECTIVE:
			frustum_set_perspective(&cam->frustum, cam->width, cam->height, cam->znear, cam->zfar);
			break;
		case PROJECTION_PERSPECTIVE_FOV:
			frustum_set_perspective_fov(&cam->frustum, cam->fov, cam->aspect, cam->znear, cam->zfar);
			break;
		case PROJECTION_ORTHO:
			frustum_set_ortho(&cam->frustum, cam->width, cam->height, cam->znear, cam->zfar);
			break;
		}
	}
	if (viewproj_changed)
		matrix_multiply(&cam->frustum.viewproj, &cam->frustum.proj, &cam->frustum.view);
}

int camera_view_changed(const camera_t *cam) {
	if (!cam) {
		debug_warning("camera_view_changed : invalid parameter");
		return(0);
	}
	return(cam->view_changed);
}

int camera_proj_changed(const camera_t *cam) {
	if (!cam) {
		debug_warning("camera_proj_changed : invalid parameter");
		return(0);
	}
	return(cam->projection_changed);
}

const matrix_t *camera_get_view(const camera_t *cam) {
	if (!cam) {
		debug_warning("camera_get_view : invalid parameter");
		return(0);
	}
	return(&cam->frustum.view);
}

const matrix_t *camera_get_proj(const camera_t *cam) {
	if (!cam) {
		debug_warning("camera_get_proj : invalid parameter");
		return(0);
	}
	return(&cam->frustum.proj);
}