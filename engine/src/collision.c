/******************************************************************************
 *	File:			collision.c
 *
 *	Created by:		Ivan Shishkin <codingdude@gmail.com>
 *
 *	History:		Created 30/01/13
 *
 *	Copyright (c) 2013 All rights reserved
 *
 *****************************************************************************/

#include "platform.h"
#include "types.h"
#include "debug.h"
#include "maths.h"
#include "collision.h"

/******************************************************************************
 * Bounding box implementation
 *****************************************************************************/

bbox_t *bbox_identity(bbox_t *out) {
	debug_assert(out);

	out->infinite = 0;

	out->minimum.x  = FLT_MAX;
	out->minimum.y  = FLT_MAX;
	out->minimum.z  = FLT_MAX;

	out->maximum.x  = -FLT_MAX;
	out->maximum.y  = -FLT_MAX;
	out->maximum.z  = -FLT_MAX;

	return(out);
}

bbox_t *bbox_set_extents(bbox_t *out, const vector3_t *minimum, const vector3_t *maximum) {
	debug_assert(out);
	debug_assert(minimum);
	debug_assert(maximum);

	out->infinite = 0;

	out->minimum = *minimum;
	out->maximum = *maximum;

	return(out);
}

bbox_t *bbox_set_infinite(bbox_t *out) {
	debug_assert(out);
	out->infinite = 1;
	return(out);
}

bbox_t *bbox_merge(bbox_t *out, const bbox_t *bb1, const bbox_t *bb2) {
	debug_assert(out);
	debug_assert(bb1);
	debug_assert(bb2);

	if (bbox_is_infinite(bb1) || bbox_is_infinite(bb2))
		out->infinite = 1;
	else {
		out->infinite = 0;
		vector3_minimize(&out->minimum, &bb1->minimum, &bb2->minimum);
		vector3_maximize(&out->maximum, &bb1->maximum, &bb2->maximum);
	}

	return(out);
}

bbox_t *bbox_merge_point(bbox_t *out, const bbox_t *bb, const vector3_t *p) {
	debug_assert(out);
	debug_assert(bb);
	debug_assert(p);

	if (bbox_is_infinite(bb)) {
		*out = *bb;
		return(out);
	}

	out->infinite = 0;

	vector3_minimize(&out->minimum, &bb->minimum, p);
	vector3_maximize(&out->maximum, &bb->maximum, p);

	return(out);
}

bbox_t *bbox_transform(bbox_t *out, const bbox_t *bb, const matrix_t *tm) {
	vector3_t minimum, maximum, corner, temp;

	debug_assert(out);
	debug_assert(bb);
	debug_assert(tm);

	if (bb->infinite) {
		*out = *bb;
		return(out);
	}

	minimum = bb->minimum;
	maximum = bb->maximum;

	bbox_identity(out);

	corner = minimum;
	vector3_transform(&temp, &corner, tm);
	bbox_merge_point(out, out, &temp);

	corner.z = maximum.z;
	vector3_transform(&temp, &corner, tm);
	bbox_merge_point(out, out, &temp);

	corner.y = maximum.y;
	vector3_transform(&temp, &corner, tm);
	bbox_merge_point(out, out, &temp);

	corner.z = minimum.z;
	vector3_transform(&temp, &corner, tm);
	bbox_merge_point(out, out, &temp);

	corner.x = maximum.x;
	vector3_transform(&temp, &corner, tm);
	bbox_merge_point(out, out, &temp);

	corner.z = maximum.z;
	vector3_transform(&temp, &corner, tm);
	bbox_merge_point(out, out, &temp);

	corner.y = minimum.y;
	vector3_transform(&temp, &corner, tm);
	bbox_merge_point(out, out, &temp);

	corner.z = minimum.z;
	vector3_transform(&temp, &corner, tm);
	bbox_merge_point(out, out, &temp);

	return(out);
}

int bbox_intersects(const bbox_t *bb1, const bbox_t *bb2) {
	debug_assert(bb1);
	debug_assert(bb2);

	if (bbox_is_infinite(bb1) || bbox_is_infinite(bb2))
		return(1);
	if (bb1->maximum.x < bb2->minimum.x)
		return(0);
	if (bb1->maximum.y < bb2->minimum.y)
		return(0);
	if (bb1->maximum.z < bb2->maximum.z)
		return(0);
	if (bb1->minimum.x > bb2->minimum.x)
		return(0);
	if (bb1->minimum.y > bb2->minimum.y)
		return(0);
	if (bb1->minimum.x > bb2->minimum.z)
		return(0);

	return(1);
}

int bbox_intersects_point(const bbox_t *bb, const vector3_t *p) {
	debug_assert(bb);
	debug_assert(p);

	if (bb->infinite)
		return(1);

	return(
		p->x >= bb->minimum.x && p->x <= bb->maximum.x
		&& p->y >= bb->minimum.x && p->y <= bb->maximum.y
		&& p->z >= bb->minimum.z && p->z <= bb->maximum.z
		);
}

vector3_t *bbox_get_minimum(vector3_t *out, const bbox_t *bb) {
	debug_assert(out);
	debug_assert(bb);

	*out = bb->minimum;

	return(out);
}

vector3_t *bbox_get_maximum(vector3_t *out, const bbox_t *bb) {
	debug_assert(out);
	debug_assert(bb);

	*out = bb->maximum;

	return(out);
}

vector3_t *bbox_get_center(vector3_t *out, const bbox_t *bbox) {
	debug_assert(out);
	debug_assert(bbox);

	out->x = (bbox->maximum.x + bbox->minimum.x) * 0.5f;
	out->y = (bbox->maximum.y + bbox->minimum.y) * 0.5f;
	out->z = (bbox->maximum.z + bbox->minimum.z) * 0.5f;

	return(out);
}

vector3_t *bbox_get_size(vector3_t *out, const bbox_t *bbox) {
	debug_assert(out);
	debug_assert(bbox);

	vector3_substract(out, &bbox->maximum, &bbox->minimum);

	return(out);
}

vector3_t *bbox_get_half_size(vector3_t *out, const bbox_t *bbox) {
	vector3_t v;

	debug_assert(out);
	debug_assert(bbox);

	vector3_scale(out, vector3_substract(&v, &bbox->maximum, &bbox->minimum), 0.5f);

	return(out);
}

vector3_t *bbox_get_corners(vector3_t *out, const bbox_t *bb) {
	debug_assert(out);
	debug_assert(bb);

	if (bb->infinite)
		return(NULL);

	out[0] = bb->minimum;

	out[1].x = bb->minimum.x;
	out[1].y = bb->maximum.y;
	out[1].z = bb->minimum.z;

	out[2].x = bb->maximum.x;
	out[2].y = bb->maximum.y;
	out[2].z = bb->minimum.z;

	out[3].x = bb->maximum.x;
	out[3].y = bb->minimum.y;
	out[3].z = bb->minimum.z;

	out[4] = bb->maximum;

	out[5].x = bb->minimum.x;
	out[5].y = bb->maximum.y;
	out[5].z = bb->maximum.z;

	out[6].x = bb->minimum.x;
	out[6].y = bb->minimum.y;
	out[6].z = bb->maximum.z;

	out[7].x = bb->maximum.x;
	out[7].y = bb->minimum.y;
	out[7].z = bb->maximum.z;

	return(out);
}

int bbox_is_infinite(const bbox_t *bb) {
	debug_assert(bb);
	return(bb->infinite);
}

/******************************************************************************
 * Frustum implementation
 *****************************************************************************/

enum frustum_plane_t {
	FRUSTUM_PLANE_NEAR,
	FRUSTUM_PLANE_FAR,
	FRUSTUM_PLANE_LEFT,
	FRUSTUM_PLANE_RIGHT,
	FRUSTUM_PLANE_TOP,
	FRUSTUM_PLANE_BOTTOM
};

static void frustum_update_planes(frustum_t *f) {
	int i;

	debug_assert(f);

	if (f->need_update) {
		f->need_update = 0;

		f->planes[FRUSTUM_PLANE_LEFT].a = f->viewproj.m41 + f->viewproj.m11;
		f->planes[FRUSTUM_PLANE_LEFT].b = f->viewproj.m42 + f->viewproj.m12;
		f->planes[FRUSTUM_PLANE_LEFT].c = f->viewproj.m43 + f->viewproj.m13;
		f->planes[FRUSTUM_PLANE_LEFT].d = f->viewproj.m44 + f->viewproj.m14;

		f->planes[FRUSTUM_PLANE_RIGHT].a = f->viewproj.m41 - f->viewproj.m11;
		f->planes[FRUSTUM_PLANE_RIGHT].b = f->viewproj.m42 - f->viewproj.m12;
		f->planes[FRUSTUM_PLANE_RIGHT].c = f->viewproj.m43 - f->viewproj.m13;
		f->planes[FRUSTUM_PLANE_RIGHT].d = f->viewproj.m44 - f->viewproj.m14;

		f->planes[FRUSTUM_PLANE_TOP].a = f->viewproj.m41 - f->viewproj.m21;
		f->planes[FRUSTUM_PLANE_TOP].b = f->viewproj.m42 - f->viewproj.m22;
		f->planes[FRUSTUM_PLANE_TOP].c = f->viewproj.m43 - f->viewproj.m23;
		f->planes[FRUSTUM_PLANE_TOP].d = f->viewproj.m44 - f->viewproj.m24;

		f->planes[FRUSTUM_PLANE_BOTTOM].a = f->viewproj.m41 + f->viewproj.m21;
		f->planes[FRUSTUM_PLANE_BOTTOM].b = f->viewproj.m42 + f->viewproj.m22;
		f->planes[FRUSTUM_PLANE_BOTTOM].c = f->viewproj.m43 + f->viewproj.m23;
		f->planes[FRUSTUM_PLANE_BOTTOM].d = f->viewproj.m44 + f->viewproj.m24;

		f->planes[FRUSTUM_PLANE_FAR].a = f->viewproj.m41 - f->viewproj.m31;
		f->planes[FRUSTUM_PLANE_FAR].b = f->viewproj.m42 - f->viewproj.m32;
		f->planes[FRUSTUM_PLANE_FAR].c = f->viewproj.m43 - f->viewproj.m33;
		f->planes[FRUSTUM_PLANE_FAR].d = f->viewproj.m44 - f->viewproj.m34;

		f->planes[FRUSTUM_PLANE_NEAR].a = f->viewproj.m41 + f->viewproj.m31;
		f->planes[FRUSTUM_PLANE_NEAR].b = f->viewproj.m42 + f->viewproj.m32;
		f->planes[FRUSTUM_PLANE_NEAR].c = f->viewproj.m43 + f->viewproj.m33;
		f->planes[FRUSTUM_PLANE_NEAR].d = f->viewproj.m44 + f->viewproj.m34;

		for (i = 0; i < 6; i++)
			plane_normalize(&f->planes[i], &f->planes[i]);
	}
}

frustum_t *frustum_set_view(frustum_t *f, const vector3_t *translation,
	const vector3_t *scale, const quaternion_t *rotation) {
		matrix_t inv;

		debug_assert(f);

		f->need_update = 1;

		matrix_transformation(&inv, translation, rotation, scale);
		matrix_inverse(&f->view, &inv);

		return(f);
}

frustum_t * frustum_set_ortho(frustum_t *f, float w, float h, float zn, float zf) {
	debug_assert(f);

	f->need_update = 1;
	matrix_ortho(&f->proj, w, h, zn, zf);

	return(f);
}

frustum_t *frustum_set_perspective(frustum_t *f, float w, float h, float zn, float zf) {
	debug_assert(f);

	f->need_update = 1;
	matrix_perspective(&f->proj, w, h, zn, zf);

	return(f);
}

frustum_t *frustum_set_perspective_fov(frustum_t *f, float fov, float aspect, float zn, float zf) {
	debug_assert(f);

	f->need_update = 1;
	matrix_perspective_fov(&f->proj, fov, aspect, zn, zf);

	return(f);
}

const matrix_t *frustum_get_view_matrix(const frustum_t *f) {
	debug_assert(f);
	return(&f->view);
}

const matrix_t *frustum_get_proj_matrix(const frustum_t *f) {
	debug_assert(f);
	return(&f->proj);
}

int frustum_is_visible_box(frustum_t *fr, const bbox_t *bb) {
	int i;
	vector3_t corners[8];

	debug_assert(fr);
	debug_assert(bb);

	if (bb->infinite)
		return(1);

	frustum_update_planes(fr);
	bbox_get_corners(corners, bb);

	for (i = 0; i < 6; i++) {
		if (plane_dot_coord(&fr->planes[i], &corners[0]) > 0)
			continue;
		if (plane_dot_coord(&fr->planes[i], &corners[1]) > 0)
			continue;
		if (plane_dot_coord(&fr->planes[i], &corners[2]) > 0)
			continue;
		if (plane_dot_coord(&fr->planes[i], &corners[3]) > 0)
			continue;
		if (plane_dot_coord(&fr->planes[i], &corners[4]) > 0)
			continue;
		if (plane_dot_coord(&fr->planes[i], &corners[5]) > 0)
			continue;
		if (plane_dot_coord(&fr->planes[i], &corners[6]) > 0)
			continue;
		if (plane_dot_coord(&fr->planes[i], &corners[7]) > 0)
			continue;
		return(0);
	}

	return(1);
}
