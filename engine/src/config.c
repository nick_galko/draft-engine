/******************************************************************************
 *	File:			config.c
 *
 *	Created by:		Ivan Shishkin <codingdude@gmail.com>
 *
 *	History:		Created 07/02/13
 *
 *	Copyright (c) 2013 All rights reserved
 *
 *****************************************************************************/

#include "platform.h"
#include "types.h"
#include "debug.h"
#include "dslib.h"
#include "config.h"

typedef struct _keyval {
	char	*key;

	union {
		tree_t	*values;
		char	*value;
	};
} keyval_t;

static int cfg_compare_func(const keyval_t *k1, const keyval_t *k2) {
	debug_assert(k1);
	debug_assert(k2);
	return(strcmp(k1->key, k2->key));
}

static void key_release_func(keyval_t *kv) {
	debug_assert(kv);
	free(kv->key);
	free(kv->value);
	free(kv);
}

static void section_release_func(keyval_t *kv) {
	debug_assert(kv);
	tree_destroy(kv->values, (release_func) key_release_func);
	free(kv->key);
	free(kv);
}

static const char *trim(char *line) {
	char *pos, *space, *tab, *ret, *newln, *beg, *end;

	if (!line)
		return(NULL);

	pos = strchr(line, '#');
	if (pos)
		*pos = 0;

	beg = strchr(line, '\"');
	end = strrchr(line, '\"');

	space = tab = ret = newln = NULL;
	while (((space = strchr(line, ' ')) != NULL && (space < beg || space > end))
		|| (tab = strchr(line, '\t')) != NULL
		|| (ret = strchr(line, '\r')) != NULL
		|| (newln = strchr(line, '\n')) != NULL) {
			if (space != NULL && (space < beg || space > end))
				memmove(space, space+1, strlen(space));
			if (tab != NULL)
				memmove(tab, tab+1, strlen(tab));
			if (ret != NULL)
				memmove(ret, ret+1, strlen(ret));
			if (newln != NULL)
				memmove(newln, newln+1, strlen(newln));
			space = tab = ret = newln = NULL;
	}

	if (!line[0])
		return(NULL);

	return(line);
}

/*----------------------------------------------------------------------------*/

config_t config_open() {
	compare_func fn = (compare_func) cfg_compare_func;
	config_t cfg = (config_t) tree_create(fn);
	return(cfg);
}

void config_close(config_t cfg) {
	tree_destroy((tree_t*) cfg, (release_func) section_release_func);
}

int config_parse_file(config_t cfg, FILE *f) {
	int last;
	keyval_t *kv;
	tree_t *curent;
	char *mid, line[1024], section[1024];

	debug_assert(cfg);
	debug_assert(f);

	while (!feof(f)) {
		*line = 0;
		fgets(line, 1024, f);
		if (!trim(line))
			continue;

		last = strlen(line) - 1;
		mid = strchr(line, '=');
		if (line[0] == '[' && line[last] == ']') {
			line[last] = 0;
			strcpy(section, &line[1]);
			kv = (keyval_t*) malloc(sizeof(keyval_t));
			kv->key = strdup(section);
			kv->values = tree_create((compare_func) cfg_compare_func);
			tree_replace(cfg, kv, (release_func) section_release_func);
			curent = kv->values;
		} else if (mid != NULL) {
			*mid++ = 0;
			kv = (keyval_t*) malloc(sizeof(keyval_t));
			kv->key = strdup(line);
			kv->value = strdup(mid);
			tree_replace(curent, kv, (release_func) key_release_func);
		} else
			return(0);
	}

	return(1);
}

void config_save_file(config_t cfg, FILE *f) {
	keyval_t *kv1, *kv2;
	node_t *n1, *n2;

	debug_assert(cfg);
	debug_assert(f);

	fputs("# this file is generated automatically\n", f);
	fputs("# all hand made changes will be lost\n", f);

	n1 = tree_first(cfg);
	while (n1) {
		kv1 = (keyval_t*) tree_data(cfg, n1);
		n1 = tree_next(cfg, n1);
		n2 = tree_first(kv1->values);
		fprintf(f, "[%s]\n", kv1->key);
		while (n2) {
			kv2 = (keyval_t*) tree_data(kv1->values, n2);
			n2 = tree_next(kv1->values, n2);
			fprintf(f, "%s = %s\n", kv2->key, kv2->value);
		}
	}
}

int config_load(config_t cfg, const char *file) {
	FILE *f;

	debug_assert(cfg);
	debug_assert(file && file[0]);

	if (!cfg || !file || !file[0]) {
		debug_warning("unable to load config file '%s', invalid parameters", file);
		return(0);
	}
	f = fopen(file, "r");
	if (!f) {
		debug_warning("unable to load config file '%s', file not found", file);
		return(0);
	}
	if (!config_parse_file(cfg, f)) {
		debug_warning("unable to load config file '%s', invalid file format", file);
		return(0);
	}

	debug_message("config file '%s' successfully loaded", file);

	fclose(f);
	return(1);
}

int config_save(config_t cfg, const char *file) {
	FILE *f;

	debug_assert(cfg);
	debug_assert(file);

	if (!cfg || !file || !file[0]) {
		debug_warning("unable to save config file '%s', invalid parameters");
		return(0);
	}
	f = fopen(file, "w+");
	if (!f) {
		debug_warning("unable to save config file '%s', can't open a file to write");
		return(0);
	}

	config_save_file(cfg, f);
	debug_message("config file '%s' successfully saved", file);

	fclose(f);
	return(1);
}

void config_clear(config_t cfg) {
	tree_clear(cfg, (release_func) section_release_func);
}

void config_set_value(config_t cfg, const char *section, const char *key, const char *value) {
	node_t *node;
	keyval_t *kv1, *kv2;

	debug_assert(cfg);
	debug_assert(section);
	debug_assert(key);
	debug_assert(value);

	if (!cfg || !section || !key || !value) {
		debug_warning("config_set_value : invalid parameter");
		return;
	}

	kv1 = (keyval_t*) malloc(sizeof(keyval_t));
	kv1->key = strdup(section);

	node = tree_find(cfg, kv1);
	if (!node) {
		kv1->values = tree_create((compare_func) cfg_compare_func);
		tree_insert(cfg, kv1);
	} else {
		kv1->value = NULL;
		key_release_func(kv1);
		kv1 = (keyval_t*) tree_data(cfg, node);
	}

	kv2 = (keyval_t*) malloc(sizeof(keyval_t));
	kv2->key = strdup(key);
	kv2->value = strdup(value);
	tree_replace(kv1->values, kv2, (release_func) key_release_func);
}

void config_set_string(config_t cfg, const char *section, const char *key, const char *str) {
	int len;
	char *buffer;

	len = strlen(str) + 10;
	buffer = (char*) malloc(len);
	sprintf(buffer, "\"%s\"", str);
	config_set_value(cfg, section, key, buffer);
	free(buffer);
}

void config_set_int(config_t cfg, const char *section, const char *key, int value) {
	char buffer[80];
	sprintf(buffer, "%i", value);
	config_set_value(cfg, section, key, buffer);
}

void config_set_float(config_t cfg, const char *section, const char *key, float value) {
	char buffer[80];
	sprintf(buffer, "%f", value);
	config_set_value(cfg, section, key, buffer);
}

int config_get_section_count(config_t cfg) {
	debug_assert(cfg);

	if(cfg)
		return(tree_size(cfg));

	debug_warning("config_get_section_count : invalid parameter");
	return(0);
}

const char *config_get_section(config_t cfg, int i) {
	int k;
	keyval_t *kv;
	node_t *n;

	debug_assert(cfg);
	debug_assert(i >= 0);
	debug_assert(i < tree_size(cfg));

	if (!cfg || i < 0 || i >= tree_size(cfg)) {
		debug_warning("config_get_section : invalid parameter");
		return(NULL);
	}

	n = tree_first(cfg);
	for (k = 1; k <= i; k++)
		n = tree_next(cfg, n);

	kv = (keyval_t*) tree_data(cfg, n);

	return(kv->key);
}

int config_get_key_count(config_t cfg, const char *section) {
	keyval_t kv1, *kv2;
	node_t *n;

	debug_assert(cfg);
	debug_assert(section);

	if (!cfg || !section) {
		debug_warning("config_get_key_count : invalid parameter");
		return(0);
	}

	kv1.key = (char*) section;
	n = tree_find(cfg, &kv1);
	if (!n)
		return(0);

	kv2 = (keyval_t*) tree_data(cfg, n);
	return(tree_size(kv2->values));
}

const char *config_get_key(config_t cfg, const char *section, int i) {
	int k;
	keyval_t kv1, *kv2;
	node_t *n1, *n2;

	debug_assert(cfg);
	debug_assert(section);

	if (!cfg || !section) {
		debug_warning("config_get_key : invalid parameter");
		return(0);
	}

	kv1.key = (char*) section;
	n1 = tree_find(cfg, &kv1);
	if (!n1)
		return(NULL);

	kv2 = (keyval_t*) tree_data(cfg, n1);
	debug_assert(i < tree_size(kv2->values));
	if (i >= tree_size(kv2->values)) {
		debug_warning("config_get_key : index out of bounds");
		return(NULL);
	}

	n2 = tree_first(kv2->values);
	for (k = 0; k < i; k++)
		n2 = tree_next(kv2->values, n2);

	kv2 = (keyval_t*) tree_data(kv2->values, n2);
	return(kv2->key);
}

const char *config_get_value(config_t cfg, const char *section, const char *key, const char *def) {
	keyval_t kv1, *kv2;
	node_t *n1, *n2;

	debug_assert(cfg);
	debug_assert(section);
	debug_assert(key);

	if (!cfg || !section || !key) {
		debug_warning("config_get_value : invalid parameter");
		return(def);
	}

	kv1.key = (char*) section;
	n1 = tree_find(cfg, &kv1);
	if (!n1)
		return(def);

	kv2 = (keyval_t*) tree_data(cfg, n1);
	kv1.key = (char*) key;
	n2 = tree_find(kv2->values, &kv1);
	if (!n2)
		return(def);

	kv2 = (keyval_t*) tree_data(kv2->values, n2);
	return(kv2->value);
}

const char *config_get_string(config_t cfg, const char *section, const char *key, char *buffer, int length) {
	int count;
	const char *beg, *end, *str = config_get_value(cfg, section, key, NULL);
	if (!str)
		return(buffer);

	beg = strchr(str, '\"');
	end = strrchr(str, '\"');

	if (beg && end && beg != end) {
		count = end - beg - 1;
		memcpy(buffer, str+1, count);
	} else {
		count = strlen(str);
		count = (length > count) ? count : length;
		memcpy(buffer, str, count);
	}

	buffer[count] = 0;

	return(buffer);
}

int config_get_int(config_t cfg, const char *section, const char *key, int def) {
	const char *str = config_get_value(cfg, section, key, NULL);
	if (!str)
		return(def);
	return(strtoul(str, NULL, 10));
}

float config_get_float(config_t cfg, const char *section, const char *key, float def) {
	const char *str = config_get_value(cfg, section, key, NULL);
	if (!str)
		return(def);
	return(strtod(str, NULL));
}
