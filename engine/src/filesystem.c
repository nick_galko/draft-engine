/******************************************************************************
 *	File:			filesystem.c
 *
 *	Created by:		Ivan Shishkin <codingdude@gmail.com>
 *
 *	History:		Created 28/12/12
 *
 *	Copyright (c) 2012 All rights reserved
 *
 *****************************************************************************/

#include "platform.h"
#include "types.h"
#include "debug.h"
#include "filesystem.h"
#include "dslib.h"
#include "lz4.h"
#include "config.h"

static int dword_aligned(int n) {
	return(((n + 3) / 4) * 4);
}

/******************************************************************************
*	File System implementation
******************************************************************************/

typedef struct _contents_entity {
	char	*path;
	int		offset;
} contents_entity_t;

typedef struct _virtual_fs {
	config_t	config;
	tree_t		*contents;
	FILE		*archive;
	int			offset;
} virtual_fs_t;

static virtual_fs_t	vfs = {NULL};

static int contents_compare_fn(const contents_entity_t *e1, const contents_entity_t *e2) {
	return(strcmp(e1->path, e2->path));
}

static void contents_release_fn(contents_entity_t *e) {
	free(e->path);
	free(e);
}

static const char *fs_get_full_name(const char *filename, const char *alias, char *buffer, int size) {
	char path[255];

	if (!alias || !alias[0] || !filename || !filename[0] || !buffer) {
		debug_warning("fs_reader_load : invalid parameter");
		return(buffer);
	}

	path[0] = 0;
	if (config_get_string(vfs.config, "file_system", alias, path, 255)[0] == 0) {
		debug_warning("can't find path by alias '%s'", alias);
		return(buffer);
	}

	sprintf(buffer, "%s/%s", path, filename);
	return(buffer);
}

static int fs_reader_load_archived(reader_t *r, const char *file) {
	contents_entity_t key, *e;
	reader_t *r1;
	node_t *n;
	uint32_t id, offset, size;

	if (!vfs.archive)
		return(0);

	key.path = (char*) file;
	n = tree_find(vfs.contents, &key);
	if (!n)
		return(0);

	e = (contents_entity_t*) tree_data(vfs.contents, n);
	offset = e->offset + vfs.offset;

	fseek(vfs.archive, offset, SEEK_SET);
	fread(&id, sizeof(uint32_t), 1, vfs.archive);
	fread(&size, sizeof(uint32_t), 1, vfs.archive);
	
	reader_clear(r);
	r->data = (uint8_t*) malloc(size);
	fread(r->data, 1, size, vfs.archive);
	r->u8p = r->data;
	r->size = size;
	r->id = id;

	if (reader_compressed(r)) {
		r1 = reader_decompress(r);
		reader_clear(r);
		r->data = r1->data;
		r->size = r1->size;
		r->u8p = r1->u8p;
		r->id = r1->id;
	}

	return(1);
}

static int fs_read_archive_contents(config_t cfg, const char *archive) {
	reader_t *r1, *r2, *r3, *r;
	const char *path, *alias;
	contents_entity_t *kv;
	int i;

	r1 = fs_reader_open();
	if (!fs_reader_load_file(r1, archive)) {
		r2 = reader_find(r1, 0, 1);
		if (!r2) {
			fs_reader_close(r1);
			return(0);
		}

		r = reader_decompress(r2);
		fs_reader_close(r2);

		i = 0;
		while (!reader_eof(r)) {
			r3 = reader_find(r, i, 1);
			if (!r3) {
				fs_reader_close(r);
				fs_reader_close(r1);
				return(0);
			}

			alias = reader_r_sz(r3);
			path = reader_r_sz(r3);

			config_set_string(vfs.config, "file_system", alias, path);

			fs_reader_close(r3);
			i++;
		}

		fs_reader_close(r);

		r2 = reader_find(r1, 0, 1);
		if (!r2) {
			fs_reader_close(r1);
			return(0);
		}

		r = reader_decompress(r2);
		fs_reader_close(r2);

		vfs.contents = tree_create((compare_func) contents_compare_fn);
		vfs.archive = fopen(archive, "rb");
		vfs.offset = ((int)r1->u8p) - ((int)r1->data) + 2 * sizeof(uint32_t);

		while (!reader_eof(r)) {
			r3 = reader_find(r, 0, 1);
			if (!r3) {
				fs_reader_close(r);
				fs_reader_close(r1);
				return(0);
			}

			kv = (contents_entity_t*) malloc(sizeof(contents_entity_t));
			kv->path = strdup(reader_r_sz(r3));
			kv->offset = reader_r_s32(r3);
			tree_insert(vfs.contents, kv);

			fs_reader_close(r3);
		}

		fs_reader_close(r);
	}

	fs_reader_close(r1);

	return(1);
}

void fs_init(const char *fs_config) {
	const char *suffix;

	if (vfs.config) {
		debug_warning("file system already initialized");
		return;
	}

	vfs.config = config_open();
	suffix = strrchr(fs_config, '.');
	if (suffix) {
		if (strcmp(suffix, ".cfg") == 0) {
			if (!config_load(vfs.config, fs_config))
				debug_error("can't read file system configuration file '%s'", fs_config);
		} else if (strcmp(suffix, ".pak") == 0) {
			if (!fs_read_archive_contents(vfs.config, fs_config))
				debug_error("can't read archive file '%s'" ,fs_config);
		} else
			debug_error("fs_init : wrong configuration file '%s'", fs_config);
	}

	debug_message("file system successfully initialized");
}

void fs_shutdown() {
	if (vfs.config) {
		config_close(vfs.config);
		vfs.config = NULL;
		if (vfs.contents) {
			tree_destroy(vfs.contents, (release_func) contents_release_fn);
			vfs.contents = NULL;
			fclose(vfs.archive);
			vfs.archive = NULL;
		}
		debug_message("file system has been shutted down");
	}
}

reader_t *fs_reader_open() {
	int size;
	reader_t *r;

	size = sizeof(reader_t);
	r = (reader_t*) malloc(size);
	if (r == NULL)
		return(0);

	memset(r, 0, size);
	return(r);
}

void fs_reader_close(reader_t *r) {
	reader_clear(r);
	free(r);
}

int fs_get_alias_count() {
	return(config_get_key_count(vfs.config, "file_system"));
}

const char *fs_get_alias(int index) {
	return(config_get_key(vfs.config, "file_system", index));
}

const char *fs_get_path(const char *alias, char *buffer, int length) {
	return(config_get_string(vfs.config, "file_system", alias, buffer, length));
}

const char *fs_get_archive(char *buffer, int length) {
	return(config_get_string(vfs.config, "archive", "path", buffer, length));
}

int fs_reader_load(reader_t *r, const char *alias, const char *filename) {
	char file[255];

	if (fs_get_full_name(filename, alias, file, 255)) {
		if (fs_reader_load_file(r, file) == 0) {
			debug_message("file '%s' successfully loaded", filename);
			return(1);
		} else if (fs_reader_load_archived(r, file)) {
			debug_message("archieved file '%s' successfully loaded", filename);
			return(1);
		}
	}

	return(0);
}

int fs_reader_load_file(reader_t *r, const char *filename) {
	FILE *f;
	uint32_t size;
	uint8_t *data;

	if (r == NULL)
		return(-1L);

	f = fopen(filename, "rb");
	if (f == NULL)
		return(-1L);
	if (fseek(f, 0, SEEK_END) != 0) {
		fclose(f);
		return(-1L);
	}
	size = ftell(f);
	if (size <= 0) {
		fclose(f);
		return(-1L);
	}
	if (fseek(f, 0, SEEK_SET) != 0) {
		fclose(f);
		return(-1L);
	}
	data = (uint8_t*) malloc(size);
	if (fread(data, 1, size, f) < size) {
		fclose(f);
		return(-1L);
	}
	fclose(f);

	reader_clear(r);
	r->size = size;
	r->data = data;
	r->u8p = data;

	return(0);
}

writer_t *fs_writer_open() {
	int size;
	writer_t *w;

	size = sizeof(writer_t);
	w = (writer_t*) malloc(size);
	if (w == NULL)
		return(0);

	memset(w, 0, size);

	w->data = array_create(sizeof(uint8_t));

	return(w);
}

void fs_writer_close(writer_t *w) {
	if (w == NULL)
		return;
	array_destroy(w->data);
	free(w);
}

int fs_writer_save(writer_t *w, const char *alias, const char *filename) {
	char file[255];

	if (fs_get_full_name(filename, alias, file, 255)) {
		if (fs_writer_save_file(w, file) == 0) {
			debug_message("file '%s' successfully saved", filename);
			return(1);
		}
	}

	return(0);
}

int fs_writer_save_file(writer_t *w, const char *filename) {
	FILE *f;
	int size;

	if (w == NULL)
		return(-1L);
	if (w->data == NULL)
		return(-1L);

	f = fopen(filename, "wb");
	if (f == NULL)
		return(-1L);
	size = array_size(w->data);
	if (fwrite(array_first(w->data), 1, size, f) < size) {
		fclose(f);
		return(-1L);
	}
	fclose(f);

	return(0);
}

/******************************************************************************
*	Reader implementation
******************************************************************************/

void reader_clear(reader_t *r) {
	int size;
	if (r == NULL)
		return;
	if (!r->refer)
		free(r->data);
	size = sizeof(reader_t);
	memset(r, 0, size);
}

reader_t *reader_find(reader_t *r, int id, int fromhere) {
	uint32_t n, size;
	reader_t *temp;

	if (r == NULL)
		return(NULL);

	if (!fromhere) {
		if (reader_seek(r, 0) < 0)
			return(NULL);
	}
	while (!reader_eof(r)) {
		n = (reader_r_u32(r) & ~0x80000000);
		size = reader_r_u32(r);
		if (n == id) {
			temp = fs_reader_open();
			if (temp == NULL)
				return(NULL);

			temp->id = id;
			temp->size = size;
			temp->refer = 1;
			temp->data = r->u8p;
			temp->u8p = r->u8p;

			reader_advance(r, dword_aligned(size));

			return(temp);
		}
		reader_advance(r, dword_aligned(size));
	}
	return(NULL);
}

int reader_copy(reader_t *source, reader_t *target, int id) {
	reader_t *temp;
	long ret; ret = -1L;

	temp = reader_find(source, id, 1);
	if (temp == NULL)
		return(ret);

	if (temp->size > 0) {
		reader_clear(target);
		target->id = temp->id;
		target->size = temp->size;

		target->data = (uint8_t*) malloc(target->size);
		if (target->data == NULL) {
			fs_reader_close(temp);
			return(ret);
		}

		memcpy(target->data, temp->data, temp->size);
		target->u8p = target->data;

		ret = 0;
	}

	fs_reader_close(temp);
	return(ret);
}

reader_t *reader_decompress(reader_t *source) {
	reader_t *target;
	uint8_t *data;
	int size, usize;

	debug_assert(source);

	size = reader_size(source) - sizeof(uint32_t);
	debug_assert(size > 0);
	size = reader_r_u32(source);
	usize = LZ4_compressBound(size);
	data = (uint8_t*) malloc(usize);
	usize = LZ4_uncompress((char*) source->u8p, (char*) data, usize);

	if (usize > size)
		realloc(data, size);

	target = fs_reader_open();
	target->data = data;
	target->u8p = data;
	target->size = size;
	target->id = source->id & ~0x80000000;

	return(target);
}

int reader_compressed(reader_t *r) {
	int id = reader_id(r);
	return(id & 0x80000000);
}

int reader_id(reader_t *r) {
	if (r == NULL)
		return(-1L);
	return r->id;
}

int reader_size(reader_t *r) {
	if (r == NULL)
		return(-1L);
	return r->size;
}

void *reader_data(reader_t *r) {
	if (r == NULL)
		return(0);
	return r->data;
}

void *reader_ptr(reader_t *r)
{
	if (r == NULL)
		return(0);
	return r->u8p;
}

int reader_seek(reader_t *r, int offset) {
	uint8_t *beg, *end, *ptr;

	if (r == NULL)
		return(-1L);
	if(r->u8p == NULL)
		return(-1L);

	beg = r->data;
	end = r->size + beg;
	ptr = beg + offset;

	if (ptr < beg || ptr > end)
		return -1;

	r->u8p = ptr;

	return(0);
}

int reader_advance(reader_t *r, int offset) {
	uint8_t *beg, *end, *ptr;

	if (r == NULL)
		return(-1L);
	if(r->u8p == NULL)
		return(-1L);

	beg = r->data;
	end = r->size + beg;
	ptr = r->u8p + offset;

	if (ptr < beg || ptr > end)
		return -1;

	r->u8p = ptr;

	return(0);
}

int reader_tell(reader_t *r) {
	uint8_t *beg, *cur;

	if (r == NULL)
		return(-1L);
	if(r->u8p == NULL)
		return(-1L);

	beg = r->data;
	cur = r->u8p;

	return(cur - beg);
}

int reader_elapsed(reader_t *r) {
	uint8_t *end, *cur;

	if (r == NULL)
		return(0);
	if(r->u8p == NULL)
		return(0);

	end = r->data + r->size;
	cur = r->u8p;

	return(end - cur);
}

int reader_eof(reader_t *r) {
	uint8_t *cur, *end;

	if (r == NULL)
		return(1);
	if(r->u8p == NULL)
		return(1);

	cur = r->u8p;
	end = r->data + r->size;

	return (cur >= end);
}

int reader_read(reader_t *r, void *data, int size) {
	int n;

	if (r == NULL)
		return(0);
	if(r->u8p == NULL)
		return(0);

	n = reader_elapsed(r);
	n = (n < size) ? n : size;
	memcpy(data, r->u8p, n);

	return(n);
}

uint8_t reader_r_u8(reader_t *r) {
	debug_assert(r);
	return(*r->u8p++);
}

int8_t reader_r_s8(reader_t *r)
{
	debug_assert(r);
	return(*r->s8p++);
}

uint16_t reader_r_u16(reader_t *r) {
	debug_assert(r);
	return(*r->u16p++);
}

int16_t reader_r_s16(reader_t *r) {
	debug_assert(r);
	return(*r->s16p++);
}

uint32_t reader_r_u32(reader_t *r) {
	debug_assert(r);
	return(*r->u32p++);
}

int32_t reader_r_s32(reader_t *r) {
	debug_assert(r);
	return(*r->s32p++);
}

uint64_t reader_r_u64(reader_t *r) {
	uint64_t *u64p;

	debug_assert(r);
	u64p = (uint64_t*) r->u32p++; r->u32p++;
	return(*u64p);
}

int64_t reader_r_s64(reader_t *r) {
	int64_t *s64p;

	debug_assert(r);
	s64p = (int64_t*) r->s32p++; r->s32p++;
	return(*s64p);
}

float reader_r_f32(reader_t *r) {
	debug_assert(r);
	return(*r->f32p++);
}

const char *reader_r_sz(reader_t *r) {
	const char *str;
	debug_assert(r);
	str = (const char*)r->s8p;
	reader_advance(r, dword_aligned(strlen(str) + 1));
	return(str);
}

/******************************************************************************
*	Writer implementation
******************************************************************************/

void writer_clear(writer_t *w) {
	if (w == NULL)
		return
	array_clear(w->data);
	w->ptr = 0;
}

writer_t *writer_compress(writer_t *source) {
	char *ip, *op;
	int size, id;
	writer_t *target;

	debug_assert(source);

	size = LZ4_compressBound(array_size(source->data));
	ip = (char*) array_at(source->data, 0);
	op = (char*) malloc(size);
	size = LZ4_compress(ip, op, size);
	id = source->id | 0x80000000;

	target = fs_writer_open();
	writer_set_id(target, id);
	writer_w_u32(target, array_size(source->data));
	writer_write(target, op, size);

	free(op);

	return(target);
}

int writer_append(writer_t *source, writer_t *target) {
	int pad, i;

	if(source == NULL)
		return(-1L);
	if(target == NULL)
		return(-1L);

	if (writer_seek(target, dword_aligned(array_size(target->data))) != 0)
		return(-1L);
	if (writer_w_u32(target, source->id) != 0)
		return(-1L);
	if (writer_w_u32(target, array_size(source->data)) != 0)
		return(-1L);
	if (writer_write(target, array_at(source->data, 0), array_size(source->data)) < array_size(source->data))
		return(-1L);

	pad = dword_aligned(array_size(source->data)) - array_size(source->data);
	for (i = 0; i < pad; i++)
		writer_w_u8(target, 0);

	return 0;
}

int writer_append_compressed(writer_t *source, writer_t *target) {
	writer_t *temp;
	if ((temp = writer_compress(source)) == 0)
		return(-1L);
	if (writer_append(temp, target) != 0)
		return(-1L);
	fs_writer_close(temp);
	return(0);
}

int writer_set_id(writer_t *w, int id) {
	if (w == NULL)
		return(-1L);
	w->id = id;
	return(0);
}

int writer_seek(writer_t *w, int offset) {
	if (w == NULL)
		return(-1L);
	if (offset > array_size(w->data))
		return(-1L);
	if (offset < 0)
		return(-1L);

	w->ptr = offset;

	return(0);
}

int writer_advance(writer_t *w, int offset) {
	int ptr;

	if (w == NULL)
		return(-1L);

	ptr = w->ptr + offset;
	if (ptr > array_size(w->data))
		return(-1L);
	if (ptr < 0)
		return(-1L);
	w->ptr = ptr;

	return(0);
}

int writer_tell(writer_t *w) {
	if (w == NULL)
		return(-1L);
	return(w->ptr);
}

int writer_write(writer_t *w, void *data, int size) {
	void *end;

	debug_assert(size > 0);
	if (w == NULL || size <= 0)
		return(0);

	end = (void*)((int) data + size - 1);
	array_set_range(w->data, w->ptr, data, end);
	w->ptr += size;

	return(size);
}

int writer_w_u8(writer_t *w, uint8_t val) {
	if (writer_write(w, &val, sizeof(uint8_t)) == 0)
		return(-1L);
	return 0;
}

int writer_w_s8(writer_t *w, int8_t val) {
	if (writer_write(w, &val, sizeof(int8_t)) == 0)
		return(-1L);
	return(0);
}

int writer_w_u16(writer_t *w, uint16_t val) {
	if (writer_write(w, &val, sizeof(uint16_t)) == 0)
		return(-1L);
	return(0);
}

int writer_w_s16(writer_t *w, int16_t val) {
	if (writer_write(w, &val, sizeof(int16_t)) == 0)
		return(-1L);
	return(0);
}

int writer_w_u32(writer_t *w, uint32_t val) {
	if (writer_write(w, &val, sizeof(uint32_t)) == 0)
		return(-1L);
	return(0);
}

int writer_w_s32(writer_t *w, int32_t val) {
	if (writer_write(w, &val, sizeof(int32_t)) == 0)
		return(-1L);
	return(0);
}

int writer_w_u64(writer_t *w, uint64_t val) {
	if (writer_write(w, &val, sizeof(uint64_t)) == 0)
		return(-1L);
	return(0);
}

int writer_w_s64(writer_t *w, int64_t val) {
	if (writer_write(w, &val, sizeof(int64_t)) == 0)
		return(-1L);
	return(0);
}

int writer_w_f32(writer_t *w, float val) {
	if (writer_write(w, &val, sizeof(float)) == 0)
		return(-1L);
	return(0);
}

int writer_w_sz(writer_t *w, const char *sz) {
	int i, raise;

	if (writer_write(w, (char*)sz,  strlen(sz)) == 0)
		return(-1L);
	if (writer_w_u8(w, 0) != 0)
		return(-1L);

	raise = dword_aligned(strlen(sz) + 1) - strlen(sz) - 1;
	for (i = 0; i < raise; i++)
		writer_w_u8(w, 0);

	return(0);
}
