#define WINVER 0x0501
#define _WIN32_WINNT 0x0501 

#include <Windows.h>
#include <Windowsx.h>

extern "C" {
#include <platform.h>
#include <types.h>
#include <debug.h>
#include <filesystem.h>
#include <render.h>
#include <resource.h>
#include <scene.h>
#include <maths.h>
#include <collision.h>
#include <config.h>
}

#define		WNDCLASSNAME	"window_class_0001"

HINSTANCE		hinst		= NULL;
HWND			hwnd		= NULL;
ATOM			clsid		= NULL;

camera_t		*cam		= NULL;

scene_node_t	*teapot		= NULL;

int				offset_x	= INT_MAX;
int				offset_y	= INT_MAX;

float			ang_x		= 0;
float			ang_y		= 0;

bool			camera_changed = false;

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
	switch (msg) {
	case WM_MOUSEMOVE:
		if (wparam & MK_LBUTTON && cam) {
			if (offset_x == INT_MAX) {
				offset_x = GET_X_LPARAM(lparam);
			} else if (GET_X_LPARAM(lparam) - offset_x) {
				ang_x += float(offset_x - GET_X_LPARAM(lparam)) * 0.005f;
				offset_x = GET_X_LPARAM(lparam);
				camera_changed = true;
			}

			if (offset_y == INT_MAX) {
				offset_y = GET_Y_LPARAM(lparam);
			} else if (GET_Y_LPARAM(lparam) - offset_y) {
				ang_y += float(offset_y - GET_Y_LPARAM(lparam)) * 0.005f;
				offset_y = GET_Y_LPARAM(lparam);
				camera_changed = true;
			}

			if (camera_changed) {
				vector3_t axis;
				quaternion_t qx, qy, q;
				axis.x = 0, axis.y = 1.0f; axis.z = 0;
				quaternion_rotation_axis(&qx, &axis, ang_x);

				axis.x = 1.0f, axis.y = 0; axis.z = 0;
				quaternion_rotation_axis(&qy, &axis, ang_y);

				quaternion_multiply(&q, &qx, &qy);
				camera_set_rotation(cam, &q);
				camera_changed = false;
			}
			return 0;
		} else {
			offset_x = INT_MAX;
			offset_y = INT_MAX;
		}
	}
	return DefWindowProc(hwnd, msg, wparam, lparam);
}

void Game_CreateWindow(INT w, INT h, LPSTR title)
{
	WNDCLASS wndclass;

	memset(&wndclass, 0, sizeof(WNDCLASS));

	wndclass.hInstance		= hinst;
	wndclass.lpszClassName	= WNDCLASSNAME;
	wndclass.style			= CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc	= WndProc;
	wndclass.hCursor		= LoadCursor(NULL, MAKEINTRESOURCE(IDC_ARROW));
	wndclass.hIcon			= LoadIcon(NULL, MAKEINTRESOURCE(IDI_APPLICATION));
	wndclass.hbrBackground	= (HBRUSH) GetStockObject(BLACK_BRUSH);

	clsid = RegisterClass(&wndclass);
	if (!clsid)
		debug_error("can't register system window class");

	hwnd = CreateWindow(
		WNDCLASSNAME,
		title, WS_BORDER | WS_VISIBLE,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		w, h,
		NULL,
		NULL,
		hinst,
		0
	);

	if (!hwnd)
		debug_error("can't create system window");
}

void Game_DestroyWindow(void)
{
	DestroyWindow(hwnd);
	UnregisterClass(WNDCLASSNAME, hinst);
}

void Game_FlushMessages(void)
{
	MSG msg;
	while (PeekMessage(&msg, hwnd, 0, 0, PM_REMOVE)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

int main(void)
{
	hinst = GetModuleHandle(NULL);

	Game_CreateWindow(1280, 1024, "test");

	fs_init("fs.cfg");
//	fs_init("fs.cfg");
	resource_system_init();
	render_init(hwnd);
	scene_init();

	render_resize(1280, 1024);

	cam = scene_create_camera("main");
	scene_set_active_camera(cam);

	vector3_t pos; pos.x = 0; pos.y = 10.0f; pos.z = 0;
	camera_set_translation(cam, &pos);
	camera_set_perspective_fov(cam, 75.0f * M_PI / 180.0f, 1.25, 1.0, 10000.0f);

	reader_t *r = fs_reader_open();
	fs_reader_load(r, PA_MAPS, "test.map");

	scene_node_t *root = scene_get_root();

	matrix_t tm;
	matrix_identity(&tm);
	render_bind_matrix(MATRIX_MODEL, &tm);

	int i = 0;
	while (!reader_eof(r)) {
		reader_t *r1 = reader_find(r, i++, 0);
		debug_assert(r1);

		const char *node = reader_r_sz(r1);
		scene_node_t *n = scene_node_create(node);

		vector3_t v;
		quaternion_t q;
		v.x = reader_r_f32(r1); v.y= reader_r_f32(r1); v.z = reader_r_f32(r1);
		scene_node_set_translation(n, &v);

		v.x = reader_r_f32(r1); v.y= reader_r_f32(r1); v.z = reader_r_f32(r1);
		quaternion_rotation_yaw_pitch_roll(&q, v.x, v.y, v.z);
		scene_node_set_rotation(n, &q);

		v.x = reader_r_f32(r1); v.y= reader_r_f32(r1); v.z = reader_r_f32(r1);
		scene_node_set_scale(n, &v);

		reader_t *r2 = reader_find(r1, 1, 1);
		const char *mesh = reader_r_sz(r2);
		const char *texture = reader_r_sz(r2);
		fs_reader_close(r2);
		fs_reader_close(r1);

		char surfname[1024];
		sprintf(surfname, "surface_%i", i);
		surface_t *surf = scene_create_surface(surfname, mesh, texture);

		mesh_t *m = (mesh_t*) resource_get(mesh);
		texture_t *t = (texture_t*) resource_get(texture);

		mesh_load(m);
		texture_load(t);

		mesh_release(m);
		texture_release(t);

		scene_node_add_entity(n, (entity_t*)surf);
//		scene_node_set_lodmode(n, LOD_DISTANCE);
//		scene_node_set_lodmode(n, LOD_PERCENTAGE);
//		scene_node_set_range(n, 0.5f, 100.0f);
		scene_node_add_child(root, n);
	}

	fs_reader_close(r);

	vector4_t color;
	color.x = 0.0f; color.y = 5.0f; color.z = 128.0f, color.w = 0;
	render_set_clear_color(&color);

	config_t cfg = config_open();
	config_load(cfg, "game.cfg");
	float ds = config_get_float(cfg, "camera", "speed", 0.5f);
	config_close(cfg);

	bool cam_changed = false;
	vector3_t v, v1;
	while (1) {
		Game_FlushMessages();
		v.x = 0; v.y = 0; v.z = 0;
		if (GetAsyncKeyState(VK_ESCAPE) & 0x8000)
			break;
		if (GetAsyncKeyState(0x57) & 0x8000) {
			v.z = -ds;
			cam_changed = true;
		}
		if (GetAsyncKeyState(0x53) & 0x8000) {
			v.z = ds;
			cam_changed = true;
		}
		if (GetAsyncKeyState(0x41) & 0x8000) {
			v.x = -ds;
			cam_changed = true;
		}
		if (GetAsyncKeyState(0x44) & 0x8000) {
			v.x = ds;
			cam_changed = true;
		}
		if (GetAsyncKeyState(0x45) & 0x8000) {
			v.y = ds;
			cam_changed = true;
		}
		if (GetAsyncKeyState(0x46) & 0x8000) {
			v.y = -ds;
			cam_changed = true;
		}
		if (cam_changed) {
			matrix_t m;
			vector3_transform(&v1, &v, matrix_rotation_quaternion(&m, &cam->rotation));
			camera_translate(cam, &v1);
			cam_changed = false;
		}

		scene_render();
	}

	scene_shutdown();
	render_shutdown();
	fs_shutdown();

	Game_DestroyWindow();

	getchar();

	return 0;
}