#include <assert.h>
#include <fbxsdk.h>

#include <map>

#include <draft.h>

int main(int argc, char *argv[])
{
	if (argc < 2)
		return 0;

	KFbxSdkManager *fbxsdk = KFbxSdkManager::Create();
	KFbxScene *fbxscene = KFbxScene::Create(fbxsdk,"");
	KFbxImporter* importer = KFbxImporter::Create(fbxsdk,"");

	const char *file = argv[1];
	bool status = importer->Initialize(file, -1, fbxsdk->GetIOSettings());
	if (!status) {
		printf("\ncan't load file '%s'\n", file);
		return 0;
	}

	if (!importer->IsFBX()) {
		printf("\nimproper FBX file format '%s'\n", file);
		return 0;
	}

	puts("parsing fbx scene, please wait...");
	status = importer->Import(fbxscene);
	if (!status) {
		printf("\nimport failed.\n");
	}

	fs_init("fs.cfg");
	resource_system_init();

	writer_t *map = fs_writer_open();

	int node_id = 0;
	for (int i = 0; i < fbxscene->GetNodeCount(); i++) {
		std::map<int,int> vids;
		std::map<int,int> tids;
		KFbxNode *n = fbxscene->GetNode(i);
		const char *name = n->GetName();
		KFbxMesh *m = n->GetMesh();
		if (m != NULL) {
			int vcount = m->GetControlPointsCount();

			KStringList uv_names;
			m->GetUVSetNames(uv_names);

			mesh_data_t md;
			memset(&md, 0, sizeof(md));
			md.indices = array_create(sizeof(int16_t));
			md.points = array_create(sizeof(vector3_t));
			md.normals = array_create(sizeof(vector3_t));
			md.uv_maps = array_create(sizeof(int));
			array_t *uvs = array_create(sizeof(vector2_t));
			array_append(md.uv_maps, &uvs);
			int npolys = m->GetPolygonCount();
			int idx = 0;
			int npvertices = m->GetPolygonVertexCount() / npolys;
			for (int j = 0; j < npolys; j++) {
				for (int k = 0; k < npvertices; k++) {
					int16_t vid = m->GetPolygonVertex(j, k);
					int16_t tid = m->GetTextureUVIndex(j, k);

					std::map<int, int>::iterator it1 = vids.find(vid);
					std::map<int, int>::iterator it2 = tids.find(tid);
					if (it1 != vids.end() && it2 != tids.end() && it1->second == it2->second)
						idx = it1->second;
					else {
						idx = array_size(md.points);
						vids[vid] = idx;
						tids[tid] = idx;

						vector3_t xyz;
						KFbxVector4 kv = m->GetControlPointAt(vid);
						xyz.x = kv[0];
						xyz.y = kv[2];
						xyz.z = -kv[1];
						array_append(md.points, &xyz);

						fbxsdk_2012_2::KFbxVector4 norm;
						m->GetPolygonVertexNormal(j, k, norm);
						vector3_t dn;
						dn.x = norm[0]; dn.y = norm[1]; dn.z = norm[2];
						array_append(md.normals, &dn);

						vector3_t duv;
						fbxsdk_2012_2::KFbxVector2 uv;
						m->GetPolygonVertexUV(j, k, uv_names[0] , uv);
						duv.x = uv[0]; duv.y = (1 - uv[1]);
						array_append(uvs, &duv);
					}

					array_append(md.indices, &idx);
				}
			}

			char *ch, mesh[1024];
			sprintf(mesh, "%s.mesh", n->GetName(), i);
			while ((ch = strchr(mesh, '\\')))
				*ch = '_';
			while ((ch = strchr(mesh, '/')))
				*ch = '_';

			mesh_t *geom = mesh_create(name);
			mesh_build(geom, &md);

			writer_t *w = fs_writer_open();
			mesh_write(geom, w);
			if (!fs_writer_save(w, PA_MESHES, mesh))
				printf("error writing mesh '%s'\n", mesh);

			fs_writer_close(w);
			mesh_destroy(geom);

			char *texmap;
			char texture[1024];
			texture[0] = 0;
			if (n->GetMaterialCount() > 0) {
				KFbxSurfaceMaterial *mat = n->GetMaterial(0);
				if (mat) {
					const KFbxProperty prop = mat->FindProperty(KFbxSurfaceMaterial::sDiffuse);
					if (prop.IsValid()) {
						KFbxFileTexture* tex = KFbxCast<KFbxFileTexture>(prop.GetSrcObject(KFbxFileTexture::ClassId, 0));
						if (tex) {
							texmap = strdup(tex->GetFileName());
							char *p = strrchr(texmap, '.'); if (p) *p = 0;
							p = strrchr(texmap, '\\'); if (p) texmap = p + 1;
							p = strrchr(texmap, '/'); if (p) texmap = p + 1;
							sprintf(texture, "%s.pvr", texmap);
						}
					}
				}
			}

			fbxsdk_2012_2::fbxDouble3 pos = n->LclTranslation.Get();
			fbxsdk_2012_2::fbxDouble3 rot = n->LclRotation.Get();
			fbxsdk_2012_2::fbxDouble3 scale = n->LclScaling.Get();

			char mdl[1024];
			sprintf(mdl, "%s", n->GetName());
			while ((ch = strchr(mdl, '\\')))
				*ch = '_';
			while ((ch = strchr(mdl, '/')))
				*ch = '_';

			writer_t *node = fs_writer_open();
			writer_t *surface = fs_writer_open();
			writer_set_id(node, node_id++);
			writer_set_id(surface, 1);
			writer_w_sz(node, mdl);
			writer_w_f32(node, pos[0]);
			writer_w_f32(node, pos[2]);
			writer_w_f32(node, pos[1]);
			writer_w_f32(node, rot[0]);
			writer_w_f32(node, rot[2]);
			writer_w_f32(node, rot[1]);
			writer_w_f32(node, scale[0]);
			writer_w_f32(node, scale[2]);
			writer_w_f32(node, scale[1]);
			writer_w_sz(surface, mesh);
			writer_w_sz(surface, texture);
			writer_append(surface, node);
			writer_append(node, map);
			fs_writer_close(surface);
			fs_writer_close(node);
		}
	}

	fs_writer_save(map, PA_MAPS, "test.map");
	fs_writer_close(map);

	resource_system_shutdown();
	fs_shutdown();

	importer->Destroy();
	fbxscene->Destroy();
	fbxsdk->Destroy();

	return 0;
}