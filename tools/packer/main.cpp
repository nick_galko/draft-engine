#include <Windows.h>
#include <draft.h>

void compress_directory(const char *path, writer_t *archive, writer_t *contents) {
	char file[MAX_PATH];
	sprintf(file, "%s/*", path);
	writer_t *w;

	WIN32_FIND_DATA ffd;
	HANDLE hfind = FindFirstFile(file, &ffd);

	BOOL next = (hfind != INVALID_HANDLE_VALUE);
	while (next) {
		if (strcmp(ffd.cFileName, ".") != 0 && strcmp(ffd.cFileName, "..")) {
			if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
				char newpath[MAX_PATH];
				sprintf(newpath, "%s/%s", path, ffd.cFileName);
				compress_directory(newpath, archive, contents);
			} else {
				sprintf(file, "%s/%s", path, ffd.cFileName);
				int offset = writer_tell(archive);

				debug_message("compressing file %s", file);

				writer_t *w = fs_writer_open();
				writer_w_sz(w, file);
				writer_w_s32(w, offset);
				writer_append(w, contents);
				fs_writer_close(w);

				reader_t *r = fs_reader_open();
				fs_reader_load_file(r, file);

				w = fs_writer_open();
				writer_write(w, reader_data(r), reader_size(r));
				writer_append_compressed(w, archive);

				fs_writer_close(w);
				fs_reader_close(r);
			}
		}

		next = FindNextFile(hfind, &ffd);
		next = (next != ERROR_NO_MORE_FILES && next != 0);
	}

	FindClose(hfind);
}

int main(int argc, char *argv[])
{
	if (argc < 2) {
		debug_message("usage : packer <config file>");
		return 0;
	}

	fs_init(argv[1]);
	writer_t *filesystem = fs_writer_open();
	writer_t *archive = fs_writer_open();
	writer_t *contents = fs_writer_open();

	for (int i = 0; i < fs_get_alias_count(); i++) {
		char path[MAX_PATH];
		const char *alias = fs_get_alias(i);
		fs_get_path(alias, path, MAX_PATH);

		writer_t *w = fs_writer_open();
		writer_set_id(w, i);
		writer_w_sz(w, alias);
		writer_w_sz(w, path);
		writer_append(w, filesystem);
		fs_writer_close(w);

		compress_directory(path, archive, contents);
	}

	writer_t *container = fs_writer_open();
	writer_append_compressed(filesystem, container);
	writer_append_compressed(contents, container);
	writer_append(archive, container);
	fs_writer_save_file(container, "data.pak");

	fs_writer_close(container);
	fs_writer_close(contents);
	fs_writer_close(archive);
	fs_writer_close(filesystem);

	fs_shutdown();

	return 0;
}